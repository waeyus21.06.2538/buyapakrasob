<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ระบบจัดการ</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- General CSS Files -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">


    <!-- Theme style / Template CSS  -->
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/style.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/components.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/custom.css">
    <!-- <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/AdminLTE.css"> -->

    <!-- include summernote css/js -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
    <!-- iziToast -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css" rel="stylesheet">
    <!-- fancybox -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css" rel="stylesheet">

    <!-- bootstrap notify master -->
    <!-- <script src="<?=base_url()?>assets/bower_components/bootstrap-notify-master/js/bootstrap-notify.js"></script> -->

    <!-- General JS Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>


</head>

<body>

    <div class="main-wrapper">
        <section class="section">
            <div class="container mt-5">
                <div class="page-error">
                    <div class="page-inner">
                        <h1>404</h1>
                        <div class="page-description">
                            <b>ไม่มีหน้าเพจที่คุณเข้ามา !!!</b>
                        </div>
                        <div class="page-search">
                            <i class="fas fa-exclamation-triangle" style="font-size: 100px; color: red;"></i>
                        </div>
                        <div class="mt-1">
                            <a href="<?=$this->dasboard_url?>">กลับสู่หน้าหลัก</a>
                        </div>
                    </div>
                </div>
                <div class="simple-footer mt-5">
                    Copyright &copy; Buya Pakrasob 2020
                </div>
            </div>
        </section>
    </div>


    <!-- General JS Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="<?=base_url()?>assets/admin/js/stisla.js"></script>

    <!-- Template JS File -->
    <script src="<?=base_url()?>assets/admin/js/scripts.js"></script>
    <script src="<?=base_url()?>assets/admin/js/custom.js"></script>
</body>

</html>