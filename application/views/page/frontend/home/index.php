<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <section class="video-frm">
    <div class="cover">
      <div class="hi">
         <!-- <img src="<?= base_url();?>/assets/frontend/images/logo.png" alt="" width="100%"> -->
      </div>
    </div>
    <div class="tv">
        <!-- <div class="screen mute" id="tv"> -->
          <!-- <video autoplay muted loop id="myVideo" >
            <source src="<?=base_url();?>assets/frontend/video/bg-section1.mp4" type="video/mp4">
            Your browser does not support HTML5 video.
          </video> -->
          <img src="<?= base_url();?>/assets/frontend/images/banner.jpg" alt="" width="100%">
        <!-- </div> -->
    </div>
    <div class="contact">
      <div class="row text-center">
        <div class="col-sm-4">
          <h3>
            <!-- โทรศัพท์ -->
            <a href="<?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_link : '' ?>" > 
              <?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_icon : '' ?> : <?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_title : '' ?> 
            </a>
          </h3>
        </div>
        <div class="col-sm-4">
          <h3>
            <!-- facebook -->
            <a href="<?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_link : '' ?>" target="_blank" >
              <?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_icon : '' ?> : <?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_title : '' ?>
            </a>
          </h3>
        </div>
        <div class="col-sm-4">
          <h3>
            <!-- Email -->
            <a href="<?=!empty( $data['contactSocial'][2] )? $data['contactSocial'][2]->contact_socialcol_link : '' ?>" target="_blank" >
              <?=!empty( $data['contactSocial'][2] )? $data['contactSocial'][2]->contact_socialcol_icon : '' ?> : <?=!empty( $data['contactSocial'][2] )? $data['contactSocial'][2]->contact_socialcol_title : '' ?>
            </a>
          </h3>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section section1">
    <div class="container">
      <h1>“I'M BUYA-PAKRASOB”</h1>
      <p>BUYA-PAKRASOB เป็นเว็บไซต์โชว์สินค้า เสื้อผ้า กางเกง มือสองที่มีแบรนด์ และไม่มีแบรนด์ สวยๆกันทั้งนั้น ราคาเบาๆเขาถึงได้ทุกคน สามารถเลือกดูสินค้าตามใจสบายๆ เพลินๆ ชิวๆ โดยไม่ต้องเดินไปที่หน้าร้าน
        <br> ถ้าถูกใจอย่างไร ก็สามารถทักแชทเพจคุยได้เลย... 
      </p>
      <hr>
    </div>

    <?php    if($data['banner']): ?>
    <div class="container">
      <h1 class="text-title"><span class="text-first-title">ป</span>ระชาสัมพันธ์</h1>
      <div class="owl-carousel owl-theme">

        <?php 
          foreach($data['banner'] as $item_banner):
            $id = base64_encode($item_banner->banner_no);
        ?>

            <div class="item">
              <a href="<?=base_url().'banner/detail/'.$id?>">
                <img src="<?= base_url().$item_banner->banner_img;?>" alt="Image" onerror="this.src='<?=base_url("upload/error/error.jpg");?>'">
                <button class="float-right mt-1 btn-banner">
                  <small>กดดูรายละเอียด</small> 
                </button>
              </a>
            </div> 
    
        <?php endforeach; ?>

      </div>
    <hr>
    </div>  
    <?php endif; ?>

    <div class="container-fluid" data-aos="fade" data-aos-delay="500">      
    <h1 class="text-title"><span class="text-first-title">สิ</span>นค้าแนะนำ</h1>
        <div class="swiper-container images-carousel">
            <div class="swiper-wrapper">

          <?php 
            $id = '';
            if(!empty($data['recommendproducts'])):
              foreach($data['recommendproducts'] as $item):
                $is_sale = '';

                $id = base64_encode($item->recommendproducts_no);
                if($item->is_sale == 0){ $is_sale="img-black-white";}
          ?>
              <div class="swiper-slide">
                <div class="image-wrap">
                  <div class="image-info">
                    <div>
                      <h2 class="mb-3">
                        <?php 
                          $text_brands = '';
                          if(!empty($data['brands'])):
                            foreach($data['brands'] as $value):
                              if(!empty($item->recommendproducts_brand) && $item->recommendproducts_brand == $value->brands_no){
                                $text_brands = $value->brands_name;
                              }elseif( $item->recommendproducts_brand == '0'){
                                $text_brands = 'ไม่มีแบรนด์';
                              }
                            endforeach;
                          endif;
                        ?>
                        <?=$text_brands?>
                      </h2>

                      <?php if($item->is_sale == 1){?>
                      <a href="<?=base_url().'recommendproducts/detail/'.$id?>" class="btn btn-outline-white py-2 px-4">กดดูรายละเอียด >> </a>
                      <?php }else{ ?>
                      <a href="javascript:void(0)" class="btn btn-outline-white py-2 px-4 is-sale">กดดูรายละเอียด >> </a>
                      <?php }?>

                    </div>
      
                    <div class="mt-5" style="background: black;">
                      <h1 class="text-head font-weight-bold p-0 m-0" style="font-size: 60px;"> ฿<?=$item->recommendproducts_discount != null?$item->recommendproducts_discount : '0'?></h1>
                      <?php $text_d_none = ""; if($item->recommendproducts_price == "0"){$text_d_none = "d-none";}?>
                      <h6 class="text-white <?=$text_d_none?>"  style="font-size: 20px;-webkit-text-decoration-line: line-through;text-decoration-line: line-through;"> ฿<?=$item->recommendproducts_price != null?$item->recommendproducts_price : '0'?></h6>
                    </div>
                  </div>
                  <img class="<?=$is_sale?>" src="<?= base_url().$item->recommendproducts_img;?>" alt="Image" onerror="this.src='<?=base_url("upload/error/error.jpg");?>'">
                </div>
              </div>

          <?php 
              endforeach; 
            endif; 
          ?>
            </div>
            <div class="swiper-pagination"></div>
            <!-- <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div> -->
            <!-- <div class="swiper-scrollbar"></div> -->
        </div>
      </div>
      <div class="container">
        <div class="row">
            <div class="col">
              <hr>
              <h1 class="text-title"><span class="text-first-title">วี</span>ดิโอไลฟ์สดจาก Facebook</h1>
            </div>
        </div> 
      </div>
  </section>
  
  <section class="section section2" style="background-image: url('<?= base_url();?>/assets/frontend/images/banner.jpg');">
    <div class="container" style="padding: 20px;background-color: black;text-align: -webkit-center;text-align: -moz-center;">
      <div class="text-white fb-size">
        <div class="fb-video" data-href="<?=!empty($data['lives']->lives_link)? $data['lives']->lives_link : '';?>" data-show-text="true" data-width="" data-autoplay="false" data-show-captions="true">
          <blockquote cite="<?=!empty($data['lives']->lives_link)? $data['lives']->lives_link : '';?>" class="fb-xfbml-parse-ignore">
            <a href="<?=!empty($data['lives']->lives_link)? $data['lives']->lives_link : '';?>"></a>
            <p>มาแล้วครับ...งานใหม่ เด็ดๆ แบร์นล้วนๆ</p>โพสต์โดย 
            <a class="text-head" href="https://www.facebook.com/Buya-ผ้ากระสอบ-104467334535875/" target="_blank">Buya ผ้ากระสอบ</a> เร็วๆนี้...
          </blockquote>
        </div>
        <!-- <img src="<?= base_url();?>/assets/frontend/images/error.jpg" alt="" width="100%" height="650"> -->
      </div>
      <div class="text-center">
        <a class="push-lives" href="<?=!empty($data['lives']->lives_link)? $data['lives']->lives_link : '';?>" target="_blank" data-code="<?=!empty($data['lives']->lives_no)? $data['lives']->lives_no : '';?>" data-title="<?=!empty($data['lives']->lives_title)? $data['lives']->lives_title : '';?>">
          <button class="button button-contactForm btn_4 boxed-btn mt-3">
            กดรับชมไลฟ์สดใน Facebook >>
          </button>
        </a>
      </div>
    </div>
  </section>

  <section class="section section3">
    <div class="container mt-5 mb-5">
      <div class="col-12 text-center">
        <h1 class="text-white text-title"><span class="text-first-title">ก</span>างเกง เสื้อ เลือกได้เลย<span class="text-first-title">...</span></h1>
      </div>
    </div>

    <!-- *********************** -->

    <div class="portfolio_area">
          <div class="portfolio_wrap">
            <?php 
              $x=0;$y=9;$s=0;$m=0;
              foreach($data['generalproducts'] as $item_gp):
                $s=$x^$y;
                $m=$s%3;
                if($m==0){$small_width = 'small_width';}else{$small_width = '';}

                $id_gp = base64_encode($item_gp->generalproducts_no);
                $is_sale = '';
                if($item_gp->is_sale == 0){ $is_sale="img-black-white";}
            ?>
            
              <div class=" single_gallery <?=$small_width?>">
                  <div class="pr-0" style="z-index: 1;position: absolute;padding: 10px;right: 0;">
                    <div class="pl-3 pb-1 pr-1" style="background: black;">
                      <h1 class="text-head font-weight-bold pb-0 m-0" style="font-size: 50px;"> ฿<?=$item_gp->generalproducts_discount != null?$item_gp->generalproducts_discount : ''?></h1>
                      <?php $text_d_none = ""; if($item_gp->generalproducts_price == "0"){$text_d_none = "d-none";}?>
                      <h6 class="text-white <?=$text_d_none?>"  style="font-size: 15px;-webkit-text-decoration-line: line-through;text-decoration-line: line-through;"> ฿<?=$item_gp->generalproducts_price != null?$item_gp->generalproducts_price : ''?></h6>
                    </div>
                  </div>
                  <div class="thumb">
                      <img class="<?=$is_sale?>" src="<?= base_url().$item_gp->generalproducts_img;?>" alt="Image" onerror="this.src='<?=base_url("upload/error/error.jpg");?>'">
                  </div>
                  <div class="gallery_hover">
                    <div class="hover_inner">
                      <span class="text-head"> 
                        <?=$item_gp->generalproducts_type? $item_gp->generalproducts_type : '';?> & 
                        <?php 
                          $text_brands = '';
                          if(!empty($data['brands'])):
                            foreach($data['brands'] as $value_gp):
                              if(!empty($item_gp->generalproducts_brand) && $item_gp->generalproducts_brand == $value_gp->brands_no){
                                $text_brands = $value_gp->brands_name;
                              }elseif( $item_gp->generalproducts_brand == '0'){
                                $text_brands = 'ไม่มีแบรนด์';
                              }
                            endforeach;
                          endif;
                        ?>
                        <?=$text_brands?>
                      </span>
                      <h3 class="d-inline-block text-truncate" style="max-width: 180px;"><?=$item_gp->generalproducts_title? $item_gp->generalproducts_title : '';?></h3>
                      <div>
                
                        <?php if($item_gp->is_sale == 1){?>
                          <a href="<?=base_url().'generalproducts/detail/'.$id_gp?>">
                            <button class="button button-contactForm btn_4 boxed-btn-custom">กดดูรายละเอียด</button>
                          </a> 
                        <?php }else{ ?>
                          <a href="javascript:void(0)">
                            <button class="button button-contactForm btn_4 boxed-btn-custom is-sale">กดดูรายละเอียด</button>
                          </a>
                        <?php }?>

                      </div>
                    </div>
                  </div>
              </div>
          <?php 
            $x++;
            endforeach; 
          ?>
          </div>
        <div class="more_works text-center">
            <!-- <a href="portfolio-work.html">More Works</a> -->
            <!-- <button class="button button-contactForm btn_4 boxed-btn">กดดูสินค้าเพิ่มเติม</button> -->
            <a class="mt-2" href="<?=base_url()?>generalproducts/more">
              <button class="button button-contactForm btn_4 boxed-btn">กดดูสินค้าเพิ่มเติม</button>
            </a>
        </div>
    </div>
    
    <!-- *********************** -->     
    <div class="row">
      <div class="col-lg-8 img-for-pres pres-n" style="background-image: url('<?= base_url();?>/assets/frontend/images/m15.jpg');">
        <div class="over-ray-frm">
          <div class="content">
            <h5>Buya Pakrasob</h5>
            <!-- <button class="bg-none border border-white mt-3 mb-3">
              <img src="<?= base_url();?>/assets/frontend/images/logo.png" alt="">
            </button> -->
            <h5>เวลาทำงาน 17:00 น. - 24:00 น.</h5>
          </div>
        </div>
      </div>
      <div class="col-lg-4 content-n text-center">
        <h4>
          สนใจติดต่อได้ที่...<br>
          
          <!-- โทรศัพท์ -->
          <a href="<?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_link : '' ?>" > 
            <?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_icon : '' ?>  <?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_title : '' ?>
          </a><br>

          <!-- facebook -->
          <a href="<?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_link : '' ?>" target="_blank" >
            <?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_icon : '' ?> <?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_title : '' ?>
          </a><br>
          
          <!-- messenger -->
          <a href="<?=!empty( $data['contactSocial'][1] )? $data['contactSocial'][1]->contact_socialcol_link : '' ?>" target="_blank" >
            <?=!empty( $data['contactSocial'][1] )? $data['contactSocial'][1]->contact_socialcol_icon : '' ?> <?=!empty( $data['contactSocial'][1] )? $data['contactSocial'][1]->contact_socialcol_title : '' ?>
          </a><br>

        </h4>
      </div>
    </div>

    <div class="contact">
      <div class="text-center mb-3">
        <img src="<?= base_url();?>/assets/frontend/images/logo.png" alt="">
      </div>
      <h5>
        <?=!empty( $data['contact'] )? html_entity_decode($data['contact']->contact_address) : '' ?>
      </h5>
    </div>
      

  </section>
 
  <!-- facebook -->
  <script async defer crossorigin="anonymous" src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v8.0"></script>


  <script>
    /// parmiter 
    var post_url = '<?=base_url()?>api/admin';

    // เก็บสถิติการกดปุ่มเข้าไปดุไลฟ์สดจากหน้าเว็บ
    $(document).on('click', '.push-lives', function () {
      var lives_code = $(this).attr('data-code');
      var lives_title = $(this).attr('data-title');
      // console.log(lives_code);
      $.ajax({
        type: "POST",
        url: post_url + '/statistics/add',
        data: {
          lives_code : lives_code,
          lives_title : lives_title
        },
        success: function (data){},
        error: function (data) {},
      });
    });

    $(document).on('click', '.is-sale', function () {
      swal("สินค้าตัวนี้ขายแล้ว", "ขอบคุณที่สนใจสินค้าตัวนี้...", "warning");
    });

  </script>
