<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="video-frm">
  <div class="cover">
    <div class="hi">
        <!-- <img src="<?= base_url();?>/assets/frontend/images/logo.png" alt="" width="100%"> -->
    </div>
  </div>
  <div class="tv">
    <img src="<?= base_url();?>/assets/frontend/images/banner.jpg" alt="" width="100%">
  </div>
  <div class="contact">
    <div class="row text-center">
      <div class="col-sm-4">
        <h3>
          <!-- โทรศัพท์ -->
          <a href="<?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_link : '' ?>" > 
            <?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_icon : '' ?> : <?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_title : '' ?> 
          </a>
        </h3>
      </div>
      <div class="col-sm-4">
        <h3>
          <!-- facebook -->
          <a href="<?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_link : '' ?>" target="_blank" >
            <?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_icon : '' ?> : <?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_title : '' ?>
          </a>
        </h3>
      </div>
      <div class="col-sm-4">
        <h3>
          <!-- Email -->
          <a href="<?=!empty( $data['contactSocial'][2] )? $data['contactSocial'][2]->contact_socialcol_link : '' ?>" target="_blank" >
            <?=!empty( $data['contactSocial'][2] )? $data['contactSocial'][2]->contact_socialcol_icon : '' ?> : <?=!empty( $data['contactSocial'][2] )? $data['contactSocial'][2]->contact_socialcol_title : '' ?>
          </a>
        </h3>
      </div>
    </div>
  </div>
</section>



  <div class="container-fluid">

        <div class="row site-section">
          <div class="col-12 ">
            <h2 class="site-section-heading text-center"><?=$pagetitle?></h2>
          </div>
        </div>
         
          <!-- Start about Area -->
          <section class="about-area pb-5">
            <div class="container">
              <div class="row">
                <div class="col-lg-8">
                  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 ">
                      
                    
                      
                        <div class="row">
                          <div class="col-12">
                            <a data-fancybox="preview" href="<?=base_url().$data['banner']->banner_img?>">
                              <img class="img-fluid" src="<?=base_url().$data['banner']->banner_img?>" alt="Image" onerror="this.src='<?=base_url("upload/error/error.jpg");?>'">
                            </a>
                          </div>
                        </div>
                    

                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 ">
                      <hr>                
                      <div class="d-flex justify-content-between">
                        <div>
                          <i class="fas fa-eye"></i> <?=$data['banner']->number_view?> | 
                          <i class="fas fa-calendar-week"></i> <?=$data['banner']->banner_startDate? substr($data['banner']->banner_startDate,0,10) : ''?> ถึง 
                          <?=$data['banner']->banner_endDate? substr($data['banner']->banner_endDate,0,10) : ''?> 
                        </div>                   
                        <div style="background-color:black; color:white; padding:10px;text-align: center;">
                          <small>สิ้นสุด : </small> <b id="days">0</b> <small>วัน</small> <b id="hours">0</b> <small>ชม.</small>  <b id="minutes">0</b> <small>น.</small>
                          <br><h2 class="text-center text-head"><i class="fas fa-stopwatch"></i> : <b id="seconds">0</b></h2>
                        </div>
                      
                      </div>
                    
                      <h1 class="text-uppercase mt-3">
                        <span><?=$data['banner']->banner_title? $data['banner']->banner_title : '';?></span>
                      </h1>
                      <p>
                        <?=$data['banner']->banner_detail? html_entity_decode($data['banner']->banner_detail) : '';?>
                      </p>

                      <hr>
                      <div class="float-right pb-3">
                        <a class="genric-btn danger" href="<?=base_url();?>"> <i class="fas fa-reply"></i> ย้อนกลับ</a>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 pres-detail-h text-center">
                  <b style="background-color: black;border: 0;color: #ff9900;padding: 10px;border-radius: 20px;">กดติดตามเพจ...</b><br>
                  <div class="fb-page pt-4" data-href="https://www.facebook.com/Buya-ผ้ากระสอบ-104467334535875/" data-tabs="timeline" data-width="500" data-height="800" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                    <blockquote cite="https://www.facebook.com/Buya-ผ้ากระสอบ-104467334535875/" class="fb-xfbml-parse-ignore">
                      <a href="https://www.facebook.com/Buya-ผ้ากระสอบ-104467334535875/">Facebook</a>
                    </blockquote>
                  </div>
                </div>

              </div>
            </div>
          </section>
          <!-- End about Area -->

    
  </div>


<section class="section section3">
  <div class="contact">
    <div class="text-center">
      <img src="<?= base_url();?>/assets/frontend/images/logo.png" alt="">
    </div>
    <h5>

      <?=!empty( $data['contact'] )? html_entity_decode($data['contact']->contact_address) : '' ?>

    </h5>
  </div>
</section>

<!-- facebook -->
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v8.0"></script>

<script>
  /// parmiter 
  var post_url = '<?=base_url()?>api/admin';
  
  var banner_no = '<?=$data['banner']->banner_no? $data['banner']->banner_no : ''?>';
  const enddate = '<?=$data['banner']->banner_endDate? $data['banner']->banner_endDate : ''?>';
  const second = 1000,
        minute = second * 60,
        hour   = minute * 60,
        day    = hour * 24;

  if(enddate){

      let countDown = new Date(enddate.replace(/-/g, '/')).getTime(), // ให้ร้องรับ iOS .replace(/-/g, '/')
      x = setInterval(function() {
      
      let now = new Date().getTime(),
          distance = countDown - now;

      document.getElementById('days').innerText = Math.floor(distance / (day)),
        document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)),
        document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),
        document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);

      // do something later when date is reached
      if (distance < 0) {
        clearInterval(x);
        document.getElementById('days').innerText = '0',
        document.getElementById('hours').innerText = '0',
        document.getElementById('minutes').innerText = '0',
        document.getElementById('seconds').innerText = '0';

        $.ajax({
          type: "POST",
          url: post_url + '/banner/d_active',
          data: {
            banner_no: banner_no
          },
          success: function (data) {
            // console.log(data);
            if (data.status > 0) {
              setTimeout(function () {
                  window.location.replace("<?=base_url()?>");
              }, 5000);
            } else {
              load_notify('', data.error_msg, data.title, data.warning);
            }
          },
          error: function (data) {},
        });
      }
	  
      }, second)

  }
</script>
  