
		<section class="ftco-section ftco-no-pt ftco-no-pb contact-section">
      <div class="container-fluid px-0">
        <div class="row no-gutters block-9">
          <div class="col-md-6 d-flex">
            <iframe id="map" src=""></iframe>
          </div>

          <div class="col-md-6 d-flex">
            
            <div class="desc m-5">
              <div class="top top-relative">
                <b><span class="subheading">ที่อยู่ :</span></b>
                <!-- <h2 class="mb-4"><a href="single.html">Fashion Style</a></h2> -->
              </div>
              <div id="adress" class="absolute relative">
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove.</p>
              </div>

              <div class="top top-relative">
                <b><span class="subheading">อีเมล์ :</span></b>
                <!-- <h2 class="mb-4"><a href="single.html">Fashion Style</a></h2> -->
              </div>
              <div id="adress" class="absolute relative">
                <p id="email">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove.</p>
              </div>

              <div class="top top-relative">
                <b><span class="subheading">เบอร์โทร :</span></b>
                <!-- <h2 class="mb-4"><a href="single.html">Fashion Style</a></h2> -->
              </div>
              <div id="adress" class="absolute relative">
                <p id="tel">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove.</p>
              </div>


            </div>

          </div>

      </div>
    </section>

  <script src="<?=base_url()?>assets/home/js/jquery.min.js"></script>
  <script src="<?=base_url()?>assets/home/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="<?=base_url()?>assets/home/js/popper.min.js"></script>
  <script src="<?=base_url()?>assets/home/js/bootstrap.min.js"></script>
  <script src="<?=base_url()?>assets/home/js/jquery.easing.1.3.js"></script>
  <script src="<?=base_url()?>assets/home/js/jquery.waypoints.min.js"></script>
  <script src="<?=base_url()?>assets/home/js/jquery.stellar.min.js"></script>
  <script src="<?=base_url()?>assets/home/js/owl.carousel.min.js"></script>
  <script src="<?=base_url()?>assets/home/js/jquery.magnific-popup.min.js"></script>
  <script src="<?=base_url()?>assets/home/js/aos.js"></script>
  <script src="<?=base_url()?>assets/home/js/jquery.animateNumber.min.js"></script>
  <script src="<?=base_url()?>assets/home/js/scrollax.min.js"></script>
  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="<?=base_url()?>assets/home/js/google-map.js"></script> -->
  <script src="<?=base_url()?>assets/home/js/main.js"></script>
    


<script>

var post_url = "<?=base_url()?>api/admin";
var html = '';
$( document ).ready(function() {
  $.ajax({
            type: "POST",
            url: post_url+'/contact/view',
            data: '',
            success: function(data){
                if(data){
                  console.log(data.contact_social);
                  
                  $(data.contact_social).each(function(key,val){
                    html +='<div class="col-md-3 d-flex">';
                      html +='<div class="align-self-stretch box p-4 py-md-5 text-center">';
                        html +='<div class="icon d-flex align-items-center justify-content-center">';
                          html +='<span>'+val.contact_socialcol_icon+'</span>';
                        html +='</div>';
                        html +='<h3 class="mb-4">'+val.contact_socialcol_title+'</h3>';
                        html +='<p></p>';
                      html +='</div>';
                    html +='</div>';
                  });
                  $('#social').append(html);

                  // var text = data.contact.contact_map;
                  // var map = $.parseHTML(text);
                  // console.log($.parseHTML(data.contact.contact_map));
                  // alert(map);
                  $('#map').attr('src', data.contact.contact_map);
                  $('#adress').append().html(data.contact.contact_address);
                  $('#adress').text(data.contact.contact_address);
                  $('#adress').;

                }else{
                    //icon ,message ,title ,color
                    load_notify('', data.message, data.title, data.warning);
                    // set funtion reset button > class or id and time
                    setButtonReset("#btn-submit-delete",1000);
                }
            },
            error: function (data) {
                console.log('An error occurred.');
            },
        });
});

</script>