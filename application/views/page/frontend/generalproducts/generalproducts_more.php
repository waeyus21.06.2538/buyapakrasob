<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="video-frm">
  <div class="cover">
    <div class="hi">
        <!-- <img src="<?= base_url();?>/assets/frontend/images/logo.png" alt="" width="100%"> -->
    </div>
  </div>
  <div class="tv">
    <img src="<?= base_url();?>/assets/frontend/images/banner.jpg" alt="" width="100%">
  </div>
  <div class="contact">
    <div class="row text-center">
      <div class="col-sm-4">
        <h3>
          <!-- โทรศัพท์ -->
          <a href="<?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_link : '' ?>" > 
            <?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_icon : '' ?> : <?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_title : '' ?> 
          </a>
        </h3>
      </div>
      <div class="col-sm-4">
        <h3>
          <!-- facebook -->
          <a href="<?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_link : '' ?>" target="_blank" >
            <?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_icon : '' ?> : <?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_title : '' ?>
          </a>
        </h3>
      </div>
      <div class="col-sm-4">
        <h3>
          <!-- Email -->
          <a href="<?=!empty( $data['contactSocial'][2] )? $data['contactSocial'][2]->contact_socialcol_link : '' ?>" target="_blank" >
            <?=!empty( $data['contactSocial'][2] )? $data['contactSocial'][2]->contact_socialcol_icon : '' ?> : <?=!empty( $data['contactSocial'][2] )? $data['contactSocial'][2]->contact_socialcol_title : '' ?>
          </a>
        </h3>
      </div>
    </div>
  </div>
</section>


<section>
  <div class="container">
    <div class="row justify-content-center">
      <div class="mb-2 site-section">
        <div class="col-12">
          <h2 class="site-section-heading text-center"><?=$pagetitle?></h2>
        </div>
      </div>
    </div>

    <!-- <div class="row">
      <div class="col-12">
      
        <nav class="navbar navbar-expand-lg">
          <button class="navbar-toggler" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="menu_icon"><i class="fas fa-bars"></i></span>
          </button>

          <div class="collapse navbar-collapse main-menu-item" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link learn-btn text-uppercase" href="index.html">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link learn-btn text-uppercase" href="about.html">about</a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link learn-btn text-uppercase" href="contact.html">Contact</a>
                </li>
              </ul>
            </div>
          </nav>

      </div>
    </div> -->

    <hr>
  </div>
</section>

<section class="section">
  <div class="container-fluit">
    <div class="row">
      <div class="col-12">
      <div class="portfolio_area">
        <div class="portfolio_wrap">

          <?php 
            $x=0;$y=2;$s=0;$m=0;
            foreach($data['generalproducts'] as $item_gp):
              $id = base64_encode($item_gp->generalproducts_no);
              $s=$x*$y;
              $m=$s%3;
              if($m==2){$small_width = 'small_width';}else{$small_width = '';}

              $is_sale = '';
              if($item_gp->is_sale == 0){ $is_sale="img-black-white";}
          ?>
          
            <div class=" single_gallery <?=$small_width?>" style="max-height: 420px;">
                <div class="pr-0" style="z-index: 1;position: absolute;padding: 10px;right: 0;">
                  <div class="pl-3 pb-1 pr-1" style="background: black;">
                    <h1 class="text-head font-weight-bold pb-0 m-0" style="font-size: 50px;"> ฿<?=$item_gp->generalproducts_discount != null?$item_gp->generalproducts_discount : ''?></h1>
                    <?php $text_d_none = ""; if($item_gp->generalproducts_price == "0"){$text_d_none = "d-none";}?>
                    <h6 class="text-white <?=$text_d_none?>"  style="font-size: 15px;-webkit-text-decoration-line: line-through;text-decoration-line: line-through;"> ฿<?=$item_gp->generalproducts_price != null?$item_gp->generalproducts_price : ''?></h6>
                  </div>
                </div>
                <div class="thumb">
                    <img class="<?=$is_sale?>" src="<?= base_url().$item_gp->generalproducts_img;?>" alt="Image" onerror="this.src='<?=base_url("upload/error/error.jpg");?>'">
                </div>
                <div class="gallery_hover">
                  <div class="hover_inner">
                    <span class="text-head"> 
                      <?=$item_gp->generalproducts_type? $item_gp->generalproducts_type : '';?> & 
                      <?php 
                          $text_brands = '';
                          if(!empty($data['brands'])):
                            foreach($data['brands'] as $value_gp):
                              if(!empty($item_gp->generalproducts_brand) && $item_gp->generalproducts_brand == $value_gp->brands_no){
                                $text_brands = $value_gp->brands_name;
                              }elseif( $item_gp->generalproducts_brand == '0'){
                                $text_brands = 'ไม่มีแบรนด์';
                              }
                            endforeach;
                          endif;
                      ?>
                      <?=$text_brands?>
                    </span>
                    <h3 class="d-inline-block text-truncate" style="max-width: 180px;"><?=$item_gp->generalproducts_title? $item_gp->generalproducts_title : '';?></h3>
                    <div>
                
                      <?php if($item_gp->is_sale == 1){?>
                          <button class="button button-contactForm btn_4 boxed-btn-custom show-item" data-id="<?=$id?>">กดดูรายละเอียด</button>
                        <?php }else{ ?>
                          <a href="javascript:void(0)">
                            <button class="button button-contactForm btn_4 boxed-btn-custom is-sale">กดดูรายละเอียด</button>
                          </a>
                        <?php }?>

                    </div>
                  </div>
                </div>
            </div>
        <?php 
          $x++;
          endforeach; 
        ?>
        </div>
        <!-- <div class="more_works text-center">
          <button class="button button-contactForm btn_4 boxed-btn">กดดูสินค้าเพิ่มเติม</button>
        </div> -->
      </div>
      
      </div>
    </div>
  </div>
</section>


<section class="section section3">
  <div class="contact">
    <div class="text-center">
      <img src="<?= base_url();?>/assets/frontend/images/logo.png" alt="">
    </div>
    <h5>

      <?=!empty( $data['contact'] )? html_entity_decode($data['contact']->contact_address) : '' ?>

    </h5>
  </div>
</section>

<!-- modal show สินค้า -->
<div class="modal fade" role="dialog" id="exampleModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

    <div class="modal-header">
      <h5 class="modal-title">รายละเอียดสินค้า</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>

      <section class="about-area pt-5 pb-5">
        <div class="container">
          <div class="row align-items-center justify-content-between" id="text-show-item">
            
          </div>
        </div>
      </section>
  
      <div class="modal-footer bg-whitesmoke">
        <button class="genric-btn danger" data-dismiss="modal" aria-label="Close"> <i class="fas fa-times"></i> ปิด</button>

        <!-- messenger -->
        <a class="genric-btn primary" href="<?=!empty( $data['contactSocial'][1] )? $data['contactSocial'][1]->contact_socialcol_link : '' ?>" target="_blank" >
          <?=!empty( $data['contactSocial'][1] )? $data['contactSocial'][1]->contact_socialcol_icon : '' ?> <?=!empty( $data['contactSocial'][1] )? $data['contactSocial'][1]->contact_socialcol_title : '' ?>
        </a>

      </div>

    </div>
  </div>
</div>



<script>
    
  /// parmiter 
  var post_url = '<?=base_url()?>api/admin';
  var cotact = ''; 
      
      // โทรศัพท์
      cotact +='<a class="learn-btn text-uppercase" href="<?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_link : '' ?>" >'; 
      cotact +='<?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_icon : '' ?> <?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_title : '' ?>'; 
      cotact +='</a><br>';

      // facebook           
      cotact +='<a class="learn-btn text-uppercase" href="<?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_link : '' ?>" target="_blank" >';
      cotact +='<?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_icon : '' ?> <?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_title : '' ?>';
      cotact +='</a><br>';

      // messenger  
      cotact +='<a class="learn-btn text-uppercase" href="<?=!empty( $data['contactSocial'][1] )? $data['contactSocial'][1]->contact_socialcol_link : '' ?>" target="_blank" >';
      cotact +=' <?=!empty( $data['contactSocial'][1] )? $data['contactSocial'][1]->contact_socialcol_icon : '' ?> <?=!empty( $data['contactSocial'][1] )? $data['contactSocial'][1]->contact_socialcol_title : '' ?>';
      cotact +='</a>';

  function plus_view(generalproducts_code) {
    $.ajax({
    type: "POST",
    url: post_url + '/generalproducts/countgeneralproducts',
      data: {
        generalproducts_code : generalproducts_code
      },
      success: function (){},
      error: function (data) {},
    });
  }

  $(document).on('click', '.is-sale', function () {
    swal("สินค้าตัวนี้ขายแล้ว", "ขอบคุณที่สนใจสินค้าตัวนี้...", "warning");
  });


  $(document).on('click', '.show-item', function () {
    var html        ='';
    var generalproducts_code = $(this).attr('data-id');
    plus_view(generalproducts_code);
    // $('<div>.col-12').remove();
    $('#show-body-1').remove();
    $('#show-body-2').remove();

    $.ajax({
      type: "POST",
      url: post_url + '/generalproducts/showItem',
        data: {
          generalproducts_code : generalproducts_code
        },
        success: function (data) {
        
          // -----------------------------
          var img_detail=[];
          var x = 0;
          if(data.images_detail != ''){
            $.each(data.images_detail, function( index, value ) {
              img_detail[index] = '<?=base_url()?>'+value.generalproducts_img_detail_path; 
              x = index;
            });
            if(x != 3){
              var i;
              for (i = x; i < 3; ++i) {
                img_detail[i+1] = data.image_error;
              }
            }
          }else{
            var i;
            for (i = 0; i < 4; ++i) {
              img_detail[i] = data.image_error;
            }
          }

          // --------------------------------
          var text_d_none ='';
          if(data.generalproducts_price == "0"){
            text_d_none = "d-none"
          }
          html +='<div class="col-12 " id="show-body-1">';           
            html +='<div class="container">';
              
              html +='<div class="row">';
                html +='<div class="col-12 p-0">';
                  html +='<a data-fancybox="preview" href="'+data.generalproducts_img+'">';
                    html +='<img src="'+data.generalproducts_img+'" style="height: 100%;" width="100%" onerror="this.src=\''+data.image_error+'\'">';
                  html +='</a>';
                html +='</div>';
              html +='</div>';

              html +='<div class="row">';
                html +='<div class="col-3 p-1">';
                  html +='<a href="'+img_detail[0]+'" data-fancybox="preview">';
                    html +='<img src="'+img_detail[0]+'" style="height: 100%;" width="100%" onerror="this.src=\''+data.image_error+'\'">';
                  html +='</a>';
                html +='</div>';
                html +='<div class="col-3 p-1">';
                  html +='<a href="'+img_detail[1]+'" data-fancybox="preview">';
                    html +='<img src="'+img_detail[1]+'" style="height: 100%;" width="100%" onerror="this.src=\''+data.image_error+'\'">';
                  html +='</a>';
                html +='</div>';
                html +='<div class="col-3 p-1">';
                  html +='<a href="'+img_detail[2]+'" data-fancybox="preview">';
                    html +='<img src="'+img_detail[2]+'" style="height: 100%;" width="100%" onerror="this.src=\''+data.image_error+'\'">';
                  html +='</a>';
                html +='</div>';
                html +='<div class="col-3 p-1">';  
                  html +='<a href="'+img_detail[3]+'" data-fancybox="preview">';
                    html +='<img src="'+img_detail[3]+'" style="height: 100%;" width="100%" onerror="this.src=\''+data.image_error+'\'">';
                  html +='</a>';
                html +='</div>';
              html +='</div>'; 

              html +='</div>';  
          html +='</div>';

          html +='<div class="col-12 about-right" id="show-body-2">';
            html +='<hr>';                
            html +='<div class="d-flex justify-content-between align-items-center">';
              html +='<div>';
                html +='<p class="m-0">สามารถติดต่อได้ที่นี้ :</p>';
                html += cotact;
              html +='</div>';  
              html +='<div>';
                html +='<h1 class="text-head font-weight-bold pb-0 m-0" style="font-size: 60px;"> ฿'+data.generalproducts_discount+'</h1>';
                html +='<h6 class="text-black '+text_d_none+'"  style="font-size: 20px;-webkit-text-decoration-line: line-through;text-decoration-line: line-through;"> ฿'+data.generalproducts_price+'</h6>';
              html +='</div>';
            html +='</div>';
            html +='<hr>';
            html +='<span class="lnr"><i class="fas fa-qrcode"></i> '+data.generalproducts_code+'</span>';
            html +='<h1 class="text-uppercase mt-3">';
            html +='<span>'+data.generalproducts_title+'</span> <br>';
            html += data.generalproducts_type +' & '+ data.generalproducts_brand;
            html +='</h1>';
            html +='<p>'+data.generalproducts_detail+'</p>';
          html +='</div>';
          // --------body------------
          $('#text-show-item').html(html);
        }
      });
      $("#exampleModal").modal()
 
  });
  

</script>