<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="video-frm">
  <div class="cover">
    <div class="hi">
        <!-- <img src="<?= base_url();?>/assets/frontend/images/logo.png" alt="" width="100%"> -->
    </div>
  </div>
  <div class="tv">
    <img src="<?= base_url();?>/assets/frontend/images/banner.jpg" alt="" width="100%">
  </div>
  <div class="contact">
    <div class="row text-center">
      <div class="col-sm-4">
        <h3>
          <!-- โทรศัพท์ -->
          <a href="<?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_link : '' ?>" > 
            <?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_icon : '' ?> : <?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_title : '' ?> 
          </a>
        </h3>
      </div>
      <div class="col-sm-4">
        <h3>
          <!-- facebook -->
          <a href="<?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_link : '' ?>" target="_blank" >
            <?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_icon : '' ?> : <?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_title : '' ?>
          </a>
        </h3>
      </div>
      <div class="col-sm-4">
        <h3>
          <!-- Email -->
          <a href="<?=!empty( $data['contactSocial'][2] )? $data['contactSocial'][2]->contact_socialcol_link : '' ?>" target="_blank" >
            <?=!empty( $data['contactSocial'][2] )? $data['contactSocial'][2]->contact_socialcol_icon : '' ?> : <?=!empty( $data['contactSocial'][2] )? $data['contactSocial'][2]->contact_socialcol_title : '' ?>
          </a>
        </h3>
      </div>
    </div>
  </div>
</section>



<div class=""  data-aos="fade">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="row mb-2 site-section">
          <div class="col-12 ">
            <h2 class="site-section-heading text-center"><?=$pagetitle?></h2>
          </div>
        </div>
         
          <!-- Start about Area -->
          <section class="about-area pt-5 pb-5">
            <div class="container">
              <div class="row align-items-center justify-content-between">
                <div class="col-lg-6 col-md-12 col-sm-12 ">
                   
                
                  <div class="container-fluid p-0">
                    <div class="row">
                      <div class="col-12 p-0 about-left">
                        <a data-fancybox="preview" href="<?=base_url().$data['generalproducts']->generalproducts_img?>">
                          <img class="img-fluid" src="<?=base_url().$data['generalproducts']->generalproducts_img?>" alt="Image" onerror="this.src='<?=base_url("upload/error/error.jpg");?>'">
                        </a>
                      </div>
                    </div>
                    <div class="row">
                    
                  <?php 
                    foreach($data['generalproductsimagesdetail'] as $item):
                  ?>
                      <div class="col-3 p-1">
                        <a href="<?=base_url().$item->generalproducts_img_detail_path?>" data-fancybox="preview">
                          <img src="<?=base_url().$item->generalproducts_img_detail_path?>" style="height: 100%;" width="100%" onerror="this.src='<?=base_url("upload/error/error.jpg");?>'">
                        </a>
                      </div>
                  <?php 
                    endforeach;
                  ?>
                      </div>  
                  </div>  

                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 about-right">
                  <hr>                
                  <div class="d-flex justify-content-between align-items-center">
                    <div>
                      <p class="m-0">สามารถติดต่อได้ที่นี้ :</p>
                     
                        <!-- โทรศัพท์ -->
                        <a class="learn-btn text-uppercase" href="<?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_link : '' ?>" > 
                          <?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_icon : '' ?> <?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_title : '' ?> 
                        </a><br>

                        <!-- facebook -->
                        <a class="learn-btn text-uppercase" href="<?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_link : '' ?>" target="_blank" >
                          <?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_icon : '' ?> <?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_title : '' ?>
                        </a><br>

                        <!-- messenger -->
                        <a class="learn-btn text-uppercase" href="<?=!empty( $data['contactSocial'][1] )? $data['contactSocial'][1]->contact_socialcol_link : '' ?>" target="_blank" >
                          <?=!empty( $data['contactSocial'][1] )? $data['contactSocial'][1]->contact_socialcol_icon : '' ?> <?=!empty( $data['contactSocial'][1] )? $data['contactSocial'][1]->contact_socialcol_title : '' ?>
                        </a>
                    </div>
                    <div>
                      <h1 class="text-head font-weight-bold pb-0 m-0" style="font-size: 60px;"> ฿<?=$data['generalproducts']->generalproducts_discount != null?$data['generalproducts']->generalproducts_discount : ''?></h1>
                      <?php $text_d_none = ""; if($data['generalproducts']->generalproducts_price == "0"){$text_d_none = "d-none";}?>
                      <h6 class="text-black <?=$text_d_none?> "  style="font-size: 20px;-webkit-text-decoration-line: line-through;text-decoration-line: line-through;"> ฿<?=$data['generalproducts']->generalproducts_price != null?$data['generalproducts']->generalproducts_price : ''?></h6>
                    </div>
                  </div>
                  <hr>
                  <span class="lnr"><i class="fas fa-qrcode"></i> <?=$data['generalproducts']->generalproducts_code? $data['generalproducts']->generalproducts_code : '';?></span>
                  <h1 class="text-uppercase mt-3">
                    <span><?=$data['generalproducts']->generalproducts_title? $data['generalproducts']->generalproducts_title : '';?></span> <br>
                    <?=$data['generalproducts']->generalproducts_type? $data['generalproducts']->generalproducts_type : '';?> & 
                    
                    <?php 
                          $text_brands = '';
                          if(!empty($data['brands'])):
                            foreach($data['brands'] as $value_gp):
                              if(!empty($data['generalproducts']->generalproducts_brand) && $data['generalproducts']->generalproducts_brand == $value_gp->brands_no){
                                $text_brands = $value_gp->brands_name;
                              }elseif( $data['generalproducts']->generalproducts_brand == '0'){
                                $text_brands = 'ไม่มีแบรนด์';
                              }
                            endforeach;
                          endif;
                      ?>
                      <?=$text_brands?>

                  </h1>
                  <p>
                    <?=$data['generalproducts']->generalproducts_detail? html_entity_decode($data['generalproducts']->generalproducts_detail) : '';?>
                  </p>
                  <hr>
                  <div class="float-right">
                    <a class="genric-btn danger" href="<?=base_url('generalproducts/more');?>"> <i class="fas fa-reply"></i> กดดูสินค้าเพิ่มเติม</a>
                    
                    <!-- messenger -->
                    <a class="genric-btn primary" href="<?=!empty( $data['contactSocial'][1] )? $data['contactSocial'][1]->contact_socialcol_link : '' ?>" target="_blank" >
                      <?=!empty( $data['contactSocial'][1] )? $data['contactSocial'][1]->contact_socialcol_icon : '' ?> <?=!empty( $data['contactSocial'][1] )? $data['contactSocial'][1]->contact_socialcol_title : '' ?>
                    </a>

                  </div>
                </div>
              </div>
            </div>
          </section>
          <!-- End about Area -->

          <!-- <div class="row mb-2 site-section">
            <div class="col-12 ">
              <p class="site-section-heading">เพิ่มเติม</p>
            </div>
          </div> -->

          <!-- Start secvice Area -->
          <!-- <section class="secvice-area pt-5 pb-5">
            <div class="container">
              <div class="row">
                <div class="col-md-4 single-service">
                  <img class="d-block mx-auto img-fluid" src="<?=base_url().$data['generalproducts']->generalproducts_img?>" alt="">
                  <div class="desc">
                    <h2 class="text-uppercase">The Important of food</h2>
                    <p>
                      Usage of the Internet is becoming more common due to rapid advancement of technology and the power of globalization. Societies globalization. Societies and the power of globalization. Societies globalization. Societies.
                    </p>
                    <a class="text-uppercase view-details" href="#">View Details</a>
                  </div>
                </div>
                <div class="col-md-4 single-service">
                  <img class="d-block mx-auto img-fluid" src="<?=base_url().$data['generalproducts']->generalproducts_img?>" alt="">
                  <div class="desc">
                    <h2 class="text-uppercase">The Important of food</h2>
                    <p>
                      Usage of the Internet is becoming more common due to rapid advancement of technology and the power of globalization. Societies globalization. Societies and the power of globalization. Societies globalization. Societies.
                    </p>
                    <a class="text-uppercase view-details" href="#">View Details</a>
                  </div>
                </div>
                <div class="col-md-4 single-service">
                  <img class="d-block mx-auto img-fluid" src="<?=base_url().$data['generalproducts']->generalproducts_img?>" alt="">
                  <div class="desc">
                    <h2 class="text-uppercase">Thrill Friends And Family</h2>
                    <p>
                      Usage of the Internet is becoming more common due to rapid advancement of technology and the power of globalization. Societies.
                    </p>
                    <a class="text-uppercase view-details" href="#">View Details</a>
                  </div>
                </div>
              </div>
            </div>
        </section> -->
        <!-- End secvice Area -->
      
      </div>
    </div>
  </div>
</div>

<section class="section section3">
  <div class="contact">
    <div class="text-center">
      <img src="<?= base_url();?>/assets/frontend/images/logo.png" alt="">
    </div>
    <h5>

      <?=!empty( $data['contact'] )? html_entity_decode($data['contact']->contact_address) : '' ?>

    </h5>
  </div>
</section>

<script>
  /// parmiter 
  var post_url = '<?=base_url()?>api/admin';

</script>
