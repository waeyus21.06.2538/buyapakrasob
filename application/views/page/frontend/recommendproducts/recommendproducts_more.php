<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="video-frm">
  <div class="cover">
    <div class="hi">
        <!-- <img src="<?= base_url();?>/assets/frontend/images/logo.png" alt="" width="100%"> -->
    </div>
  </div>
  <div class="tv">
    <img src="<?= base_url();?>/assets/frontend/images/banner.jpg" alt="" width="100%">
  </div>
  <div class="contact">
    <div class="row text-center">
      <div class="col-sm-4">
        <h3>
          <!-- โทรศัพท์ -->
          <a href="<?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_link : '' ?>" > 
            <?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_icon : '' ?> : <?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_title : '' ?> 
          </a>
        </h3>
      </div>
      <div class="col-sm-4">
        <h3>
          <!-- facebook -->
          <a href="<?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_link : '' ?>" target="_blank" >
            <?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_icon : '' ?> : <?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_title : '' ?>
          </a>
        </h3>
      </div>
      <div class="col-sm-4">
        <h3>
          <!-- Email -->
          <a href="<?=!empty( $data['contactSocial'][2] )? $data['contactSocial'][2]->contact_socialcol_link : '' ?>" target="_blank" >
            <?=!empty( $data['contactSocial'][2] )? $data['contactSocial'][2]->contact_socialcol_icon : '' ?> : <?=!empty( $data['contactSocial'][2] )? $data['contactSocial'][2]->contact_socialcol_title : '' ?>
          </a>
        </h3>
      </div>
    </div>
  </div>
</section>


<section>
  <div class="container">
    <div class="row justify-content-center">
      <div class="mb-2 site-section">
        <div class="col-12">
          <h2 class="site-section-heading text-center"><?=$pagetitle?></h2>
        </div>
      </div>
    </div>

    <!-- <div class="row">
      <div class="col-12">
      
        <nav class="navbar navbar-expand-lg">
          <button class="navbar-toggler" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="menu_icon"><i class="fas fa-bars"></i></span>
          </button>

          <div class="collapse navbar-collapse main-menu-item" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link learn-btn text-uppercase" href="index.html">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link learn-btn text-uppercase" href="about.html">about</a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link learn-btn text-uppercase" href="contact.html">Contact</a>
                </li>
              </ul>
            </div>
          </nav>

      </div>
    </div> -->

    <hr>
  </div>
</section>

<section class="section section1">
  <div class="container-fluit">
    <div class="row">
      <div class="col-12 mt-3">
      <div class="portfolio_area">
        <div class="portfolio_wrap">

          <?php 
            $x=0;$y=2;$s=0;$m=0;
            foreach($data['recommendproducts_test'] as $item_test):
              $id = base64_encode($item_test->recommendproducts_no);
              $s=$x*$y;
              $m=$s%3;
              if($m==2){$small_width = 'small_width';}else{$small_width = '';}

              $is_sale = '';
              if($item_test->is_sale == 0){ $is_sale="img-black-white";}
          ?>
          
            <div class=" single_gallery <?=$small_width?>" style="max-height: 420px;">
                <div class="pr-0" style="z-index: 1;position: absolute;padding: 10px;right: 0;">
                  <div class="pl-3 pb-1 pr-1" style="background: black;">
                    <h1 class="text-head font-weight-bold pb-0 m-0" style="font-size: 50px;"> ฿<?=$item_test->recommendproducts_discount != null?$item_test->recommendproducts_discount : ''?></h1>
                    <h6 class="text-white"  style="font-size: 15px;-webkit-text-decoration-line: line-through;text-decoration-line: line-through;"> ฿<?=$item_test->recommendproducts_price != null?$item_test->recommendproducts_price : ''?></h6>
                  </div>
                </div>
                <div class="thumb">
                    <img class="<?=$is_sale?>" src="<?= base_url().$item_test->recommendproducts_img;?>" alt="Image" onerror="this.src='<?=base_url("upload/error/error.jpg");?>'">
                </div>
                <div class="gallery_hover">
                  <div class="hover_inner">
                    <span class="text-head"> <?=$item_test->recommendproducts_type? $item_test->recommendproducts_type : '';?> & <?=$item_test->recommendproducts_brand? strtoupper($item_test->recommendproducts_brand) : '';?></span>
                    <h3 class="d-inline-block text-truncate" style="max-width: 180px;"><?=$item_test->recommendproducts_title? $item_test->recommendproducts_title : '';?></h3>
                    <div>
                      <!-- <a href="<?=base_url().'recommendproducts/detail/'.$id?>" class="number-view" data-code="<?=$item_test->recommendproducts_no?>">
                        <button class="button button-contactForm btn_4 boxed-btn-custom ">กดดูรายละเอียด</button>
                      </a>  -->
                      <!-- <a href="javascript:void(0)" class="number-view" data-code="<?=$item_test->recommendproducts_no?>"> -->
                        <button class="button button-contactForm btn_4 boxed-btn-custom show-item" data-id="<?=$id?>">กดดูรายละเอียด</button>
                      <!-- </a>  -->
                    </div>
                  </div>
                </div>
            </div>
        <?php 
          $x++;
          endforeach; 
        ?>
        </div>
        <!-- <div class="more_works text-center">
          <button class="button button-contactForm btn_4 boxed-btn">กดดูสินค้าเพิ่มเติม</button>
        </div> -->
      </div>
      
      </div>
    </div>
  </div>
</section>


<section class="section section3 mt-5">
  <div class="contact">
    <div class="text-center">
      <img src="<?= base_url();?>/assets/frontend/images/logo.png" alt="">
    </div>
    <h5>

     <?=!empty( $data['contact'] )? html_entity_decode($data['contact']->contact_address) : '' ?>

    </h5>
  </div>
</section>

<!-- modal show สินค้า -->
<div class="modal fade" role="dialog" id="exampleModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content" id="text-show-item">

    <div class="modal-header">
      <h5 class="modal-title">รายละเอียดสินค้า</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>



 <!-- Start about Area -->
          <section class="about-area pt-5 pb-5">
            <div class="container">
              <div class="row align-items-center justify-content-between">
                <div class="col-12 ">

                <!-- fancybox ไม่ทำงาน ตอนนี้-->
                 
                  <div class="container">
                    <div class="row">
                      <div class="col-12 p-0">
                        <a data-fancybox="preview" href="<?=base_url().$data['recommendproducts']->recommendproducts_img?>">
                          <img class="img-fluid" src="<?=base_url().$data['recommendproducts']->recommendproducts_img?>" alt="Image" onerror="this.src='<?=base_url("upload/error/error.jpg");?>'">
                        </a>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-3 p-1">
                        <a data-fancybox="preview" href="<?=base_url().$data['recommendproducts']->recommendproducts_img?>">
                          <img src="" style="height: 100%;" width="100%" onerror="this.src='<?=base_url('upload/error/error.jpg');?>'">
                        </a>
                      </div>
                      <div class="col-3 p-1">
                        <a data-fancybox="preview" href="<?=base_url().$data['recommendproducts']->recommendproducts_img?>">
                          <img src="" style="height: 100%;" width="100%" onerror="this.src='<?=base_url('upload/error/error.jpg');?>'">
                        </a>
                      </div>
                      <div class="col-3 p-1">
                        <a data-fancybox="preview" href="<?=base_url().$data['recommendproducts']->recommendproducts_img?>">
                          <img src="" style="height: 100%;" width="100%" onerror="this.src='<?=base_url('upload/error/error.jpg');?>'">
                        </a>
                      </div>
                      <div class="col-3 p-1">
                        <a data-fancybox="preview" href="<?=base_url().$data['recommendproducts']->recommendproducts_img?>">
                          <img src="" style="height: 100%;" width="100%" onerror="this.src='<?=base_url('upload/error/error.jpg');?>'">
                        </a>
                      </div>
                    </div> 

                  </div>  



                </div>
                <div class="col-12 about-right">
                  <hr>                
                  <div class="d-flex justify-content-between align-items-center">
                    <div>
                      <p class="m-0">สามารถติดต่อได้ที่นี้ :</p>
                      <a class="learn-btn text-uppercase" href="tel:0929965667"> <i class="fas fa-phone-volume"></i> 092-9965667</a> <br>
                      <a class="learn-btn text-uppercase" href="https://www.facebook.com/Buya-ผ้ากระสอบ-104467334535875" target="_blank"><i class="fab fa-facebook-square"></i> Buya ผ้ากระสอบ</a> <br>
                      <a class="learn-btn text-uppercase" href="http://m.me/104467334535875" target="_blank"> <i class="fab fa-facebook-messenger"></i> ติดต่อทางแชท</a>
                    </div>
                    <div>
                      <h1 class="text-head font-weight-bold pb-0 m-0" style="font-size: 60px;"> ฿<?=$data['recommendproducts']->recommendproducts_discount != null?$data['recommendproducts']->recommendproducts_discount : ''?></h1>
                      <?php $text_d_none = ""; if($data['recommendproducts']->recommendproducts_price == "0"){$text_d_none = "d-none";}?>
                      <h6 class="text-black <?=$text_d_none?> "  style="font-size: 20px;-webkit-text-decoration-line: line-through;text-decoration-line: line-through;"> ฿<?=$data['recommendproducts']->recommendproducts_price != null?$data['recommendproducts']->recommendproducts_price : ''?></h6>
                    </div>
                  </div>
                  <hr>
                  <span class="lnr"><i class="fas fa-qrcode"></i> <?=$data['recommendproducts']->recommendproducts_code? $data['recommendproducts']->recommendproducts_code : '';?></span>
                  <h1 class="text-uppercase mt-3">
                    <span><?=$data['recommendproducts']->recommendproducts_title? $data['recommendproducts']->recommendproducts_title : '';?></span> <br>
                    <?=$data['recommendproducts']->recommendproducts_type? $data['recommendproducts']->recommendproducts_type : '';?> & <?=$data['recommendproducts']->recommendproducts_brand? $data['recommendproducts']->recommendproducts_brand : '';?>
                  </h1>
                  <p>
                    <?=$data['recommendproducts']->recommendproducts_detail? html_entity_decode($data['recommendproducts']->recommendproducts_detail) : '';?>
                  </p>
                  
                </div>
              </div>
            </div>
          </section>
          <!-- End about Area -->

      <div class="modal-footer bg-whitesmoke">
        <button class="genric-btn danger" data-dismiss="modal" aria-label="Close"> <i class="fas fa-reply"></i> ปิด</button>
        <a class="genric-btn primary" href="http://m.me/104467334535875" target="_blank"> <i class="fab fa-facebook-messenger"></i> ติดต่อทางแชท</a>
      </div>

    </div>
  </div>
</div>



<script>
    
  /// parmiter 
  var post_url = '<?=base_url()?>api/admin';

  $(document).on('click', '.number-view', function () {
    var recommendproducts_code = $(this).attr('data-code');
    // alert(recommendproducts_code );
    $.ajax({
    type: "POST",
    url: post_url + '/recommendproducts/countRecommendproducts',
      data: {
        recommendproducts_code : recommendproducts_code
      },
      success: function (){},
      error: function (data) {},
    });
  });

  $(document).on('click', '.show-item', function () {
    // e.preventDefault();
    var html ='';
    var html_sale = '';
    var recommendproducts_code = $(this).attr('data-id');
    
    $('#exampleModal').modal('toggle');

		// $("#exampleModal").modal()
 
  });
  

</script>
