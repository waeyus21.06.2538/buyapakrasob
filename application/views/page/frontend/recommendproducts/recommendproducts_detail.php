<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="video-frm">
  <div class="cover">
    <div class="hi">
        <!-- <img src="<?= base_url();?>/assets/frontend/images/logo.png" alt="" width="100%"> -->
    </div>
  </div>
  <div class="tv">
    <img src="<?= base_url();?>/assets/frontend/images/banner.jpg" alt="" width="100%">
  </div>
  <div class="contact">
    <div class="row text-center">
      <div class="col-sm-4">
        <h3>
          <!-- โทรศัพท์ -->
          <a href="<?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_link : '' ?>" > 
            <?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_icon : '' ?> : <?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_title : '' ?> 
          </a>
        </h3>
      </div>
      <div class="col-sm-4">
        <h3>
          <!-- facebook -->
          <a href="<?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_link : '' ?>" target="_blank" >
            <?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_icon : '' ?> : <?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_title : '' ?>
          </a>
        </h3>
      </div>
      <div class="col-sm-4">
        <h3>
          <!-- Email -->
          <a href="<?=!empty( $data['contactSocial'][2] )? $data['contactSocial'][2]->contact_socialcol_link : '' ?>" target="_blank" >
            <?=!empty( $data['contactSocial'][2] )? $data['contactSocial'][2]->contact_socialcol_icon : '' ?> : <?=!empty( $data['contactSocial'][2] )? $data['contactSocial'][2]->contact_socialcol_title : '' ?>
          </a>
        </h3>
      </div>
    </div>
  </div>
</section>



<div class=""  data-aos="fade">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="row mb-2 site-section">
          <div class="col-12 ">
            <h2 class="site-section-heading text-center"><?=$pagetitle?></h2>
          </div>
        </div>
         
          <!-- Start about Area -->
          <section class="about-area pt-5 pb-5">
            <div class="container">
              <div class="row align-items-center justify-content-between">
                <div class="col-lg-6 col-md-12 col-sm-12 ">
                   
                
                  <div class="container-fluid p-0">
                    <div class="row">
                      <div class="col-12 p-0 about-left">
                        <a data-fancybox="preview" href="<?=base_url().$data['recommendproducts']->recommendproducts_img?>">
                          <img class="img-fluid" src="<?=base_url().$data['recommendproducts']->recommendproducts_img?>" alt="Image" onerror="this.src='<?=base_url("upload/error/error.jpg");?>'">
                        </a>
                      </div>
                    </div>
                    <div class="row">
                    
                  <?php 
                    foreach($data['recommendproductsimagesdetail'] as $item):
                  ?>
                      <div class="col-3 p-1">
                        <a href="<?=base_url().$item->recommendproducts_img_detail_path?>" data-fancybox="preview">
                          <img src="<?=base_url().$item->recommendproducts_img_detail_path?>" style="height: 100%;" width="100%" onerror="this.src='<?=base_url("upload/error/error.jpg");?>'">
                        </a>
                      </div>
                  <?php 
                    endforeach;
                  ?>
                      </div>  
                  </div>  

                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 about-right">
                  <hr>                
                  <div class="d-flex justify-content-between align-items-center">
                    <div>
                      <p class="m-0">สามารถติดต่อได้ที่นี้ :</p>
                        <!-- โทรศัพท์ -->
                        <a class="learn-btn text-uppercase" href="<?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_link : '' ?>" > 
                          <?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_icon : '' ?> <?=!empty( $data['contactSocial'][3] )? $data['contactSocial'][3]->contact_socialcol_title : '' ?> 
                        </a><br>

                        <!-- facebook -->
                        <a class="learn-btn text-uppercase" href="<?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_link : '' ?>" target="_blank" >
                          <?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_icon : '' ?> <?=!empty( $data['contactSocial'][0] )? $data['contactSocial'][0]->contact_socialcol_title : '' ?>
                        </a><br>

                        <!-- messenger -->
                        <a class="learn-btn text-uppercase" href="<?=!empty( $data['contactSocial'][1] )? $data['contactSocial'][1]->contact_socialcol_link : '' ?>" target="_blank" >
                          <?=!empty( $data['contactSocial'][1] )? $data['contactSocial'][1]->contact_socialcol_icon : '' ?> <?=!empty( $data['contactSocial'][1] )? $data['contactSocial'][1]->contact_socialcol_title : '' ?>
                        </a>

                    </div>
                    <div>
                      <h1 class="text-head font-weight-bold pb-0 m-0" style="font-size: 60px;"> ฿<?=$data['recommendproducts']->recommendproducts_discount != null?$data['recommendproducts']->recommendproducts_discount : ''?></h1>
                      <?php $text_d_none = ""; if($data['recommendproducts']->recommendproducts_price == "0"){$text_d_none = "d-none";}?>
                      <h6 class="text-black <?=$text_d_none?> "  style="font-size: 20px;-webkit-text-decoration-line: line-through;text-decoration-line: line-through;"> ฿<?=$data['recommendproducts']->recommendproducts_price != null?$data['recommendproducts']->recommendproducts_price : ''?></h6>
                    </div>
                  </div>
                  <hr>
                  <span class="lnr"><i class="fas fa-qrcode"></i> <?=$data['recommendproducts']->recommendproducts_code? $data['recommendproducts']->recommendproducts_code : '';?></span>
                  <h1 class="text-uppercase mt-3">
                    <span><?=$data['recommendproducts']->recommendproducts_title? $data['recommendproducts']->recommendproducts_title : '';?></span> <br>
                    <?=$data['recommendproducts']->recommendproducts_type? $data['recommendproducts']->recommendproducts_type : '';?> & 
                    <?php 
                      $text_brands = '';
                      if(!empty($data['brands'])):
                        foreach($data['brands'] as $value_gp):
                          if(!empty($data['recommendproducts']->recommendproducts_brand) && $data['recommendproducts']->recommendproducts_brand == $value_gp->brands_no){
                            $text_brands = $value_gp->brands_name;
                          }elseif( $data['recommendproducts']->recommendproducts_brand == '0'){
                            $text_brands = 'ไม่มีแบรนด์';
                          }
                        endforeach;
                      endif;
                    ?>
                    <?=$text_brands?>
                  </h1>
                  <p>
                    <?=$data['recommendproducts']->recommendproducts_detail? html_entity_decode($data['recommendproducts']->recommendproducts_detail) : '';?>
                  </p>
                  <hr>
                  <div class="float-right">
                    <a class="genric-btn danger" href="<?=base_url();?>"> <i class="fas fa-reply"></i> ย้อนกลับ</a>

                    <!-- messenger -->
                    <a class="genric-btn primary" href="<?=!empty( $data['contactSocial'][1] )? $data['contactSocial'][1]->contact_socialcol_link : '' ?>" target="_blank" >
                      <?=!empty( $data['contactSocial'][1] )? $data['contactSocial'][1]->contact_socialcol_icon : '' ?> <?=!empty( $data['contactSocial'][1] )? $data['contactSocial'][1]->contact_socialcol_title : '' ?>
                    </a>

                  </div>
                </div>
              </div>
            </div>
          </section>
          <!-- End about Area -->

          <div class="row mb-2 site-section m-0 p-0">
            <div class="col-12 ">
              <p class="site-section-heading">สินค้าแนะนำเพิ่มเติม</p>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid" data-aos="fade" data-aos-delay="500">  
  <div class="swiper-container images-carousel">
    <div class="swiper-wrapper">
      <?php 
        $id = '';
        if(!empty($data['recommendproducts_more'])):
          foreach($data['recommendproducts_more'] as $item):
            $is_sale = '';

            $id = base64_encode($item->recommendproducts_no);
            if($item->is_sale == 0){ $is_sale="img-black-white";}
      ?>
          <div class="swiper-slide">
            <div class="image-wrap">
              <div class="image-info">
                <div>
                  <h2 class="mb-3">
                    <?php 
                      $text_brands = '';
                      if(!empty($data['brands'])):
                        foreach($data['brands'] as $value):
                          if(!empty($item->recommendproducts_brand) && $item->recommendproducts_brand == $value->brands_no){
                            $text_brands = $value->brands_name;
                          }elseif( $item->recommendproducts_brand == '0'){
                            $text_brands = 'ไม่มีแบรนด์';
                          }
                        endforeach;
                      endif;
                    ?>
                    <?=$text_brands?>
                  </h2>

                  <?php if($item->is_sale == 1){?>
                  <a href="<?=base_url().'recommendproducts/detail/'.$id?>" class="btn btn-outline-white py-2 px-4">กดดูรายละเอียด >> </a>
                  <?php }else{ ?>
                  <a href="javascript:void(0)" class="btn btn-outline-white py-2 px-4 is-sale">กดดูรายละเอียด >> </a>
                  <?php }?>

                </div>
  
                <div class="mt-5" style="background: black;">
                  <h1 class="text-head font-weight-bold p-0 m-0" style="font-size: 60px;"> ฿<?=$item->recommendproducts_discount != null?$item->recommendproducts_discount : '0'?></h1>
                  <?php $text_d_none = ""; if($item->recommendproducts_price == "0"){$text_d_none = "d-none";}?>
                  <h6 class="text-white <?=$text_d_none?>"  style="font-size: 20px;-webkit-text-decoration-line: line-through;text-decoration-line: line-through;"> ฿<?=$item->recommendproducts_price != null?$item->recommendproducts_price : '0'?></h6>
                </div>
              </div>
              <img class="<?=$is_sale?>" src="<?= base_url().$item->recommendproducts_img;?>" alt="Image" onerror="this.src='<?=base_url("upload/error/error.jpg");?>'">
            </div>
          </div>
      <?php 
          endforeach; 
        endif; 
      ?>
    </div>
    <div class="swiper-pagination"></div>
    <!-- <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>
    <div class="swiper-scrollbar"></div> -->
  </div>
</div>

<section class="section section3">
  <div class="contact">
    <div class="text-center">
      <img src="<?= base_url();?>/assets/frontend/images/logo.png" alt="">
    </div>
    <h5>

      <?=!empty( $data['contact'] )? html_entity_decode($data['contact']->contact_address) : '' ?>
      
    </h5>
  </div>
</section>

<script>
  /// parmiter 
  var post_url = '<?=base_url()?>api/admin';
  
  $(document).on('click', '.is-sale', function () {
    swal("สินค้าตัวนี้ขายแล้ว", "ขอบคุณที่สนใจสินค้าตัวนี้...", "warning");
  });

  $(document).on('click', '.number-view', function () {
    var recommendproducts_code = $(this).attr('data-code');
    $.ajax({
    type: "POST",
    url: post_url + '/recommendproducts/countRecommendproducts',
      data: {
        recommendproducts_code : recommendproducts_code
      },
      success: function (){},
      error: function (data) {},
    });

  });
</script>
