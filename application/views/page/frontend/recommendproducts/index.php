<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

  <section class="section section1">
    <div class="container">
      <h1>“WE ARE HAPPY COMMUNITY”</h1>
      <p>DDANCE THAILAND โรงเรียนสอนเต้นที่พัฒนาเด็กให้มีศักยภาพทางด้านทักษะ เพื่อการเติบโตอย่างมีความสุขและมีประสิทธิภาพ
      โดย ครูอู๋ เปรมจิตต์ ผู้ที่ปลุกปั้นลูกศิษย์ให้เป็นดาวประดับวงการมานับไม่ถ้วน ด้วยประสบการณ์กว่า20ปี ในการออกแบบท่าเต้นและผู้อยู่เบื้อหลังให้ศิลปินดังมากมาย
      </p>
      <hr>
    </div> 
    <div class="container-fluid" data-aos="fade" data-aos-delay="500">      
    <h1 class="text-head">สินค้าแนะนำ</h1>
        <div class="swiper-container images-carousel">
            <div class="swiper-wrapper">

          <?php 
            if(!empty($data['recommendproducts'])):
              foreach($data['recommendproducts'] as $item):
          ?>
              <div class="swiper-slide">
                <div class="image-wrap">
                  <div class="image-info">
                    <h2 class="mb-3"><?=strtoupper($item->recommendproducts_brand)?></h2>
                    <a href="javascript:void(0)" class="btn btn-outline-white py-2 px-4 number-view" data-code="<?=$item->recommendproducts_no?>">กดดูรายละเอียด >> </a>
                    <h1 class="text-head font-weight-bold pb-0 m-0" style="font-size: 60px;"> ฿<?=$item->recommendproducts_discount?$item->recommendproducts_discount : ''?></h1>
                    <h6 class="text-white"  style="font-size: 20px;-webkit-text-decoration-line: line-through;text-decoration-line: line-through;"> ฿<?=$item->recommendproducts_price?$item->recommendproducts_price : ''?></h6>
                  </div>
                  <img src="<?= base_url().$item->recommendproducts_img;?>" alt="Image">
                </div>
              </div>

          <?php 
              endforeach; 
            endif; 
          ?>
            </div>
            <div class="swiper-pagination"></div>
            <!-- <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div> -->
            <div class="swiper-scrollbar"></div>
        </div>
      </div>
      <div class="container">
        <div class="row">
            <div class="col">
            <hr>
            <h1 class="text-head">วีดิโอไลฟจาก Facebook</h1>
            </div>
        </div> 
      </div>
  </section>

    <script>
    /// parmiter 
    var post_url = '<?=base_url()?>api/admin';

    $(document).on('click', '.number-view', function () {
      var recommendproducts_code = $(this).attr('data-code');
// alert(recommendproducts_code );
      $.ajax({
      type: "POST",
      url: post_url + '/recommendproducts/countRecommendproducts',
        data: {
          recommendproducts_code : recommendproducts_code
        },
        success: function (){},
        error: function (data) {},
      });

    });
  

  </script>
