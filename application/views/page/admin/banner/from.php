<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1><?=$pagetitle?></h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="<?=$this->dasboard_url?>">แผนควบคุม</a></div>
        <div class="breadcrumb-item"><a href="<?=base_url().'admin/banner'?>">ป้ายโฆษณา</a></div>
        <div class="breadcrumb-item"><?=$pagetitle?></div>
      </div>
    </div>
    <div class="section-body">
      <div class="row mt-sm-4">
        <div class="col-12 col-md-12">
          <div class="card">
            <!-- <form  id="form-submit-contact" class="needs-validation"> autocomplete="off" -->
            <form id="form-submit-banner" class="needs-validation" enctype="multipart/form-data">
              <div class="card-header">
                <h4>แบบฟอร์ม</h4>
              </div>
              <div class="card-body">
              
                <div class="form-group">
                  <?php if($mode == 'edit'){?>

                  <small class="d-flex justify-content-center">***( รูปภาพเก่า )***</small>
                  <div class="d-flex justify-content-center">
                    <a data-fancybox="preview" href="<?=base_url().$data['banner']->banner_img?>">
                      <img src="<?=base_url().$data['banner']->banner_img?>" / width="150px"  onerror="this.src='<?=base_url("upload/error/error.jpg");?>'">
                    </a>
                  </div> 
                  <input class="form-control" type="hidden" name="banner_img_edit" value="<?=$data['banner']->banner_img?>">                  
                  
                  <?php }?>
                  <label>รูปภาพป้ายโฆษณา ( <span class="text-danger">รูปหลัก 1 รูป</span> )</label> 
                  <div class="file-loading"> 
                      <input id="input-b6" name="banner_img" type="file" <?=$mode == 'edit'? '' : 'required'?> >
                  </div>
                </div>

                <div class="form-group">
                    <label>ช่วงวันที่แสดงผล</label>
                    <div class="row">
                      <div class="col-12 col-lg-6">
                        <div class="form-group">
                          <label>เริ่ม : </label>
                          <input type="date" class="form-control" name="banner_startDate"  value="<?=!empty($data['banner']->banner_startDate)? substr($data['banner']->banner_startDate,0,10) : '';?>" required>
                        </div>
                      </div>
                      <div class="col-12 col-lg-6">
                        <div class="form-group">
                          <label>ถึง : </label>
                            <input type="date" class="form-control" name="banner_endDate"  value="<?=!empty($data['banner']->banner_endDate)? substr($data['banner']->banner_endDate,0,10) : '';?>" required>
                        </div>
                      </div>
                    </div>
                </div>

                <div class="form-group">
                  <label>หัวข้อ</label>
                    <input type="text" class="form-control" name="banner_title" value="<?=!empty($data['banner']->banner_title)? $data['banner']->banner_title : '';?>" required>
                  <div class="invalid-feedback">
                    กรุณากรอกหัวข้อด้วย
                  </div>
                </div>

                <div class="form-group">
                  <label>รายละเอียด</label>
                  <textarea class="form-control summernote-1" name="banner_detail" ><?=!empty($data['banner']->banner_detail)? html_entity_decode($data['banner']->banner_detail) : '';?></textarea>
                </div>

                
              </div>
              <div class="card-footer text-right">
                <input type="hidden" name="mode" value="<?=!empty($mode)? $mode : '' ?>">
                <input type="hidden" name="banner_no" value="<?=!empty($data['banner']->banner_no)? $data['banner']->banner_no : '' ?>">
                <button type="submit" class="btn btn-primary" id="btn-submit-item" data-loading-text="<i class='fa fa-spinner fa-pulse fa-fw'></i> กำลังบันทึกข้อมูล" >บันทึก</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<script>

  var post_url = "<?=$form_action?>";

  // this is the id of the form
  $("#form-submit-banner").submit(function(e) {
    e.preventDefault();
    setButtonLoading("#btn-submit-item");

    $.ajax({
      type: "POST",
      url: post_url,
      // data: $("#form-submit-banner").serialize(),
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData:false,
      success: function(data){
        if(data.status > 0){
          //icon ,message ,title ,color
          iziToast.success({
            title: data.title,
            message: data.message,
            position: 'topRight'
          });
          // setInputValue();
          setButtonReset("#btn-submit-item", 1000);
          setTimeout(function () {
            window.location.replace("<?=base_url()?>" + "admin/banner");
          }, 5000);
        }else{
          iziToast.error({
            title: data.title,
            message: data.message,
            position: 'topRight'
          });
          setButtonReset("#btn-submit-item", 1000);
        }
      },
      error: function (data) {
        console.log('An error occurred.');
        iziToast.error({
            title: data.title,
            message: data.message,
            position: 'topRight'
        });
        setButtonReset("#btn-submit-item", 1000);
      },
    });
  });

</script>