<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1><?=$pagetitle?></h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="<?=$this->dasboard_url?>">แผนควบคุม</a></div>
        <div class="breadcrumb-item"><a href="<?=base_url().'admin/banner'?>">ป้ายโฆษณา</a></div>
        <div class="breadcrumb-item"><?=$pagetitle?></div>
      </div>
    </div>
    <div class="section-body">
      <div class="row mt-sm-4">
        <div class="col-12 col-md-12">
          <div class="card">
           
            <div class="card-body">

            <div class="chocolat-parent">
              <a data-fancybox="preview" href="<?=base_url($data['banner']->banner_img)?>">
                <img alt="<?=$data['banner']->banner_title? $data['banner']->banner_title : ''?>" src="<?=base_url($data['banner']->banner_img)?>" width="100%" onerror="this.src='<?=base_url("upload/error/error.jpg");?>'">
              </a>
            </div>

              <article class="article article-style-c">
                
                <div class="article-details">
                  <hr>
                  <div class="article-category">
                    <i class="fas fa-eye"></i> <a><?=$data['banner']->number_view?></a> <div class="bullet"></div>
                    <i class="fas fa-calendar-check"></i> <a>สร้างเมื่อ : <?=$data['banner']->system_add_date? $data['banner']->system_add_date : ''?></a> ,
                    <i class="fas fa-calendar-times"></i> <a>แก้ไขเมื่อ : <?=$data['banner']->system_update_date? $data['banner']->system_update_date : ''?></a>
                  </div>
                

                  <div class="article-title mt-3">
                  <u><b><?=$data['banner']->banner_title? $data['banner']->banner_title : ''?></b></u>
                  </div>
                  <small>
                    <?=!empty($data['banner']->banner_detail)? html_entity_decode($data['banner']->banner_detail) : '';?> 
                  </small>

                  
                  <div class="article-user">
                    <div class="article-user-details">
                      <hr>
                      <div class="user-detail-name">
                        <small>ช่วงวันที่แสดงผล</small>
                      </div>
                      <div class="text-job"> <i class="fas fa-calendar-week"></i> เริ่ม : <?=$data['banner']->banner_startDate? substr($data['banner']->banner_startDate,0,10) : ''?> ถึง : <?=$data['banner']->banner_endDate? substr($data['banner']->banner_endDate,0,10) : ''?></div>
                      <div class="float-right">
                      <a href="<?=base_url()?>admin/banner" class="btn btn-danger m-1"><i class="fas fa-reply"></i> ย้อนกลับ</a>
                      </div>
                    </div>
                  </div>
                </div>
              </article>

            </div>
           
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
