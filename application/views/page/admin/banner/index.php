<style>
.switch {
    display: inline-block;
    height: 22px;
    position: relative;
    width: 42px;
}

.switch input {
	display:none;
}

.slider {
	background-color: #ccc;
	bottom: 0;
	cursor: pointer;
	left: 0;
	position: absolute;
	right: 0;
	top: 0;
	transition: .4s;
}

.slider:before {
    background-color: #fff;
    bottom: 2px;
    content: "";
    height: 18px;
    left: 2px;
    position: absolute;
    transition: .4s;
    width: 18px;
}

input:checked + .slider {
	background-color: #66bb6a;
}

input:checked + .slider:before {
	transform: translateX(20px);
}

.slider.round {
	border-radius: 34px;
}

.slider.round:before {
	border-radius: 50%;
}
</style>


<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1><?=$pagetitle?></h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="<?=$this->dasboard_url?>">แผนควบคุม</a></div>
        <div class="breadcrumb-item"><?=$pagetitle?></div>
      </div>
    </div>
    <div class="section-body">
      <div class="row mt-sm-4">
        <div class="col-12 col-md-12">
          <div class="card">
            <div class="card-header">
              <!-- <h4>รายละเอียดติดต่อเรา</h4> -->
              <div class="custom-btn-action">
                <a href="<?=$data['url_create']?>" class="btn btn-success m-1"><i class="fas fa-plus"></i> เพิ่มป้ายโฆษณา</a>
              </div>
            </div>
            <div class="card-body">
              <div class="section-body">
              <div class="card shadow">
                <div class="table-responsive p-2">
                  <table class="table align-items-center table-striped" id="example">
                    <thead class="thead-light">
                      <tr>
                        <th scope="col">รูป</th>
                        <th scope="col">หัวข้อ</th>
                        <th scope="col">สถานะ</th>
                        <th scope="col">ปี-เดือน-วัน เวลา ( เริ่ม )</th>
                        <th scope="col">ปี-เดือน-วัน เวลา ( ถึง )</th>
                        <th scope="col"></th>
                      </tr>
                    </thead>
                    <tbody>

                    <?php 
                      if(!empty($data['banner'])):
                        foreach($data['banner'] as $item):
                    ?>
                    <tr>
                      <th>
                      <div class="media align-items-center">
                          <a data-fancybox="preview" href="<?=base_url($item->banner_img)?>">
                            <img alt="<?=$item->banner_title? $item->banner_title : ''?>" src="<?=base_url($item->banner_img)?>" width="80" onerror="this.src='<?=base_url("upload/error/error.jpg");?>'">
                          </a>
                        </div>
                      </th>
                      <th scope="row">
                        <div class="media align-items-center">
                          <div class="media-body">
                            <span class="mb-0 text-sm"><?=$item->banner_title? $item->banner_title : ''?></span>
                          </div>
                        </div>
                      </th>  
                      <td>
                      <?php

                        $id = base64_encode($item->banner_no);
                        if($item->is_active < 1){
                          $text_is_active = 'checked';
                        }else{
                          $text_is_active = '';
                          $disabled        = 'disabled';
                        }
                      ?>
                        <label class="switch" for="active_<?=$id?>">
                          <input type="checkbox" class="checkbox_active" id="active_<?=$id?>" data-id="<?=$id?>" <?=$text_is_active?>>
                          <div class="slider round"></div>
                        </label>
                      </td>            
               
                      <td><?=$item->banner_startDate? substr($item->banner_startDate,0,10) : ''?></td>
                      <td><?=$item->banner_endDate?substr($item->banner_endDate,0,10) : ''?></td>
              
                      <td class="text-center" width="90px">
                      <a href="<?=base_url().'admin/banner/detail/'.$id?>"><i class="fas fa-eye"></i></a>
                        <a href="<?=base_url().'admin/banner/edit/'.$id?>"><i class="fas fa-edit"></i></a>
                        <a href="javascript:void(0)" data-id="<?=$id?>" class="delete-item"><i class="fas fa-trash-alt"></i></a>
                      </td>

                    </tr>
                    <?php 
                        endforeach; 
                      endif; 
                    ?>
                    </tbody>
                    <tfoot class="thead-light">
                      <tr>
                        <th scope="col">รูป</th>
                        <th scope="col">หัวข้อ</th>
                        <th scope="col">สถานะ</th>
                        <th scope="col">ปี-เดือน-วัน เวลา ( เริ่ม )</th>
                        <th scope="col">ปี-เดือน-วัน เวลา ( ถึง )</th>
                        <th scope="col"></th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
               </div>
              </div>
            </div>
            <!-- <div class="card-footer bg-whitesmoke">
              This is card footer
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </section>
</div>


<script>
  var post_url = '<?=base_url()?>api/admin'; 

  $(document).on('click', '.checkbox_active', function () {
    var active    = $(this).is(':checked');
    var banner_no = $(this).attr('data-id');

    $.ajax({
    type: "POST",
    url: post_url + '/banner/active',
      data: {
        active : active,
        banner_no : banner_no
      },
      success: function (data) {
        if(data.status > 0){
            iziToast.success({title: data.title,message: data.message,position: 'topRight'
          });
        }else{
          iziToast.error({title: data.title,message: data.message,position: 'topRight'
          });
        }
        setTimeout(function(){
            location.reload();
        }, 5000);    
      },
      error: function (data) {},
    });

  });

  $(document).on('click', '.delete-item', function () {
    var banner_no = $(this).attr('data-id');
    swal({
      title: "ต้องการลบข้อมูล หรือไหม?",
      text: "กรุณากดปุมยืนยัน",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "ยืนยัน",
      cancelButtonClass: "btn-secondary",
      cancelButtonText: "ยกเลิก",
      closeOnConfirm: true,
      closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {   
        $.ajax({
        type: "POST",
        url: post_url + '/banner/delete',
          data: {
            banner_no : banner_no,
          },
          success: function (data) {
            if(data.status > 0){
                iziToast.success({title: data.title,message: data.message,position: 'topRight'
              });
            }else{
              iziToast.error({title: data.title,message: data.message,position: 'topRight'
              });
            }
            setTimeout(function(){
                location.reload();
            }, 5000);    
          },
          error: function (data) {},
        });
      }
    });
  });

</script>