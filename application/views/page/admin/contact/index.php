<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1><?=$pagetitle?></h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="<?=$this->dasboard_url?>">แผนควบคุม</a></div>
        <div class="breadcrumb-item"><?=$pagetitle?></div>
      </div>
    </div>
    <div class="section-body">
      <div class="row mt-sm-4">
        <div class="col-12 col-md-12">
          <div class="card">
            <div class="card-header">
              <h4>รายละเอียดติดต่อเรา</h4>
              <div class="custom-btn-action">
                <?php if(empty($data['contact'])):?>
                <a href="<?=$data['url_create']?>" class="btn btn-success m-1">สร้าง</a>
                <?php endif;?>
                <a href="<?=$data['url_create']?>" class="btn btn-success m-1">สร้าง</a>
                <a href="<?=$data['url_edit']?>" class="btn btn-warning m-1">แก้ไข</a>
              </div>
            </div>
            <div class="card-body">
              <div class="section-body">
                <h4 class="section-title">ที่อยู่</h4>
                <div class="section-lead custom-text-detail">
                  <?=!empty($data['contact']->contact_address)? html_entity_decode($data['contact']->contact_address) : '' ?>
                </div>
                <h4 class="section-title">อีเมล์</h4>
                <p class="section-lead custom-text-detail">
                  <?=!empty($data['contact']->contact_email)? $data['contact']->contact_email : ''?>
                </p>
                <h4 class="section-title">เบอร์โทร</h4>
                <p class="section-lead custom-text-detail">
                  <?=!empty($data['contact']->contact_tel)? $data['contact']->contact_tel : ''?>
                </p>
                <h4 class="section-title">แผ่นที่</h4>
               
                  <iframe class="section-lead" src="<?=!empty($data['contact']->contact_map)? html_entity_decode($data['contact']->contact_map) : ''?>" frameborder="0" style="border:0"></iframe>
              
                <h4 class="section-title">โซเซียว</h4>
                  <div class="row section-lead custom-text-detail">
                    <?php 
                        if(count($data['contact_social']) > 0):
                        foreach($data['contact_social'] as $item_soial):?>
                          <div class="col-sm-3">
                            <div class="custom-size-icon">
                              <a href=" <?=$item_soial->contact_socialcol_link?>" target="_blank">
                                  <?=$item_soial->contact_socialcol_icon?>
                                </div>
                                <div class="mt-1">
                                  <?=$item_soial->contact_socialcol_title?>
                              </a>
                            </div>
                          </div>
                    <?php 
                        endforeach;
                      endif;?>
                  </div>
              </div>
            </div>
            <!-- <div class="card-footer bg-whitesmoke">
              This is card footer
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </section>
</div>


<script>
  /// parmiter 
  var post_url = '<?=base_url()?>api/admin';

  $(document).on('click', '#btn-detail-add', function () {
    var html = '';
    var n = $('#table-detail-add tbody tr').length + 1;

    html += '<tr id="tr-' + n + '">';
    html += '<td class="text-center">';
    html += '<button type="button" class="btn btn-danger btn-xs btn-flat btn-detail-delete" data-id="tr-' + n +
    '">';
    html += '<i class="fa fa-times" aria-hidden="true"></i> ลบ</button>';
    html += '</td>';
    html +=
      '<td><input type="text" class="form-control" name="contact_socialcol_title[]" value="" placeholder="กรุณาระบุข้อความ"></td>';
    html +=
      '<td><input type="text" class="form-control m-2" name="contact_socialcol_icon[]" value="" placeholder="กรุณาระบุไอคอลโซเซียว"></td>';
    html += '</tr>';
    $('#table-detail-add tbody').append(html);
  });

  $(document).on('click', '.btn-detail-delete', function () {
    $('#table-detail-add tbody #' + $(this).attr('data-id')).remove();
    var n = 1;
    $("#table-detail-add tbody tr").each(function () {
      $(this).attr('id', 'tr-' + n);
      $(this).find('.btn-detail-delete').attr('data-id', 'tr-' + n);
      n++;
    })
  });

  // this is the id of the form
  $("#form-submit-contact").submit(function (e) {
    e.preventDefault();

    $.ajax({
      type: "POST",
      url: post_url + '/contact/add',
      data: $("#form-submit-contact").serialize(),
      success: function (data) {
        if (data.status > 0) {
          //icon ,message ,title ,color
          iziToast.success({
            title: data.title,
            message: data.message,
            position: 'topRight'
          });
          setInputValue();
        } else {
          iziToast.error({
            title: data.title,
            message: data.message,
            position: 'topRight'
          });
        }
      },
      error: function (data) {
        console.log('An error occurred.');
      },
    });
  });
</script>