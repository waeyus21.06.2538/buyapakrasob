<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1><?=$pagetitle?></h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="<?=$this->dasboard_url?>">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="<?=base_url().'admin/contact'?>">ติดต่อเรา</a></div>
        <div class="breadcrumb-item"><?=$pagetitle?></div>
      </div>
    </div>
    <div class="section-body">
      <div class="row mt-sm-4">
        <div class="col-12 col-md-12">
          <div class="card">
            <!-- <form  id="form-submit-contact" class="needs-validation"> -->
            <form id="form-submit-contact" class="needs-validation">
              <div class="card-header">
                <h4>แบบฟอร์ม</h4>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>ที่อยู่</label>
                    <textarea class="form-control summernote-1" name="contact_address" required=""><?=!empty($data['contact']->contact_address)? html_entity_decode($data['contact']->contact_address) : '';?></textarea>
                  <div class="invalid-feedback">
                    กรุณากรอกที่อยู่ด้วย
                  </div>
                </div>
                <div class="form-group">
                  <label>อีเมล์</label>
                    <input type="email" class="form-control" name="contact_email" value="<?=!empty($data['contact']->contact_email)? $data['contact']->contact_email : '';?> "required="">
                  <div class="invalid-feedback">
                    กรุณากรอกอีเมล์ด้วย
                  </div>
                </div>
                <div class="form-group">
                  <label>เบอร์โทร</label>
                    <input type="text" class="form-control" name="contact_tel" value="<?=!empty($data['contact']->contact_tel)? $data['contact']->contact_tel : '';?>" required="">
                  <div class="invalid-feedback">
                    กรุณากรอกเบอร์โทรด้วย
                  </div>
                </div>
                <div class="form-group">
                  <label>แผ่นที่</label>

                    <?php if($mode == 'edit'):?>
                    <div class="text-center">
                      <iframe class="section-lead" src="<?=!empty($data['contact']->contact_map)? html_entity_decode($data['contact']->contact_map) : ''?>" frameborder="0" style="border:0"></iframe>
                    </div>
                    <?php endif;?>

                    <textarea class="form-control" name="contact_map" required=""><?=!empty($data['contact']->contact_map)? $data['contact']->cut_text_front.$data['contact']->contact_map.$data['contact']->cut_text_back : '';?></textarea>
                    <!-- <input type="text" class="form-control" name="contact_map" value="" required=""> -->
                  <div class="invalid-feedback">
                    กรุณากรอกแผ่นที่ด้วย
                  </div>
                </div>
    
                <div class="form-group">
                  <label class="control-label">โซเซียว </label>
                  <div class="col-sm-12">
                    <button type="button" id="btn-detail-add" class="btn btn-success btn-xs btn-flat">
                    <i class="fa fa-plus" aria-hidden="true"></i> เพิ่ม</button>
                    <a href="https://fontawesome.com/icons?d=gallery&q=social%20&m=free" class="btn btn-warning btn-xs btn-flat" target="_blank">
                    <i class="fa fa-external-link-alt" aria-hidden="true"></i> ตัวอยางไอคอลโซเซียว</a>

                    <table id="table-detail-add" class="table mt-2">
                      <thead>
                        <tr>
                          <th class="text-center" style="width:20%">#</th>
                          <th style="">ข้อความ</th>
                          <th style="">ลิงค์</th>
                          <th style="width:30%">ไอคอลโซเซียว</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                          if(!empty($data['contact_social']) && count($data['contact_social']) > 0):
                            $i = 1; 
                            foreach($data['contact_social'] as $item_soial):?>
                              <tr id="tr-<?=$i?>">
                                <td class="text-center">
                                  <button type="button" class="btn btn-danger btn-xs btn-flat btn-detail-delete" data-id="tr-<?=$i?>">
                                  <i class="fa fa-times" aria-hidden="true"></i> ลบ</button>
                                </td>
                                <td><input type="text" class="form-control" name="contact_socialcol_title[]" value="<?=!empty($item_soial->contact_socialcol_title)? $item_soial->contact_socialcol_title : '';?>" placeholder="กรุณาระบุข้อความ"></td>
                                <td><input type="text" class="form-control" name="contact_socialcol_link[]" value="<?=!empty($item_soial->contact_socialcol_link)? $item_soial->contact_socialcol_link : '';?>" placeholder="กรุณาระบุลิงค์"></td>
                                <td>
                                    <input type="text" class="form-control m-2" name="contact_socialcol_icon[]" value='<?=!empty($item_soial->contact_socialcol_icon)? $item_soial->contact_socialcol_icon: "" ?>' placeholder="กรุณาระบุไอคอลโซเซียว">
                                </td>
                              </tr>
                        <?php 
                            $i++;
                            endforeach;
                          else:?>
                            <tr id="tr-1">
                              <td class="text-center">
                                <button type="button" class="btn btn-danger btn-xs btn-flat btn-detail-delete" data-id="tr-1">
                                <i class="fa fa-times" aria-hidden="true"></i> ลบ</button>
                              </td>
                              <td><input type="text" class="form-control" name="contact_socialcol_title[]" value="" placeholder="กรุณาระบุข้อความ"></td>
                              <td><input type="text" class="form-control" name="contact_socialcol_link[]" value="" placeholder="กรุณาระบุลิงค์"></td>
                              <td>
                                  <input type="text" class="form-control m-2" name="contact_socialcol_icon[]" value="" placeholder="กรุณาระบุไอคอลโซเซียว">
                              </td>
                            </tr>
                        <?php 
                          endif;
                          ?>
                      </tbody>
                    </table>
                  </div>
                </div>

              </div>
              <div class="card-footer text-right">
                <input type="hidden" name="mode" value="<?=!empty($mode)? $mode : '' ?>">
                <input type="hidden" name="contact_id" value="<?=!empty($data['contact']->contact_id)? $data['contact']->contact_id : '' ?>">
                <button type="submit" class="btn btn-primary" id="btn-submit-item" data-loading-text="<i class='fa fa-spinner fa-pulse fa-fw'></i> กำลังบันทึกข้อมูล" >บันทึก</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<script>

var post_url = "<?=$form_action?>";

$(document).on('click','#btn-detail-add', function(){
  var html 	= '';
  var n 		= $('#table-detail-add tbody tr').length+1;

    html+='<tr id="tr-'+n+'">';
    html+='<td class="text-center">';
      html+='<button type="button" class="btn btn-danger btn-xs btn-flat btn-detail-delete" data-id="tr-'+n+'">';
      html+='<i class="fa fa-times" aria-hidden="true"></i> ลบ</button>';
    html+='</td>';
    html+='<td><input type="text" class="form-control" name="contact_socialcol_title[]" value="" placeholder="กรุณาระบุข้อความ"></td>';
    html+='<td><input type="text" class="form-control" name="contact_socialcol_link[]" value="" placeholder="กรุณาระบุลิงค์"></td>';
    html+='<td><input type="text" class="form-control m-2" name="contact_socialcol_icon[]" value="" placeholder="กรุณาระบุไอคอลโซเซียว"></td>';
    html+='</tr>';
  $('#table-detail-add tbody').append(html);
});

$(document).on('click','.btn-detail-delete', function(){
  $('#table-detail-add tbody #'+$(this).attr('data-id')).remove();
  var n = 1;
  $("#table-detail-add tbody tr").each(function(){
    $(this).attr('id', 'tr-'+n);
    $(this).find('.btn-detail-delete').attr('data-id', 'tr-'+n);
    n++;
  })
});

// this is the id of the form
$("#form-submit-contact").submit(function(e) {
  e.preventDefault();
  setButtonLoading("#btn-submit-item");

  $.ajax({
    type: "POST",
    url: post_url,
    data: $("#form-submit-contact").serialize(),
    success: function(data){
      if(data.status > 0){
        //icon ,message ,title ,color
        iziToast.success({
          title: data.title,
          message: data.message,
          position: 'topRight'
        });
        // setInputValue();
        setButtonReset("#btn-submit-item", 1000);
        setTimeout(function () {
          window.location.replace("<?=base_url()?>" + "admin/contact");
        }, 5000);
      }else{
        iziToast.error({
          title: data.title,
          message: data.message,
          position: 'topRight'
        });
        setButtonReset("#btn-submit-item", 1000);
      }
    },
    error: function (data) {
      console.log('An error occurred.');
      setButtonReset("#btn-submit-item", 1000);
    },
  });
});

</script>