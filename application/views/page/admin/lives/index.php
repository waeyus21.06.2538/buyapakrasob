<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1><?=$pagetitle?></h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="<?=$this->dasboard_url?>">แผนควบคุม</a></div>
        <div class="breadcrumb-item"><a href="<?=base_url().'admin/lives/detail'?>">รายการไลฟ์สด</a></div>
        <div class="breadcrumb-item"><?=$pagetitle?></div>
      </div>
    </div>
    <div class="section-body">
      <div class="row mt-sm-4">
        <div class="col-12 col-md-12">
          <div class="card">
            <form id="form-submit-contact" class="needs-validation" >
              <div class="card-header">
                <h4>แบบฟอร์ม</h4>
                <div class="custom-btn-action">
                  <a href="<?=base_url().'admin/lives/detail'?>" class="btn btn-success m-1"><i class="fas fa-eye"></i> ดูรายการไลฟ์สด</a>
                </div>
              </div>
              <div class="card-body">
                <div class="container" style="padding: 20px;background-color: black;text-align: -webkit-center;text-align: -moz-center;">
                  <div style="width: 300px !important;">
                    <div class="fb-video" data-href="<?=!empty($data['lives']->lives_link)? $data['lives']->lives_link : '';?>" data-show-text="true" data-width="">
                      <blockquote cite="<?=!empty($data['lives']->lives_link)? $data['lives']->lives_link : '';?>" class="fb-xfbml-parse-ignore">
                        <a href="<?=!empty($data['lives']->lives_link)? $data['lives']->lives_link : '';?>"></a><p>เกิดข้อผิดพลาด</p> 
                        <a href="https://www.facebook.com/Buya-ผ้ากระสอบ-104467334535875/">Buya ผ้ากระสอบ</a>
                      </blockquote>
                    </div>
                  </div>
                </div>
                <div class="form-group mt-3">
                  <label>คักลอกข้อความจาก facebook มาว่าง<br>เพื่อทำการคัดลอก</label>
                  <textarea class="form-control" name="lives_link_detail" id="" cols="30" rows="10" required></textarea>
                </div>

                <div class="form-group">
                  <label>ลิงค์ไลฟ์สด</label>
                    <input type="text" class="form-control" name="lives_link" value="<?=!empty($data['lives']->lives_link)? $data['lives']->lives_link : '';?>"required>
                </div>
                <div class="form-group">
                  <label>หัวข้อ</label>
                    <input type="text" class="form-control" name="lives_title" value="<?=!empty($data['lives']->lives_title)? $data['lives']->lives_title : '';?>"required>
                </div>
                <div class="form-group">
                  <label>วันที่ไลฟ์สด</label>
                    <input type="text" class="form-control" name="lives_date" value="<?=!empty($data['lives']->lives_date)? $data['lives']->lives_date : '';?>"required>
                </div>
                
              <div class="card-footer text-right">
                <input type="hidden" name="lives_id" value="<?=!empty($data['lives']->lives_no)? $data['lives']->lives_no : '' ?>">
                <button type="submit" class="btn btn-primary" id="btn-submit-item" data-loading-text="<i class='fa fa-spinner fa-pulse fa-fw'></i> กำลังบันทึกข้อมูล" >บันทึก</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- facebook -->
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v8.0"></script>

<script>

var post_url = "<?=$form_action?>";

// this is the id of the form
$("#form-submit-contact").submit(function(e) {
  e.preventDefault();
  setButtonLoading("#btn-submit-item");

  $.ajax({
    type: "POST",
    url: post_url,
    // data: $("#form-submit-contact").serialize(),
    data: new FormData(this),
    contentType: false,
    cache: false,
    processData:false,
    success: function(data){
      if(data.status > 0){
        //icon ,message ,title ,color
        iziToast.success({
          title: data.title,
          message: data.message,
          position: 'topRight'
        });
        // setInputValue();
        setButtonReset("#btn-submit-item", 1000);
        setTimeout(function () {
          window.location.replace("<?=base_url()?>" + "admin/lives");
        }, 5000);
      }else{
        iziToast.error({
          title: data.title,
          message: data.message,
          position: 'topRight'
        });
        setButtonReset("#btn-submit-item", 1000);
      }
    },
    error: function (data) {
      console.log('An error occurred.');
      setButtonReset("#btn-submit-item", 1000);
    },
  });
});

</script>