<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1><?=$pagetitle?></h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="<?=$this->dasboard_url?>">แผนควบคุม</a></div>
        <div class="breadcrumb-item"><?=$pagetitle?></div>
      </div>
    </div>
    <div class="section-body">
      <div class="row mt-sm-4">
        <div class="col-12 col-md-12">
          <div class="card">
            <div class="card-header">
              <!-- <h4>รายละเอียดติดต่อเรา</h4> -->
              <div class="custom-btn-action">
                <a href="<?=$url_create?>" class="btn btn-success m-1"><i class="fas fa-plus"></i> เพิ่มรายการไลฟ์สด</a>
              </div>
            </div>
            <div class="card-body">
              <div class="section-body">
              <div class="card shadow">
                <div class="table-responsive p-2">
                  <table class="table align-items-center table-striped" id="exampleLives">
                    <thead class="thead-light">
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col" width="20%">หัวข้อ</th>
                        <th scope="col" width="30%">จำนวนคนกดดูจากหน้าเว็บไซต์</th>
                        <th scope="col" width="20%">วันที่ไลฟ์สด</th>
                        <th scope="col" width="20%">ลิงค์ไลฟ์สด</th>
                        <th scope="col" width="10%"></th>
                      </tr>
                    </thead>
                    <tbody>

                    <?php 
                      if(!empty($data['lives'])):
                        $key = 1;
                        foreach($data['lives'] as $item):
                    ?>
                    <tr>
                      <th><?=$key++?></th>
                      <th scope="row">
                        <div class="media align-items-center">
                          <div class="media-body">
                            <span class="mb-0 text-sm"><?=$item->lives_title? $item->lives_title : ''?></span>
                          </div>
                        </div>
                      </th>              
                      <td><?=$item->number_view? number_format($item->number_view) : '0'?> ครั้ง</td>
                      <td><?=$item->lives_date? $item->lives_date : ''?></td>
                      <td>
                        <div class="fb-video" data-href="<?=$item->lives_link? $item->lives_link : ''?>" data-width=""></div>
                        <!-- <p><?=$item->lives_link? $item->lives_link : ''?></p> -->
                      </td>
                      <td class="text-center">
                        <a href="javascript:void(0)" data-id="<?=$item->lives_no? $item->lives_no : ''?>" class="delete-item"><i class="fas fa-trash-alt"></i></a>
                      </td>
                    </tr>
                    <?php 
                        endforeach; 
                      endif; 
                    ?>
                    </tbody>
                    <tfoot class="thead-light">
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">หัวข้อ</th>
                        <th scope="col">จำนวนคนกดดูจากหน้าเว็บไซต์</th>
                        <th scope="col">วันที่ไลฟ์สด</th>
                        <th scope="col">ลิงค์ไลฟ์สด</th>
                        <th scope="col"></th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
               </div>
              </div>
            </div>
            <!-- <div class="card-footer bg-whitesmoke">
              This is card footer
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- facebook -->
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v8.0"></script>

<script>
  var post_url = '<?=base_url()?>api/admin'; 

  $(document).on('click', '.delete-item', function () {
    var lives_no = $(this).attr('data-id');
    swal({
      title: "ต้องการลบข้อมูล หรือไหม?",
      text: "กรุณากดปุมยืนยัน",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "ยืนยัน",
      cancelButtonClass: "btn-secondary",
      cancelButtonText: "ยกเลิก",
      closeOnConfirm: true,
      closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {   
        $.ajax({
        type: "POST",
        url: post_url + '/lives/delete',
          data: {
            lives_no : lives_no,
          },
          success: function (data) {
            if(data.status > 0){
                iziToast.success({title: data.title,message: data.message,position: 'topRight'
              });
            }else{
              iziToast.error({title: data.title,message: data.message,position: 'topRight'
              });
            }
            setTimeout(function(){
                location.reload();
            }, 5000);    
          },
          error: function (data) {},
        });
      }
    });
  });

</script>

