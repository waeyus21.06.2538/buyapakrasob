<style>
.switch {
    display: inline-block;
    height: 22px;
    position: relative;
    width: 42px;
}

.switch input {
	display:none;
}

.slider {
	background-color: #ccc;
	bottom: 0;
	cursor: pointer;
	left: 0;
	position: absolute;
	right: 0;
	top: 0;
	transition: .4s;
}

.slider:before {
    background-color: #fff;
    bottom: 2px;
    content: "";
    height: 18px;
    left: 2px;
    position: absolute;
    transition: .4s;
    width: 18px;
}

input:checked + .slider {
	background-color: #66bb6a;
}

input:checked + .slider:before {
	transform: translateX(20px);
}

.slider.round {
	border-radius: 34px;
}

.slider.round:before {
	border-radius: 50%;
}
</style>


<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1><?=$pagetitle?></h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="<?=$this->dasboard_url?>">แผนควบคุม</a></div>
        <div class="breadcrumb-item"><?=$pagetitle?></div>
      </div>
    </div>
    <div class="section-body">
      <div class="row mt-sm-4">
        <div class="col-12 col-md-12">
          <div class="card">
            <div class="card-header">
              <!-- <h4>รายละเอียดติดต่อเรา</h4> -->
              <div class="custom-btn-action">
                <a href="<?=$data['url_create']?>" class="btn btn-success m-1"><i class="fas fa-plus"></i> เพิ่มสินค้า</a>
              </div>
            </div>
            <div class="card-body">
              <div class="section-body">
              <div class="card shadow">
                <div class="table-responsive p-2">
                  <table class="table align-items-center table-striped" id="example">
                    <thead class="thead-light">
                      <tr>
                        <th scope="col">รหัสสินค้า</th>
                        <th scope="col">ชื่อสินค้า</th>
                        <th scope="col">Brand สินค้า</th>
                        <th scope="col">ประเภทสินค้า</th>
                        <th scope="col">ขายราคาปกติ</th>
                        <th scope="col">ขายราคาลด</th>
                        <th scope="col">สถานะการขาย</th>
                        <th scope="col">สถานะสินค้า</th>
                        <th scope="col"></th>
                      </tr>
                    </thead>
                    <tbody>

                    <?php 
                      if(!empty($data['generalproducts'])):
                        foreach($data['generalproducts'] as $item):
                    ?>
                    <tr>
                      <th><?=$item->generalproducts_code? $item->generalproducts_code : ''?></th>
                      <th scope="row">
                        <div class="media align-items-center">
                          <div class="avatar rounded-circle mr-3">
                            <img alt="<?=$item->generalproducts_title? $item->generalproducts_title : ''?>" src="<?=base_url($item->generalproducts_img)?>" onerror="this.src='<?=base_url("upload/error/error.jpg");?>'">
                          </div>
                          <div class="media-body">
                            <span class="mb-0 text-sm"><?=$item->generalproducts_title? $item->generalproducts_title : ''?></span>
                          </div>
                        </div>
                      </th>
                      <th>
                        <?php 
                          $text_brands = '';
                          if(!empty($data['brands'])):
                            foreach($data['brands'] as $value):
                              if(!empty($item->generalproducts_brand) && $item->generalproducts_brand == $value->brands_no){
                                $text_brands = $value->brands_name;
                              }elseif( $item->generalproducts_brand == '0'){
                                $text_brands = 'ไม่มีแบรนด์';
                              }
                            endforeach;
                          endif;
                         ?>
                         <?=$text_brands?>
                      </th>
                      <th><?=$item->generalproducts_type? $item->generalproducts_type : ''?></th>
                      <th><?=$item->generalproducts_price != null? $item->generalproducts_price : ''?></th>
                      <th class="text-danger"><?=$item->generalproducts_discount != null? $item->generalproducts_discount : ''?></th>               
                      <td>
                        <?php
                          $id             = "";
                          $text_is_sale   = "";
                          $text_is_active = "";
                          $disabled       = ""; 

                          $id = base64_encode($item->generalproducts_no);
                          if($item->is_active < 1){
                            $text_is_active = 'checked';
                          }else{
                            $text_is_active = '';
                            $disabled        = 'disabled';
                          }

                          if($item->is_sale < 1){
                            $text_is_sale = 'checked';
                          }else{
                            $text_is_sale = '';
                          }
                        ?>
                        <label class="switch" for="sale_<?=$id?>">
                          <input type="checkbox" class="checkbox_sale" id="sale_<?=$id?>" data-code="<?=$id?>" <?=$text_is_sale?> <?=$disabled?>>
                          <div class="slider round"></div>
                        </label>
                      </td>
                      <td>
                        <label class="switch" for="active_<?=$id?>">
                          <input type="checkbox" class="checkbox_active" id="active_<?=$id?>" data-code="<?=$id?>" <?=$text_is_active?>>
                          <div class="slider round"></div>
                        </label>
                      </td>
                      <td class="text-center" width="90px">
                        <a href="javascript:void(0)" data-id="<?=$id?>" class="show-item" data-toggle="modal"><i class="fas fa-eye"></i></a>
                        <a href="javascript:void(0)" data-id="<?=$id?>" class="form-add-img" data-toggle="modal"><i class="fas fa-images"></i></a>
                        <a href="<?=base_url().'admin/generalproducts/edit/'.$id?>"><i class="fas fa-edit"></i></a>
                        <a href="javascript:void(0)" data-id="<?=$id?>" class="delete-item"><i class="fas fa-trash-alt"></i></a>
                      </td>
                    </tr>
                    <?php 
                        endforeach; 
                      endif; 
                    ?>
                    </tbody>
                    <tfoot class="thead-light">
                      <tr>
                        <th scope="col">รหัสสินค้า</th>
                        <th scope="col">ชื่อสินค้า</th>
                        <th scope="col">Brand สินค้า</th>
                        <th scope="col">ประเภทสินค้า</th>
                        <th scope="col">ขายราคาปกติ</th>
                        <th scope="col">ขายราคาลด</th>
                        <th scope="col">สถานะการขาย</th>
                        <th scope="col">สถานะสินค้า</th>
                        <th scope="col"></th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
               </div>
              </div>
            </div>
            <!-- <div class="card-footer bg-whitesmoke">
              This is card footer
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- modal เลือกการขาย -->
<div class="modal fade" tabindex="-1" role="dialog" id="checbox-sale">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">เลือกการขาย</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" id="customRadioInline1" name="checboxSale" class="custom-control-input checbox-sale" value='0' checked>
          <label class="custom-control-label" for="customRadioInline1">ขายปกติ</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" id="customRadioInline2" name="checboxSale" class="custom-control-input checbox-sale" value='1'>
          <label class="custom-control-label" for="customRadioInline2">ขายไม่ปกติ</label>
        </div>
      </div>
      <div class="modal-footer bg-whitesmoke br">
        <button type="button" class="btn btn-primary btn-checbox-sale">ตกลง</button>
      </div>
    </div>
  </div>
</div>

<!-- modal form เพิ่มรายละเอียดภาพสินค้า -->
<div class="modal fade" aria-labelledby="myLargeModalLabel" aria-hidden="true" role="dialog" id="imageDetail">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">เพิ่มรายละเอียดภาพสินค้า <small class="text-danger" ><b>(จำกัด 4 รูป)</b></small></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">      
        <article class="article article-style-b m-0">
          <div class="article-header" style="height: 100% !important;">
            <div class="container-fluid p-0">
              <div class="row pb-1">
                <div class="col-12">
                  <!-- รูปภาพตรงนี้ -->
                  <img id="text-img" src="" width="100%" onerror="this.src='<?=base_url("upload/error/error.jpg");?>'">
                </div>
              </div>
              </div>  
            </div>  
            <div width="100%">
              <p class="float-right text-primary" style="padding: 5px;position: absolute;bottom: 10px;right: 10px;background-color: black;border-radius: 30px;">
               <b><i class="fas fa-qrcode"></i></b> <b id="text-code">p0001</b>
              </p>
            </div>        
          </div>
          <div class="article-details mt-2">
              <div class="container">
                <div class="row text-center">
                  <div class="col-12 mb-2">
                    <div class="article-title ">
                      <div >
                        <b id="text-title" style="font-size:25px" >NIKYสวยงาม</b>
                        <br><small>( ตอนนี้มีอยู่แล้ว <span id="num-img"></span> รูป )</small>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col">
                    <hr style="width: 300px;">
                    <form id="form-submit-imgDetail" class="needs-validation" enctype="multipart/form-data" >
                      <input id="text-no" class="form-control" type="hidden" name="code" value="">
                      <div id="filemultiple-edit">
                      </div>
                      <div class="form-group">
                        <label>รูปภาพสินค้า <span class="text-danger">( รูปรายละเอียดมากสุด 4 รูป )</span></label>
                        <div class="file-loading"> 
                          <input id="input-b7" name="filemultiple[]" type="file" multiple="true">
                        </div>
                      </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 p-2 bg-whitesmoke d-flex justify-content-end">                     
                    <button type="button" class="btn btn-danger mr-2" data-dismiss="modal">ยกเลิก</button>
                    <button type="submit" class="btn btn-primary" id="btn-submit-item" data-loading-text="<i class='fa fa-spinner fa-pulse fa-fw'></i> กำลังบันทึกข้อมูล" >บันทึก</button>
                </div>
              </div>
                    </form>
          </div>
        </article>    
      </div>
    </div>
  </div>
</div>

<!-- modal show สินค้า -->
<div class="modal fade" tabindex="-1" role="dialog" id="exampleModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content" id="text-show-item">

    </div>
  </div>
</div>

<script>
  var post_url = '<?=base_url()?>api/admin'; 

  $(document).on('click', '.checkbox_sale', function () {
    var sale    = $(this).is(':checked');
    var generalproducts_code = $(this).attr('data-code');
		// $("#checbox-sale").modal();
    $('#checbox-sale').modal({backdrop: 'static', keyboard: false})  

      $(document).on('click', '.btn-checbox-sale', function () {  
        var value_sale    = $('input[name="checboxSale"]:checked').val();
        $.ajax({
          type: "POST",
          url: post_url + '/generalproducts/sale',
            data: {
              sale                    : sale,
              generalproducts_code  : generalproducts_code,
              value_sale              : value_sale
            },
            success: function (data) {
              if(data.status > 0){
                  iziToast.success({title: data.title,message: data.message,position: 'topRight'});
              }else{
                iziToast.error({title: data.title,message: data.message,position: 'topRight'});
              }
              $('#checbox-sale').modal('toggle');
              setTimeout(function(){
                location.reload(); 
              }, 5000);
            },
            error: function (data) {},
          });
      });

  });

  $(document).on('click', '.checkbox_active', function () {
    var active    = $(this).is(':checked');
    var generalproducts_code = $(this).attr('data-code');

    $.ajax({
    type: "POST",
    url: post_url + '/generalproducts/active',
      data: {
        active : active,
        generalproducts_code : generalproducts_code
      },
      success: function (data) {
        if(data.status > 0){
            iziToast.success({title: data.title,message: data.message,position: 'topRight'
          });
        }else{
          iziToast.error({title: data.title,message: data.message,position: 'topRight'
          });
        }
        setTimeout(function(){
            location.reload();
        }, 5000);    
      },
      error: function (data) {},
    });

  });

  $(document).on('click', '.delete-item', function () {
    var generalproducts_code = $(this).attr('data-id');
    swal({
      title: "ต้องการลบข้อมูล หรือไหม?",
      text: "กรุณากดปุมยืนยัน",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "ยืนยัน",
      cancelButtonClass: "btn-secondary",
      cancelButtonText: "ยกเลิก",
      closeOnConfirm: true,
      closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {   
        $.ajax({
        type: "POST",
        url: post_url + '/generalproducts/delete',
          data: {
            generalproducts_code : generalproducts_code,
          },
          success: function (data) {
            if(data.status > 0){
                iziToast.success({title: data.title,message: data.message,position: 'topRight'
              });
            }else{
              iziToast.error({title: data.title,message: data.message,position: 'topRight'
              });
            }
            setTimeout(function(){
                location.reload();
            }, 5000);    
          },
          error: function (data) {},
        });
      }
    });
  });

  $(document).on('click', '.show-item', function () {
    var html ='';
    var html_sale = '';
    var generalproducts_code = $(this).attr('data-id');
    
    $.ajax({
    type: "POST",
    url: post_url + '/generalproducts/showItem',
      data: {
        generalproducts_code : generalproducts_code
      },
      success: function (data) {
        if(data.is_sale == 1){
          html_sale = '<div class="article-badge-item bg-danger"><i class="fas fa-window-close"></i> ยังไม่ขาย</div>';
        }else{
          html_sale = '<div class="article-badge-item bg-success"><i class="fas fa-check-circle"></i> ขายแล้ว</div>';
        }

        // -----------------------------
        var img_detail=[];
        var x = 0;
        if(data.images_detail != ''){
          $.each(data.images_detail, function( index, value ) {
            img_detail[index] = '<?=base_url()?>'+value.generalproducts_img_detail_path; 
            x = index;
          });
          if(x != 3){
            var i;
            for (i = x; i < 3; ++i) {
              img_detail[i+1] = data.image_error;
            }
          }
        }else{
          var i;
          for (i = 0; i < 4; ++i) {
            img_detail[i] = data.image_error;
          }
        }

        // --------------------------------
        var img_head = '';

        //  -------header------------
        html +='<div class="modal-header">';
        html += '<h5 class="modal-title">รายละเอียดสินค้า</h5>';
          html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
            html += '<span aria-hidden="true">&times;</span>';
          html += '</button>';
        html += '</div>';
        // --------body------------
        html +='<div class="modal-body">';    
        html +='<article class="article article-style-b m-0">';
          html +='<div class="article-header" style="height: 100% !important;">';
            html +='<div class="container-fluid p-0">';
              html +='<div class="row pb-1">';
                html +='<div class="col-12">';
                  html +='<a data-fancybox="preview" href="'+data.generalproducts_img+'">';
                    html +='<img src="'+data.generalproducts_img+'" width="100%" onerror="this.src=\''+data.image_error+'\'">';
                  html +='</a>';
                html +='</div>';
              html +='</div>';
              html +='<div class="row">';
                html +='<div class="col-3 p-1">';
                  html +='<a href="'+img_detail[0]+'" data-fancybox="preview">';
                    html +='<img src="'+img_detail[0]+'" width="100%" onerror="this.src=\''+data.image_error+'\'">';
                  html +='</a>';
                html +='</div>';
                html +='<div class="col-3 p-1">';
                  html +='<a href="'+img_detail[1]+'" data-fancybox="preview">';
                    html +='<img src="'+img_detail[1]+'" width="100%" onerror="this.src=\''+data.image_error+'\'">';
                  html +='</a>';
                html +='</div>';
                html +='<div class="col-3 p-1">';
                  html +='<a href="'+img_detail[2]+'" data-fancybox="preview">';
                    html +='<img src="'+img_detail[2]+'" width="100%" onerror="this.src=\''+data.image_error+'\'">';
                  html +='</a>';
                html +='</div>';
                html +='<div class="col-3 p-1">';  
                  html +='<a href="'+img_detail[3]+'" data-fancybox="preview">';
                    html +='<img src="'+img_detail[3]+'" width="100%" onerror="this.src=\''+data.image_error+'\'">';
                  html +='</a>';
                html +='</div>';
              html +='</div>';  
            html +='</div>';  
            html +='<div width="100%">';
              html +='<div class="article-badge" style="position: unset !important;">';
                html += html_sale;
              html +='</div>';
              html +='<p class="float-right text-white" style="position: absolute;bottom: -15px;right: 10px;">';
                html +='<b><i class="fas fa-qrcode"></i> '+data.generalproducts_code+'</b>';
              html +='</p>';
            html +='</div>';        
          
          html +='</div>';
          html +='<div class="article-details mt-3">';
              html +='<div class="container">';
                html +='<div class="row text-center">';
                  html +='<div class="col-12">';
                    html +='<div class="article-title">';
                      html +='<div style="font-size:30px">';
                       html +='<b>'+data.generalproducts_title+'</b>';
                      html +='</div>';
                    html +='</div>';
                  html +='</div>';
                html +='</div>';
                html +='<div class="row">';
                  html +='<div class="col-6">';
                    html +='<p><b>ราคาขายลด</b> </p>';
                  html +='</div>';
                  html +='<div class="col-6">';
                    html +='<p class="text-primary"><b> ฿'+data.generalproducts_discount+'</b></p>';
                  html +='</div>';
                  html +='<div class="col-6">';
                    html +='<p><b>ราคาขายปกติ</b> </p>';
                  html +='</div>';
                  html +='<div class="col-6">';
                    html +='<p style="-webkit-text-decoration-line: line-through;text-decoration-line: line-through;"> ฿'+data.generalproducts_price+'</p>';
                  html +='</div>';
                html +='</div>';
                html +='<div class="row">';
                  html +='<div class="col-6">';
                    html +='<p><b>วันเวลาที่ขาย</b> </p>';
                  html +='</div>';
                  html +='<div class="col-6">';
                    html +='<p> '+data.system_sale_date+'</p>';
                  html +='</div>';
                html +='</div>';
              html +='</div>';
            html +='</div>';
          html +='</article>';    
        html +='</div>';
        // -------footr--------------
        html +='<div class="modal-footer bg-whitesmoke br d-flex justify-content-between">';
            html += '<p class="m-0"><i class="far fa-calendar-alt"></i> '+data.system_add_date+'</p>';
            html += '<p class="m-0"><i class="fas fa-eye"></i> '+data.number_view+'</p>';
        html+= '</div>';
        // console.log(html);
        $('#text-show-item').html(html);
      }
    });
		$("#exampleModal").modal()
  });

  // form เพิ่มรายละเอียดภาพสินค้า
  $(document).on('click', '.form-add-img', function () {
    var html ='';
    var generalproducts_code = $(this).attr('data-id');
    
    $.ajax({
    type: "POST",
    url: post_url + '/generalproducts/showItem',
      data: {
        generalproducts_code : generalproducts_code
      },
      success: function (data) {
        // console.log(data);
        $("#text-title").text(data.generalproducts_title);
        $("#text-code").text(data.generalproducts_code);
        $("#text-no").attr("value",generalproducts_code);
        $("#text-img").attr("src",data.generalproducts_img);
        $("#num-img").text(data.images_detail.length);

        if(data.images_detail.length > 0){
          $.each( data.images_detail, function( key, value ) {
            html +='<input class="form-control" type="hidden" name="filemultiple-edit[]" value="'+value.generalproducts_img_detail_path+'">';
          });
        }else{
          html ='<input class="form-control" type="hidden" name="filemultiple-edit[]" value="">';
        } 
         $("#filemultiple-edit input").remove();   
         $("#filemultiple-edit").append(html);   
      }
    });
		$("#imageDetail").modal()
  });

  //บันทึกเพิ่มรายละเอียดภาพสินค้า
  $("#form-submit-imgDetail").submit(function(e) {
    e.preventDefault();
    setButtonLoading("#btn-submit-item");

    $.ajax({
      type: "POST",
      url: post_url + '/generalproducts/addImgDetail',
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData:false,
      success: function(data){
        if(data.status > 0){
          iziToast.success({
            title: data.title,
            message: data.message,
            position: 'topRight'
          });
        }else{
          iziToast.error({
            title: data.title,
            message: data.message,
            position: 'topRight'
          });
        }
        setButtonReset("#btn-submit-item", 1000);
        setTimeout(function(){
          $('#imageDetail').modal('toggle');
          location.reload(); 
        }, 5000);
   
      },
      error: function (data) {
        console.log('An error occurred.');
        setButtonReset("#btn-submit-item", 1000);
      },
    });

  });


</script>