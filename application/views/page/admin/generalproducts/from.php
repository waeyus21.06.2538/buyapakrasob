<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1><?=$pagetitle?></h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="<?=$this->dasboard_url?>">แผนควบคุม</a></div>
        <div class="breadcrumb-item"><a href="<?=base_url().'admin/generalproducts'?>">สินค้าทั่วไป</a></div>
        <div class="breadcrumb-item"><?=$pagetitle?></div>
      </div>
    </div>
    <div class="section-body">
      <div class="row mt-sm-4">
        <div class="col-12 col-md-12">
          <div class="card">
            <!-- <form  id="form-submit-contact" class="needs-validation"> autocomplete="off" -->
            <form id="form-submit-contact" class="needs-validation" enctype="multipart/form-data" >
              <div class="card-header">
                <h4>แบบฟอร์ม</h4>
              </div>
              <div class="card-body">
                <div class="form-group">
                
            <?php 
             
              $text_generalproducts_code  ='';
              $text_disabled                ='';
              $required                     ='';
          
              if(!empty($mode) && $mode == 'edit'){
                $text_disabled = 'disabled';
              }else{
                $code = '';
                if(!empty($data['generalproducts_code']->generalproducts_code)){
                  $code = $data['generalproducts_code']->generalproducts_code;
                }else{
                  $code = 'กำหนดเอง';
                }
                $text_generalproducts_code  = "(รหัสที่ใช้ล่าสุด : <span class='text-primary'>".$code."</span> )";
                $required                     = 'required';
              }
            ?>
                  <label>รหัสสินค้า <?=$text_generalproducts_code?></label>
                    <input type="text" id="generalproducts_code" class="form-control" name="generalproducts_code" value="<?=!empty($data['generalproducts']->generalproducts_code)? $data['generalproducts']->generalproducts_code : '';?>" <?=$text_disabled ?> <?=$required?> >
            <?php if($mode == 'edit'){?><input type="hidden" name="generalproducts_code" value="<?=!empty($data['generalproducts']->generalproducts_code)? $data['generalproducts']->generalproducts_code : '';?>"> <?php }?>
                  <div class="invalid-feedback">
                    กรุณากรอกรหัสสินค้าด้วย
                  </div>
                </div>
                <div class="form-group">
                  <label>ชื่อสินค้า</label>
                    <input type="text" class="form-control" name="generalproducts_title" value="<?=!empty($data['generalproducts']->generalproducts_title)? $data['generalproducts']->generalproducts_title : '';?>"required>
                  <div class="invalid-feedback">
                    กรุณากรอกชื่อสินค้าด้วย
                  </div>
                </div>

            <?php 
              $selected_type = array('','');
              $selected_type_detail =  array('','','');
              $selected_brand = array('','','','');

              if($mode == 'edit'){

                 // ---------------generalproducts_type----------------------
                if($data['generalproducts']->generalproducts_type == "เสื้อ"){
                  $selected_type[0] ='selected';
                  $selected_type[1] ='';
                }
                elseif($data['generalproducts']->generalproducts_type == "กางเกง"){
                  $selected_type[0] ='';
                  $selected_type[1] ='selected';
                }

                // ----------------generalproducts_type_detail---------------------
                if($data['generalproducts']->generalproducts_type_detail == "เสื้อแขนสั่น"){
                  $selected_type_detail[0] ='selected';
                  $selected_type_detail[1] ='';
                  $selected_type_detail[2] ='';
                }
                elseif($data['generalproducts']->generalproducts_type_detail == "เสื้อแขนยาว"){
                  $selected_type_detail[0] ='';
                  $selected_type_detail[1] ='selected';
                  $selected_type_detail[2] ='';
                }
                elseif($data['generalproducts']->generalproducts_type_detail == "กางเกงขายาว"){
                  $selected_type_detail[0] ='';
                  $selected_type_detail[1] ='';
                  $selected_type_detail[2] ='selected';
                }

              }
            ?>

                <div class="form-group">
                  <label>ประเภทสินค้า</label>
                    <select class="form-control" name="generalproducts_type" required>
                      <option value="" >-- เลือก --</option>
                      <option value="เสื้อ" <?=$selected_type[0]?> >เสื้อ</option>
                      <option value="กางเกง" <?=$selected_type[1]?> >กางเกง</option>
                    </select>
                </div>
                <div class="form-group">
                  <label>ประเภทสินค้าอย่างละเอียด</label>
                    <select class="form-control" name="generalproducts_type_detail" required>
                      <option value="" >-- เลือก --</option>
                      <option value="เสื้อแขนสั่น" <?=$selected_type_detail[0]?> >เสื้อแขนสั่น</option>
                      <option value="เสื้อแขนยาว"<?=$selected_type_detail[1]?> >เสื้อแขนยาว</option>
                      <option value="กางเกงขายาว"<?=$selected_type_detail[2]?> >กางเกงขายาว</option>
                    </select>
                </div>
                <div class="form-group">
                  <label>Brand สินค้า</label>
                    <select class="form-control" name="generalproducts_brand" required>
                      <option value="" >-- เลือก --</option>
                      <?php
                        if(!empty($data['brands'])):
                          foreach($data['brands'] as $item):
                            $selected_brand = '';
                            if(!empty($data['generalproducts']->generalproducts_brand) && $data['generalproducts']->generalproducts_brand == $item->brands_no && $data['generalproducts']->generalproducts_brand != '0'){
                              $selected_brand ='selected';
                            }
                      ?>
                        <option value="<?=$item->brands_no?>" <?=$selected_brand?> ><?=$item->brands_name?></option>
                      <?php
                          endforeach;
                        endif;
                      ?>
                      <?php if($mode == 'edit'){?>
                        <option value="0" <?=$data['generalproducts']->generalproducts_brand == '0'? 'selected' : '' ?>>ไม่มีแบรนด์</option>
                      <?php }else{?>
                        <option value="0" >ไม่มีแบรนด์</option>
                      <?php }?>
                    </select>
                </div>
                <div class="form-group">
                  <label>ขายราคาปกติ</label>
                    <input type="number" class="form-control" name="generalproducts_price" value="<?=!empty($data['generalproducts']->generalproducts_price)? $data['generalproducts']->generalproducts_price : '0';?>">
                  <div class="invalid-feedback">
                    กรุณากรอกราคาขายด้วย
                  </div>
                </div>
                <div class="form-group">
                  <label>ขายราคาลด</label>
                    <input type="number" class="form-control" name="generalproducts_discount" value="<?=!empty($data['generalproducts']->generalproducts_discount)? $data['generalproducts']->generalproducts_discount : '0';?>"required>
                  <div class="invalid-feedback">
                    กรุณากรอกราคาลดด้วย
                  </div>
                </div>
                <div class="form-group">
                  <label>รายละเอียดสินค้า</label>
                    <textarea class="form-control summernote-1" name="generalproducts_detail" ><?=!empty($data['generalproducts']->generalproducts_detail)? html_entity_decode($data['generalproducts']->generalproducts_detail) : '';?></textarea>
                </div>

                <div class="form-group">
                  <?php if($mode == 'edit'){?>

                  <small class="d-flex justify-content-center">***( รูปภาพเก่า )***</small>
                  <div class="d-flex justify-content-center">
                    <a data-fancybox="preview" href="<?=base_url().$data['generalproducts']->generalproducts_img?>">
                      <img src="<?=base_url().$data['generalproducts']->generalproducts_img?>" / width="100px"  onerror="this.src='<?=base_url("upload/error/error.jpg");?>'">
                    </a>
                  </div> 
                  <input class="form-control" type="hidden" name="generalproducts_img_edit" value="<?=$data['generalproducts']->generalproducts_img?>">                  
                  
                  <?php }?>
                  <label>รูปภาพสินค้า ( <span class="text-danger">รูปหลัก 1 รูป</span> )</label>
                  <div class="file-loading"> 
                      <input id="input-b6" name="generalproducts_img" type="file" <?=$required?> >
                  </div>
                </div>
              </div>
              <div class="card-footer text-right">
                <input type="hidden" name="mode" value="<?=!empty($mode)? $mode : '' ?>">
                <input type="hidden" name="generalproducts_id" value="<?=!empty($data['generalproducts']->generalproducts_no)? $data['generalproducts']->generalproducts_no : '' ?>">
                <button type="submit" class="btn btn-primary" id="btn-submit-item" data-loading-text="<i class='fa fa-spinner fa-pulse fa-fw'></i> กำลังบันทึกข้อมูล" >บันทึก</button>            
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<script>

var post_url = "<?=$form_action?>";

// this is the id of the form
$("#form-submit-contact").submit(function(e) {
  e.preventDefault();
  setButtonLoading("#btn-submit-item");

  $.ajax({
    type: "POST",
    url: post_url,
    // data: $("#form-submit-contact").serialize(),
    data: new FormData(this),
    contentType: false,
    cache: false,
    processData:false,
    success: function(data){
      if(data.status > 0){
        //icon ,message ,title ,color
        iziToast.success({
          title: data.title,
          message: data.message,
          position: 'topRight'
        });
        setButtonReset("#btn-submit-item", 1000);
        setTimeout(function () {
          window.location.replace("<?=base_url()?>" + "admin/generalproducts");
        }, 5000);
      }else if(data.status < 0){
        iziToast.warning({
          title: data.title,
          message: data.message,
          position: 'topRight'
        });
        setButtonReset("#btn-submit-item", 1000);
        $('#generalproducts_code').focus();
      }else{
        iziToast.error({
          title: data.title,
          message: data.message,
          position: 'topRight'
        });
        setButtonReset("#btn-submit-item", 1000);
      }
    },
    error: function (data) {
      console.log('An error occurred.');
      setButtonReset("#btn-submit-item", 1000);
    },
  });
});

</script>