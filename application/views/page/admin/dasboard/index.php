<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="card card-statistic-2">
          <div class="card-stats">
            <div class="card-stats-title">ยอดขาย/ต่อปี =>
              <div class="dropdown d-inline">
                <a class="font-weight-600 dropdown-toggle" data-toggle="dropdown" href="#" id="orders-month">2020</a>
                <ul class="dropdown-menu dropdown-menu-sm">
                  <li class="dropdown-title">-- เลือกปี --</li>
                  <li><a href="#" class="dropdown-item active">2020</a></li>
                  <li><a href="#" class="dropdown-item">2021</a></li>
                  <li><a href="#" class="dropdown-item">2022</a></li>
                  <li><a href="#" class="dropdown-item">2023</a></li>
                  <li><a href="#" class="dropdown-item">2024</a></li>
                  <li><a href="#" class="dropdown-item">2025</a></li>
                  <li><a href="#" class="dropdown-item">2026</a></li>
                  <li><a href="#" class="dropdown-item">2027</a></li>
                  <li><a href="#" class="dropdown-item">2028</a></li>
                  <li><a href="#" class="dropdown-item">2029</a></li>
                  <li><a href="#" class="dropdown-item">2030</a></li>
                  <li><a href="#" class="dropdown-item">2031</a></li>
                </ul>
              </div>
            </div>
          </div> 
          <div class="card-chart m-1">
            <!-- <canvas id="balance-chart" height="80"></canvas> -->
            <canvas id="myChartProductSale"></canvas>
          </div>
          <!-- d-flex -->
          <div class="d-flex justify-content-between align-items-center">
            <div style="width: 480px;"> 
              <div class="card-icon shadow-primary bg-primary">
                <i class="fas fa-dollar-sign"></i>
              </div>
              <div class="card-wrap">
                <div class="card-header">
                  <h4>ยอดขายทั้งหมด</h4>
                </div>
                <div class="card-body">
                  <b id="sumallsale">0</b> บ.
                </div>
              </div>
            </div>
            <div class="m-1" style="width: 200px;">
              <small>ยอดขายปกติ <b><span id="sumallsaleNormal">0</span></b> บ.</small><br>
              <small>ยอดขายไม่ปกติ <b><span id="sumallsaleNONormal">0</span></b> บ.</small>
            </div>
          </div>
          <!-- End d-flex -->
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="card card-statistic-2">
          <div class="card-stats">
            <div class="card-stats-title">จำนวนขายสินค้า/ต่อปี =>
              <div class="dropdown d-inline">
                <a class="font-weight-600 dropdown-toggle" data-toggle="dropdown" href="#" id="orders-month">2020</a>
                <ul class="dropdown-menu dropdown-menu-sm">
                  <li class="dropdown-title">-- เลือกปี --</li>
                  <li><a href="#" class="dropdown-item active">2020</a></li>
                  <li><a href="#" class="dropdown-item">2021</a></li>
                  <li><a href="#" class="dropdown-item">2022</a></li>
                  <li><a href="#" class="dropdown-item">2023</a></li>
                  <li><a href="#" class="dropdown-item">2024</a></li>
                  <li><a href="#" class="dropdown-item">2025</a></li>
                  <li><a href="#" class="dropdown-item">2026</a></li>
                  <li><a href="#" class="dropdown-item">2027</a></li>
                  <li><a href="#" class="dropdown-item">2028</a></li>
                  <li><a href="#" class="dropdown-item">2029</a></li>
                  <li><a href="#" class="dropdown-item">2030</a></li>
                  <li><a href="#" class="dropdown-item">2031</a></li>
                </ul>
              </div>
            </div>
          </div> 
          <div class="card-chart m-1">
            <!-- <canvas id="sales-chart" height="80"></canvas> -->
            <canvas id="myChartProductGrade"></canvas>
          </div>
          <div class="d-flex justify-content-between align-items-center">
            <div style="width: 480px;"> <!--************************** -->
              <div class="card-icon shadow-primary bg-primary">
                <i class="fas fa-shopping-bag"></i>
              </div>
              <div class="card-wrap">
                <div class="card-header">
                  <h4>จำนวนทั้งหมดที่ขายได้</h4>
                </div>
                <div class="card-body">
                  <b id="countall">0</b> ตัว
                </div>
              </div>
            </div> <!--************************** -->
            <div class="m-1" style="width: 200px;">
              <small>เกรดA <b><span id="countallgradeA">0</span></b> ตัว</small><br>
              <small>เกรดB <b><span id="countallgradeB">0</span></b> ตัว</small>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card card-statistic-2">
        <div class="card-stats">
            <div class="card-stats-title">จำนวนสินค้าขายตามแบรนด์/ต่อปี =>
              <div class="dropdown d-inline">
                <a class="font-weight-600 dropdown-toggle" data-toggle="dropdown" href="#" id="orders-month">2020</a>
                <ul class="dropdown-menu dropdown-menu-sm">
                  <li class="dropdown-title">-- เลือกปี --</li>
                  <li><a href="#" class="dropdown-item active">2020</a></li>
                  <li><a href="#" class="dropdown-item">2021</a></li>
                  <li><a href="#" class="dropdown-item">2022</a></li>
                  <li><a href="#" class="dropdown-item">2023</a></li>
                  <li><a href="#" class="dropdown-item">2024</a></li>
                  <li><a href="#" class="dropdown-item">2025</a></li>
                  <li><a href="#" class="dropdown-item">2026</a></li>
                  <li><a href="#" class="dropdown-item">2027</a></li>
                  <li><a href="#" class="dropdown-item">2028</a></li>
                  <li><a href="#" class="dropdown-item">2029</a></li>
                  <li><a href="#" class="dropdown-item">2030</a></li>
                  <li><a href="#" class="dropdown-item">2031</a></li>
                </ul>
              </div>
            </div>
            <!-- <div class="card-stats-items">
              <div class="card-stats-item">
                <div class="card-stats-item-count">24</div>
                <div class="card-stats-item-label">Pending</div>
              </div>
              <div class="card-stats-item">
                <div class="card-stats-item-count">12</div>
                <div class="card-stats-item-label">Shipping</div>
              </div>
              <div class="card-stats-item">
                <div class="card-stats-item-count">23</div>
                <div class="card-stats-item-label">Completed</div>
              </div>
            </div> -->

            <canvas id="myChartNumberProductBrands" height="100"></canvas>   
          </div>
          <!-- <div class="card-icon shadow-primary bg-primary">
            <i class="fas fa-archive"></i>
          </div> -->
          <div class="card-wrap">
            <div class="card-header">
              <!-- <h4>จำนวนสินค้าทั้งหมด</h4> -->
            </div>
            <div class="card-body">
            <!-- <b id="totalproducts">59</b> ตัว -->
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="row">
      <div class="col-lg-6">
        <div class="card">
          <div class="card-header">
            <h4>จำนวนคนกดมาดูเว็บไซต์/ต่อวัน</h4>
            <div class="card-header-action">
              <div class="form-group">
                <label>เลือกวันที่</label>
                <input type="date" class="form-control" id="browser-date" value="<?=date('Y-m-d');?>">
              </div>
            </div>
          </div>
          <div class="card-body" id="graph-container-browser-date">
            <small>
              <b>จำนวนทั้งหมด : </b><span id="browser-countAll"></span> ครั้ง
            </small>
           
            <canvas id="myChartBrowserDate" height="158"></canvas>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="card">
          <div class="card-header">
            <h4>จำนวนคนกดมาดูไลฟ์สดจากหน้าเว็บไซต์</h4>
            <div class="card-header-action">
              <div class="form-group">
                <label>เลือกวันที่</label>
                <input type="date" class="form-control" id="lives-date" value="<?=date('Y-m-d');?>">
                <!-- <input type="datetime-local" class="form-control"> -->
              </div>
            </div>
          </div>
          <div class="card-body" id="graph-container">
            <small>
              <b>หัวข้อ : </b><span id="lives-title"></span>
            </small>
          
            <canvas id="myChartLives"></canvas>
          </div>
        </div>
        <!-- <div class="card gradient-bottom">
          <div class="card-header">
            <h4>Top 5 Products</h4>
            <div class="card-header-action dropdown">
              <a href="#" data-toggle="dropdown" class="btn btn-danger dropdown-toggle">Month</a>
              <ul class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                <li class="dropdown-title">Select Period</li>
                <li><a href="#" class="dropdown-item">Today</a></li>
                <li><a href="#" class="dropdown-item">Week</a></li>
                <li><a href="#" class="dropdown-item active">Month</a></li>
                <li><a href="#" class="dropdown-item">This Year</a></li>
              </ul>
            </div>
          </div>
          <div class="card-body" id="top-5-scroll">
            <ul class="list-unstyled list-unstyled-border">
              <li class="media">
                <img class="mr-3 rounded" width="55" src="<?=base_url()?>assets/admin/img/products/product-3-50.png" alt="product">
                <div class="media-body">
                  <div class="float-right"><div class="font-weight-600 text-muted text-small">86 Sales</div></div>
                  <div class="media-title">oPhone S9 Limited</div>
                  <div class="mt-1">
                    <div class="budget-price">
                      <div class="budget-price-square bg-primary" data-width="64%"></div>
                      <div class="budget-price-label">$68,714</div>
                    </div>
                    <div class="budget-price">
                      <div class="budget-price-square bg-danger" data-width="43%"></div>
                      <div class="budget-price-label">$38,700</div>
                    </div>
                  </div>
                </div>
              </li>
              <li class="media">
                <img class="mr-3 rounded" width="55" src="<?=base_url()?>assets/admin/img/products/product-4-50.png" alt="product">
                <div class="media-body">
                  <div class="float-right"><div class="font-weight-600 text-muted text-small">67 Sales</div></div>
                  <div class="media-title">iBook Pro 2018</div>
                  <div class="mt-1">
                    <div class="budget-price">
                      <div class="budget-price-square bg-primary" data-width="84%"></div>
                      <div class="budget-price-label">$107,133</div>
                    </div>
                    <div class="budget-price">
                      <div class="budget-price-square bg-danger" data-width="60%"></div>
                      <div class="budget-price-label">$91,455</div>
                    </div>
                  </div>
                </div>
              </li>
              <li class="media">
                <img class="mr-3 rounded" width="55" src="<?=base_url()?>assets/admin/img/products/product-1-50.png" alt="product">
                <div class="media-body">
                  <div class="float-right"><div class="font-weight-600 text-muted text-small">63 Sales</div></div>
                  <div class="media-title">Headphone Blitz</div>
                  <div class="mt-1">
                    <div class="budget-price">
                      <div class="budget-price-square bg-primary" data-width="34%"></div>
                      <div class="budget-price-label">$3,717</div>
                    </div>
                    <div class="budget-price">
                      <div class="budget-price-square bg-danger" data-width="28%"></div>
                      <div class="budget-price-label">$2,835</div>
                    </div>
                  </div>
                </div>
              </li>
              <li class="media">
                <img class="mr-3 rounded" width="55" src="<?=base_url()?>assets/admin/img/products/product-3-50.png" alt="product">
                <div class="media-body">
                  <div class="float-right"><div class="font-weight-600 text-muted text-small">28 Sales</div></div>
                  <div class="media-title">oPhone X Lite</div>
                  <div class="mt-1">
                    <div class="budget-price">
                      <div class="budget-price-square bg-primary" data-width="45%"></div>
                      <div class="budget-price-label">$13,972</div>
                    </div>
                    <div class="budget-price">
                      <div class="budget-price-square bg-danger" data-width="30%"></div>
                      <div class="budget-price-label">$9,660</div>
                    </div>
                  </div>
                </div>
              </li>
              <li class="media">
                <img class="mr-3 rounded" width="55" src="<?=base_url()?>assets/admin/img/products/product-5-50.png" alt="product">
                <div class="media-body">
                  <div class="float-right"><div class="font-weight-600 text-muted text-small">19 Sales</div></div>
                  <div class="media-title">Old Camera</div>
                  <div class="mt-1">
                    <div class="budget-price">
                      <div class="budget-price-square bg-primary" data-width="35%"></div>
                      <div class="budget-price-label">$7,391</div>
                    </div>
                    <div class="budget-price">
                      <div class="budget-price-square bg-danger" data-width="28%"></div>
                      <div class="budget-price-label">$5,472</div>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </div>
          <div class="card-footer pt-3 d-flex justify-content-center">
            <div class="budget-price justify-content-center">
              <div class="budget-price-square bg-primary" data-width="20"></div>
              <div class="budget-price-label">Selling Price</div>
            </div>
            <div class="budget-price justify-content-center">
              <div class="budget-price-square bg-danger" data-width="20"></div>
              <div class="budget-price-label">Budget Price</div>
            </div>
          </div>
        </div> -->
      </div>
    </div>

    <div class="row">
      <div class="col-lg-6">
        <div class="card">
          <div class="card-header">
             <h4>อุปกรณ์ที่ใช้</h4>
        </div>
        <div class="card-body">
          <canvas id="myChartPhone" height="158"></canvas>
        </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="card">
          <div class="card-header">
            <h4>จำนวนระบบที่ใช้งาน</h4>
          </div>
          <div class="card-body">
            <canvas id="myChartSystem" height="158"></canvas>
          </div>
        </div>
      </div>
      
    </div>
    
    <div class="row">
      <div class="col-lg-6">
        <div class="card">
          <div class="card-header">
            <h4>จำนวน Browser</h4>
          </div>
          <div class="card-body">
            <canvas id="myChartBrowser" height="158"></canvas>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
       
      </div>
    </div>

  </section>        
</div>

<script>

  /// parmiter 
  var post_url  = '<?=base_url()?>api/admin';
  var date_now  = '<?=date('Y-m-d');?>';
  var time      = ['01', '02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24'];
  var month     = ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.', ];
</script>

<!-- แสดงข้อมูลคนดูไลฟ์สดตามวันที่ที่เลือก -->
<script>
  //โหลดข้อมูลปัจจุบันก่อน
  load_statistic_lives(date_now);

  $(document).on('input','#lives-date', function(event){
    var date = $(this).val();

    //ลบก่อนเขียน
    $('#myChartLives').remove();
    $('#graph-container').append('<canvas id="myChartLives" height="158"><canvas>');
    $('#graph-container div.chartjs-size-monitor').remove();

    load_statistic_lives(date);
	});

  //แสดงข้อมูลคนดูไลฟ์สดตามวันที่ที่เลือก
  function load_statistic_lives(date){
    $('#lives-title').text('');
    $.ajax({
      type: "POST",
      url: post_url + '/statistics/das_viewLives',
        data: {
          date : date,
        },
        success: function (data) {
          if(data.status > 0){
            var ctx = document.getElementById('myChartLives').getContext('2d');
            var chart = new Chart(ctx, {
                type: 'line',
                // The data for our dataset
                data: {
                    labels: time,
                    datasets: [{
                        label: 'คนกด/ครั้ง ',
                        backgroundColor: 'rgb(75, 192, 192, 0.1)',
                        borderColor: 'rgb(75, 192, 192, 1.0)',
                        data: data.countDateTime
                    }]
                },
                // Configuration options go here
                options: {}
            });
            $('#lives-title').text(data.livesTitle);
            iziToast.success({title: 'อัพเดทข้อมูลเรียบร้อย',message: '',position: 'topRight'})
          }else{
            iziToast.error({title: 'เกิดข้อผิดพลาด', message: '', position: 'topRight'})
          }
        },
        error: function (data) {
          iziToast.error({title: 'เกิดข้อผิดพลาด', message: '', position: 'topRight'})
        },
    });

  }
</script>

<!-- แสดงข้อมูลคนดูบราวเซอร์ตามวันที่ที่เลือก -->
<script>
  load_statistic_browser_date(date_now);
  $(document).on('input','#browser-date', function(event){
    var date = $(this).val();

    //ลบก่อนเขียน
    $('#myChartBrowserDate').remove();
    $('#graph-container-browser-date').append('<canvas id="myChartBrowserDate" height="158"><canvas>');
    $('#graph-container-browser-date div.chartjs-size-monitor').remove();

    load_statistic_browser_date(date);
	});

  //แสดงข้อมูลคนดูบราวเซอร์ตามวันที่ที่เลือก
  function load_statistic_browser_date(date){
    $('#lives-title').text('');
    $.ajax({
      type: "POST",
      url: post_url + '/statistics/das_browser_date',
        data: {
          date : date,
        },
        success: function (data) {
          if(data.status > 0){
            var ctx = document.getElementById('myChartBrowserDate').getContext('2d');
            var chart = new Chart(ctx, {
                type: 'line',
                // The data for our dataset
                data: {
                    labels: time,
                    datasets: [{
                        label: 'คนกด/ครั้ง ',
                        backgroundColor: 'rgb(255, 99, 132, 0.1)',
                        borderColor: 'rgb(255, 99, 132, 1.0)',
                        data: data.countDateTime
                    }]
                },
                // Configuration options go here
                options: {}
            });
            $('#browser-countAll').text(data.CountAllBrowserDate);
            iziToast.success({title: 'อัพเดทข้อมูลเรียบร้อย',message: '',position: 'topRight'});
          }else{
            iziToast.error({title: 'เกิดข้อผิดพลาด', message: '', position: 'topRight'});
          }
        },
        error: function (data) {
          iziToast.error({title: 'เกิดข้อผิดพลาด', message: '', position: 'topRight'});
        },
    });

  }
</script>

<!-- แสดงข้อมูลอุปกรณ์ที่ใช้ -->
<script>
  var count_AlliPhone   = <?=$data['countAlliPhone']?>;
  var count_AlliPad     = <?=$data['countAlliPad']?>;
  var count_AllAndroid  = <?=$data['countAllAndroid']?>;
  var count_AllOrther   = <?=$data['countAllOrther']?>;

  var ctx = document.getElementById('myChartPhone').getContext('2d');
  var myChart = new Chart(ctx, {
      type: 'doughnut',
      data: {
          labels: ['โทรศัพท์ Android', 'โทรศัพท์ iPhone', 'iPad',  'คอมพิวเเตอร์,อื่นๆ'],
          datasets: [{
              label: 'จำนวน/ครั้ง ',
              data: [count_AllAndroid, count_AlliPhone, count_AlliPad, count_AllOrther,],
              backgroundColor: [
                  'rgb(255, 99, 132)',
                  'rgb(75, 192, 192)',
                  'rgb(153, 51, 153)',
                  'rgb(0, 153, 204)',
              ],
              borderColor: [
                  'rgb(255, 99, 132)',
                  'rgb(75, 192, 192)',
                  'rgb(153, 51, 153)',
                  'rgb(0, 153, 204)',
              ],
              borderWidth: 1
          }],
      },
      options: {}
  });
</script>

<!-- แสดงข้อมูลจำนวนระบบที่ใช้งาน -->
<script>
  // var count_AllSystem     = <?=$data['countAllSystem']?>; จำนวนทั้งหมด
  var count_SystemWindows = <?=$data['countSystemWindows']?>;
  var count_SystemiOS     = <?=$data['countSystemiOS']?>;
  var count_SystemAndroid = <?=$data['countSystemAndroid']?>;

  var ctx = document.getElementById('myChartSystem').getContext('2d');
  var myChart = new Chart(ctx, {
      type: 'pie',
      data: {
          labels: ['SystemAndroid', 'SystemiOS', 'SystemWindows'],
          datasets: [{
              label: 'จำนวน/ครั้ง ',
              data: [count_SystemAndroid, count_SystemiOS, count_SystemWindows,],
              backgroundColor: [
                  'rgb(255, 99, 132)',
                  'rgb(75, 192, 192)',
                  'rgb(0, 153, 204)', 
              ],
              borderColor: [
                  'rgb(255, 99, 132)',
                  'rgb(75, 192, 192)',
                  'rgb(0, 153, 204)',
              ],
              borderWidth: 1
          }],
      },
      options: {}
  });
</script>

<!-- แสดงข้อมูลจำนวน Browser ที่ใช้งาน -->
<script>
  var count_AllBrowser  = <?=$data['countAllBrowser']?>;
  var count_chrome      = <?=$data['countAllChrome']?>;
  var count_safari      = <?=$data['countAllSafari']?>;
  var count_firefox     = <?=$data['countAllFirefox']?>;

  // 'Green', 'Purple', 'Orange'
  var ctx = document.getElementById('myChartBrowser').getContext('2d');
  var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
          labels: ['chrome', 'safari', 'firefox', 'จำนวนทั้งหมด'],
          datasets: [{
              label: 'จำนวน/ครั้ง ',
              data: [count_chrome, count_safari, count_firefox, count_AllBrowser,],
              backgroundColor: [
                  'rgb(255, 99, 132)',
                  'rgb(255, 99, 132)',
                  'rgb(255, 99, 132)',
                  'rgb(75, 192, 192)',
                  // 'rgba(153, 102, 255, 0.2)',
                  // 'rgba(255, 159, 64, 0.2)'
              ],
              borderColor: [
                  'rgb(255, 99, 132)',
                  'rgb(255, 99, 132)',
                  'rgb(255, 99, 132)',
                  'rgb(75, 192, 192)',
                  // 'rgba(153, 102, 255, 1)',
                  // 'rgba(255, 159, 64, 1)'
              ],
              borderWidth: 1
          }],
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true
                  }
              }]
          }
      }
  });
</script>

<!-- ยอดขาย/ต่อปี -->
<script>
   $.ajax({
    type: "POST",
    url: post_url + '/statistics/das_sale_month',
      data: {
        date : '',
      },
      success: function (data) {
        if(data.status > 0){
          var ctx = document.getElementById('myChartProductSale').getContext('2d');
          var chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: month,
                datasets: [{
                    label: 'ขายปกติ ',
                    backgroundColor: 'rgb(75, 192, 192, 0.1)',
                    borderColor: 'rgb(75, 192, 192, 1.0)',
                    data: data.salenormal
                },{
                    label: 'ขายไม่ปกติ ',
                    backgroundColor: 'rgb(255, 99, 132, 0.1)',
                    borderColor: 'rgb(255, 99, 132, 1.0)',
                    data: data.saleNonormal
                }]
            },
            options: {}
          });
          $('#sumallsaleNormal').text(data.sumallsalenormal);
          $('#sumallsaleNONormal').text(data.sumallsaleNonormal);
          $('#sumallsale').text(data.sumallsale);
          // iziToast.success({title: 'อัพเดทข้อมูลเรียบร้อย',message: '',position: 'topRight'});
        }else{
          iziToast.error({title: 'เกิดข้อผิดพลาด', message: '', position: 'topRight'});
        }
      },
      error: function (data) {
        iziToast.error({title: 'เกิดข้อผิดพลาด', message: '', position: 'topRight'});
      },
  });
  
</script>

<!-- จำนวนขายสินค้า/ต่อปี  -->
<script>
  $.ajax({
    type: "POST",
    url: post_url + '/statistics/das_sale_prduct_grade_month',
      data: {
        date : '',
      },
      success: function (data) {
        if(data.status > 0){
          var ctx = document.getElementById('myChartProductGrade').getContext('2d');
          var chart = new Chart(ctx, {
              type: 'line',
              data: {
                  labels: month,
                  datasets: [{
                      label: 'เกรด A ',
                      backgroundColor: 'rgb(75, 192, 192, 0.1)',
                      borderColor: 'rgb(75, 192, 192, 1.0)',
                      data: data.countMonthGradeA
                  }, {

                      label: 'เกรด B ',
                      backgroundColor: 'rgb(255, 99, 132, 0.1)',
                      borderColor: 'rgb(255, 99, 132, 1.0)',
                      data: data.countMonthGradeB
                  }]       
              },
              options: {}
          });
          $('#countallgradeA').text(data.countAllGradeA);
          $('#countallgradeB').text(data.countAllGradeB);
          $('#countall').text(data.countAll);
          // iziToast.success({title: 'อัพเดทข้อมูลเรียบร้อย',message: '',position: 'topRight'});
        }else{
          iziToast.error({title: 'เกิดข้อผิดพลาด', message: '', position: 'topRight'});
        }
      },
      error: function (data) {
        iziToast.error({title: 'เกิดข้อผิดพลาด', message: '', position: 'topRight'});
      },
  });
</script>

<!-- จำนวนสินค้าขายตามแบรนด์ -->
<script>
  $.ajax({
    type: "POST",
    url: post_url + '/statistics/das_sale_brands',
      data: {
        date : '',
      },
      success: function (data) {
        console.log(data);
        if(data.status > 0){
          var ctx = document.getElementById('myChartNumberProductBrands').getContext('2d');
          var brand = data.brands;
          var chart = new Chart(ctx, {
              type: 'bar',
              data: {
                  labels: brand,
                  datasets: [{
                      label: 'เกรด A ',
                      backgroundColor: 'rgb(75, 192, 192)',
                      borderColor: 'rgb(75, 192, 192)',
                      data: data.brandsgradeA
                  }, {

                      label: 'เกรด B ',
                      backgroundColor: 'rgb(255, 99, 132)',
                      borderColor: 'rgb(255, 99, 132)',
                      data: data.brandsgradeB
                  }]       
              },
              options: {}
          });
          $('#countallgradeA').text(data.countAllGradeA);
          $('#countallgradeB').text(data.countAllGradeB);
          $('#countall').text(data.countAll);
          // iziToast.success({title: 'อัพเดทข้อมูลเรียบร้อย',message: '',position: 'topRight'});
        }else{
          iziToast.error({title: 'เกิดข้อผิดพลาด', message: '', position: 'topRight'});
        }
      },
      error: function (data) {
        iziToast.error({title: 'เกิดข้อผิดพลาด', message: '', position: 'topRight'});
      },
  });
</script>


