<?php
$message		= $this->session->flashdata('message');
$message_title = $this->session->flashdata('message_title');
$message_check = $this->session->flashdata('message_check');
?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
		<?=$pagetitle?>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="box">
            <!-- /.box-header -->
            <div class="box-body">
				<form id="form-submit-food" class="form-horizontal" action="#" method="POST" enctype="multipart/form-data" autocomplete="off">
					<div class="box-body">
						<div class="form-group">
							<label class="col-sm-2 control-label">ประเภทผู้ใช้งาน <span class="text-red">*</span></label>
							<div class="col-sm-3">
								<select id="user_type_code" class="form-control select2" name="user_type_code" style="width: 100%;">
									<option value="" selected="selected">เลือกประเภทผู้ใช้งาน</option>
									<?php
									if(isset($data['user_type'])){
										foreach($data['user_type'] as $type){
											echo '<option value="'.$type['user_type_code'].'">'.$type['user_type_name'].'</option>';
										}
									}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">ชื่อ-สกุล </label>
							<div class="col-sm-3">
								<input type="text" id="user_name" class="form-control" name="user_name" value="" placeholder="กรุณาระบุชื่อ-สกุล ">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">ชื่อผู้ใช้งาน <span class="text-red">*</span></label>
							<div class="col-sm-3">
								<input type="text" id="user_login_name" class="form-control" name="user_login_name" value="" placeholder="กรุณาระบุชื่อผู้ใช้งาน ">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">รหัสผ่าน <span class="text-red">*</span></label>
							<div class="col-sm-3">
								<input type="password" id="user_login_pass" class="form-control" name="user_login_pass" value="" placeholder="กรุณาระบุรหัสผ่าน ">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">ยืนยันรหัสผ่าน <span class="text-red">*</span></label>
							<div class="col-sm-3">
								<input type="password" id="user_login_pass_cf" class="form-control" name="user_login_pass_cf" value="" placeholder="กรุณาระบุยืนยันรหัสผ่าน ">
							</div>
						</div>
					</div>
				<!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" id="btn-submit-item" class="btn btn-success btn-flat" data-loading-text="<i class='fa fa-spinner fa-spin '></i> กำลังบันทึกข้อมูล">
							<i class="fa fa-floppy-o" aria-hidden="true"></i> บันทึกข้อมูล</button>
						<a href="<?=base_url();?>admin/users" class="btn btn-default btn-flat">
							<i class="fa fa-chevron-left" aria-hidden="true"></i> ย้อนกลับ</a>
					</div>
				<!-- /.box-footer -->
				</form>
            </div>
            <!-- /.box-body -->
        </div>
          <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<script>
	/// parmiter 
	var post_url = '<?=base_url()?>api';
   
   // this is the id of the form
   $("#form-submit-food").submit(function(e) {
	   e.preventDefault();
	   // set funtion loading button > class or id
	   setButtonLoading("#btn-submit-food");
	   $.ajax({
		   type: "POST",
		   url: post_url+'/users/add',
		   data: $("#form-submit-food").serialize(),
		   success: function(data){
			   if(data.status > 0){
				   //icon ,message ,title ,color
				   load_notify('', data.message, data.title, data.warning);
				   //set value to null
				   setInputValue();
				   // set funtion reset button > class or id and time
				   setButtonReset("#btn-submit-item",1000);
			   }else{
				   //icon ,message ,title ,color
				   load_notify('', data.message, data.title, data.warning);
				   // set funtion reset button > class or id and time
				   setButtonReset("#btn-submit-item",1000);
			   }
		   },
		   error: function (data) {
			   console.log('An error occurred.');
			   // set funtion reset button > class or id and time
			   setButtonReset("#btn-submit-item",1000);
		   },
	   });
   });

   function setInputValue(){

	   $('#user_name').val('');
	   $('#user_login_name').val('');
	   $('#user_login_pass_cf').val('');
	   $('#user_login_pass').val('');
	   $('#org_code').val(null).trigger('change');
	   $('#user_type_code').val(null).trigger('change');
   }
</script>
