
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
		<?=$pagetitle?>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="box">

            <div class="box-header">
				<a href="<?=base_url();?>admin/users/add" class="btn btn-success btn-flat"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มข้อมูล</a>
				<button type="button" id="btn-delete-item" class="btn btn-danger btn-flat hidden"><i class="fa fa-trash-o" aria-hidden="true"></i> ลบข้อมูล</button>
				
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form id="form-search-reservation" class="form-horizontal" action="#" method="POST" autocomplete="off">
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-4"> 
                            <h4><b>ค้นหาข้อมูล</b></h4>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="user_name" class="col-sm-4 control-label">ชื่อผู้ใช้งาน</label>
                        <div class="col-sm-3">
                            <input type="text" id="user_name" class="form-control pull-right" name="user_name" placeholder="กรุณาระบุชื่อผู้ใช้งาน">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-4">
                            <button type="button" id="btn-search-reservation" class="btn btn-primary btn-flat" data-loading-text="<i class='fa fa-spinner fa-spin '></i> กำลังโหลดข้อมูล">
                            <i class="fa fa-search" aria-hidden="true"></i> ค้นหาข้อมูล</button>
                        </div>
                    </div>
                </form>
                <hr>
				<table id="table-disply-list" class="table table-bordered table-striped table-item">
                <thead>
					<tr>
						<th style="width:5%">
							<label for="minimal">
								<input type="checkbox" id="select-all" class="minimal">
							</label>
						</th>
						<th style="width:20%">ชื่อผู้ใช้งาน</th>
                        <th style="width:5%">ประเภทผู้ใช้งาน</th>
                        <th style="width:5%">วันที่สร้าง</th>
                        <th style="width:20%">สถานะ</th>
						<th style="width:5%">Action</th>
					</tr>
                </thead>
                <tbody>
				</tbody>
				</table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  
   <div class="modal fade" id="modal-confirm-delete">
		<div class="modal-dialog">
			<div class="modal-content">
			<form id="form-submit-delete" class="form-horizontal" action="#" method="POST">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">ยืนยันการลบข้อมูล</h4>
				</div>
				<div class="modal-body">
					<h4 class="text-center">รายการที่ต้องการลบจำนวน <small id="text-total-delete-item" class="label bg-red">0</small> รายการ</h4>
					<ul id="text-confirm-item-name" class="nav nav-pills nav-stacked">
						
					</ul>
				</div>
				<div class="modal-footer">
					<button type="submit" id="btn-submit-delete" class="btn btn-success btn-flat" data-loading-text="<i class='fa fa-spinner fa-spin '></i> กำลังบันทึกข้อมูล">
						<i class="fa fa-check-square-o" aria-hidden="true"></i> ยืนยันการลบ</button>
					<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i> ปิดหน้าต่าง</button>
				</div>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
		
  <script>
	/// parmiter 
	var post_url = '<?=base_url()?>api/admin';
    
    $(document).on('click','#btn-search-reservation', function(event){
        setButtonLoading("#btn-search-reservation");
        load_datatable();
	});

	$(document).on('ifChecked','#select-all', function(event){
		$('.table-item tbody .select-by-item').iCheck('check');
		$('#btn-delete-item').removeClass('hidden');
	});
	
	$(document).on('ifUnchecked','#select-all', function (event) {
		$('.table-item tbody .select-by-item').iCheck('uncheck');
		$('#btn-delete-item').addClass('hidden');
	});
	
	$(document).on('ifChecked','.table-item tbody .select-by-item', function(event){
		if($('.table-item tbody .select-by-item').filter(":checked").length > 0){
			$('#btn-delete-item').removeClass('hidden');
		}
	});
	
	$(document).on('ifUnchecked','.table-item tbody .select-by-item', function (event) {
		if($('.table-item tbody .select-by-item').filter(":checked").length < 1){
			$('#btn-delete-item').addClass('hidden');
		}
	});
	
	$('#btn-delete-item').click(function(){
		var html ='';
		$('#text-total-delete-item').text($('.table-item tbody .select-by-item').filter(":checked").length);
		$($('.table-item tbody .select-by-item').filter(":checked")).each(function(){
			html += '<li>';
				html += '<a href="javascript:void(0)">';
					html += '<i class="fa fa-check-square-o text-light-blue"></i> '+$(this).attr('data-name-item');
				html += '</a>';
				html += '<input type="hidden" name="item_code[]" value="'+$(this).val()+'">';
			html += '</li>';
		});
		
		$('#text-confirm-item-name').html(html);
		$('#modal-confirm-delete').modal('show');
	});
    
     // this is the id of the form
    $("#form-submit-delete").submit(function(e) {
        e.preventDefault();
        // set funtion loading button > class or id
        setButtonLoading("#btn-submit-delete");
        $.ajax({
            type: "POST",
            url: post_url+'/users/deletes',
            data: $("#form-submit-delete").serialize(),
            success: function(data){
                if(data.status > 0){
                    //icon ,message ,title ,color
                    load_notify('', data.message, data.title, data.warning);
                    load_datatable();
                    $('#modal-confirm-delete').modal('hide');
                    // set funtion reset button > class or id and time
                    setButtonReset("#btn-submit-delete",1000);
                    
                }else{
                    //icon ,message ,title ,color
                    load_notify('', data.message, data.title, data.warning);
                    // set funtion reset button > class or id and time
                    setButtonReset("#btn-submit-delete",1000);
                }
            },
            error: function (data) {
                console.log('An error occurred.');
                // set funtion reset button > class or id and time
                setButtonReset("#btn-submit-delete",1000);
            },
        });
    });
    
    load_datatable();
    
    function load_datatable(){
        var user_name  = $('#user_name').val();
        var table = $('#table-disply-list').DataTable();
        table.destroy();
        table = $('#table-disply-list').DataTable({
            "pageLength"	: 10,
            "searching" 	: false,
            "lengthChange"	: true,
            "processing"	: true,
            "serverSide"	: true,
            "scrollX"       : true,
            "order"			: [],
            "ajax"			: {
                "url"	: post_url+'/users/view',
                "type"	: "POST",
                "data"	: {
                    user_name : user_name
                }
                ,complete: function() {
                    load_checkbox();
                    setButtonReset("#btn-search-reservation",1000);
                }
            },
            "columns": [
                 { "width": "1%","data": "CHECKBOK"}
                ,{ "width": "15%","data": "user_name"}
                ,{ "width": "10%","data": "user_type_name"}
                ,{ "width": "7%","data": "update_date"}
                ,{ "width": "5%","data": "status_name"}
                ,{ "width": "1%","data": "action"}
            ],"columnDefs": [{
                "targets": [0,1,2,3,4,5],
                "orderable": false,
                "data": null,
                "className": "",
                "render": function (data, type, row, meta) {
                    return data;
                  }
                }],
            "initComplete": function (settings, json) {
                $('div.loading').remove();
            }
            
        });

    }

    function load_checkbox(){
        
        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass   : 'iradio_minimal-blue'
        });
    
    }
  </script>
 

