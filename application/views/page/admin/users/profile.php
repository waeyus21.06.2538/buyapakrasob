<!-- Main Content -->
<div class="main-content">
	<section class="section">
		<div class="section-header">
			<h1><?=$pagetitle?></h1>
			<div class="section-header-breadcrumb">
				<div class="breadcrumb-item active"><a href="<?=$this->dasboard_url?>">แผนควบคุม</a></div>
				<div class="breadcrumb-item"><?=$pagetitle?></div>
			</div>
		</div>
		<div class="section-body">
			<h2 class="section-title">แวยูรี มูซอ</h2>
			<p class="section-lead">
				ตำแหน่ง : ผู้พัฒนาระบบ Buya-Pakrasob
			</p>

			<div class="row mt-sm-4">
				<div class="col-12 col-md-12 col-lg-5">
					<div class="card profile-widget">
						<div class="profile-widget-header">
							<img alt="image" src="<?=base_url()?>assets/admin/img/avatar/avatar-1.png" class="rounded-circle profile-widget-picture">
							<div class="profile-widget-items">
								<!-- <div class="profile-widget-item">
									<div class="profile-widget-item-label">Posts</div>
									<div class="profile-widget-item-value">187</div>
								</div>
								<div class="profile-widget-item">
									<div class="profile-widget-item-label">Followers</div>
									<div class="profile-widget-item-value">6,8K</div>
								</div>
								<div class="profile-widget-item">
									<div class="profile-widget-item-label">Following</div>
									<div class="profile-widget-item-value">2,1K</div>
								</div> -->
							</div>
						</div>
						<div class="profile-widget-description">
								<div class="profile-widget-name">
									<!-- Ujang Maman 
									<div class="text-muted d-inline font-weight-normal">
										<div class="slash"></div>
										Web Developer
									</div> -->
							</div>
							<!-- Ujang maman is a superhero name in <b>Indonesia</b>, especially in my family. He is not a fictional
							character but an original hero in my family, a hero for his children and for his wife. So, I use the name
							as a user in this template. Not a tribute, I'm just bored with <b>'John Doe'</b>. -->
						</div>
					</div>
				</div>
				<div class="col-12 col-md-12 col-lg-7">
					<div class="card">
						<form method="post" class="needs-validation" novalidate="">
							<div class="card-header">
								<h4>แก้ไขข้อมูลผู้ใช้งาน</h4>
							</div>
							<div class="card-body">
								<div class="row">
									<div class="form-group col-md-6 col-12">
										<label>First Name</label>
										<input type="text" class="form-control" value="แวยูรี" required="">
										<div class="invalid-feedback">
											Please fill in the first name
										</div>
									</div>
									<div class="form-group col-md-6 col-12">
										<label>Last Name</label>
										<input type="text" class="form-control" value="มูซอ" required="">
										<div class="invalid-feedback">
											Please fill in the last name
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-md-6 col-12">
										<label>Email</label>
										<input type="email" class="form-control" value="waeyus21.06.2538@gmail.com" required="">
										<div class="invalid-feedback">
											Please fill in the email
										</div>
									</div>
									<div class="form-group col-md-6 col-12">
										<label>Phone</label>
										<input type="tel" class="form-control" value="092-996-5667">
									</div>
								</div>
							</div>
							<div class="card-footer text-right">
								<!-- <button class="btn btn-primary">Save Changes</button> -->
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>