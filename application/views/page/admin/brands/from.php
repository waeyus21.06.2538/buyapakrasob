<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1><?=$pagetitle?></h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="<?=$this->dasboard_url?>">แผนควบคุม</a></div>
        <div class="breadcrumb-item"><a href="<?=base_url().'admin/brands'?>">แบรนด์สินค้า</a></div>
        <div class="breadcrumb-item"><?=$pagetitle?></div>
      </div>
    </div>
    <div class="section-body">
      <div class="row mt-sm-4">
        <div class="col-12 col-md-12">
          <div class="card">
            <!-- <form  id="form-submit-contact" class="needs-validation"> autocomplete="off" -->
            <form id="form-submit-brands" class="needs-validation" enctype="multipart/form-data">
              <div class="card-header">
                <h4>แบบฟอร์ม</h4>
              </div>
              <div class="card-body">
          
                <div class="form-group">
                  <label>ชื่อแบรนด์สินค้า</label>
                    <input type="text" class="form-control" name="brands_name" value="<?=!empty($data['brands']->brands_name)? $data['brands']->brands_name : '';?>"required>
                  <div class="invalid-feedback">
                    กรุณากรอกชื่อแบรนด์สินค้าด้วย
                  </div>
                </div>

                <div class="form-group">
                  <?php if($mode == 'edit'){?>

                  <small class="d-flex justify-content-center">***( รูปภาพเก่า )***</small>
                  <div class="d-flex justify-content-center">
                    <a data-fancybox="preview" href="<?=base_url().$data['brands']->brands_img?>">
                      <img src="<?=base_url().$data['brands']->brands_img?>" / width="150px"  onerror="this.src='<?=base_url("upload/error/error.jpg");?>'">
                    </a>
                  </div> 
                  <input class="form-control" type="hidden" name="brands_img_edit" value="<?=$data['brands']->brands_img?>">                  
                  
                  <?php }?>
                  <label>รูปภาพแบรนด์สินค้า ( <span class="text-danger">รูปหลัก 1 รูป</span> )</label>
                  <div class="file-loading"> 
                      <input id="input-b6" name="brands_img" type="file">
                  </div>
                </div>
              </div>
              <div class="card-footer text-right">
                <input type="hidden" name="mode" value="<?=!empty($mode)? $mode : '' ?>">
                <input type="hidden" name="brands_no" value="<?=!empty($data['brands']->brands_no)? $data['brands']->brands_no : '' ?>">
                <button type="submit" class="btn btn-primary" id="btn-submit-item" data-loading-text="<i class='fa fa-spinner fa-pulse fa-fw'></i> กำลังบันทึกข้อมูล" >บันทึก</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<script>

var post_url = "<?=$form_action?>";

// this is the id of the form
$("#form-submit-brands").submit(function(e) {
  e.preventDefault();
  setButtonLoading("#btn-submit-item");

  $.ajax({
    type: "POST",
    url: post_url,
    // data: $("#form-submit-brands").serialize(),
    data: new FormData(this),
    contentType: false,
    cache: false,
    processData:false,
    success: function(data){
      if(data.status > 0){
        //icon ,message ,title ,color
        iziToast.success({
          title: data.title,
          message: data.message,
          position: 'topRight'
        });
        // setInputValue();
        setButtonReset("#btn-submit-item", 1000);
        setTimeout(function () {
          window.location.replace("<?=base_url()?>" + "admin/brands");
        }, 5000);
      }else{
        iziToast.error({
          title: data.title,
          message: data.message,
          position: 'topRight'
        });
        setButtonReset("#btn-submit-item", 1000);
      }
    },
    error: function (data) {
      console.log('An error occurred.');
      setButtonReset("#btn-submit-item", 1000);
    },
  });
});

</script>