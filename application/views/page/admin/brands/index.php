<style>
.switch {
    display: inline-block;
    height: 22px;
    position: relative;
    width: 42px;
}

.switch input {
	display:none;
}

.slider {
	background-color: #ccc;
	bottom: 0;
	cursor: pointer;
	left: 0;
	position: absolute;
	right: 0;
	top: 0;
	transition: .4s;
}

.slider:before {
    background-color: #fff;
    bottom: 2px;
    content: "";
    height: 18px;
    left: 2px;
    position: absolute;
    transition: .4s;
    width: 18px;
}

input:checked + .slider {
	background-color: #66bb6a;
}

input:checked + .slider:before {
	transform: translateX(20px);
}

.slider.round {
	border-radius: 34px;
}

.slider.round:before {
	border-radius: 50%;
}
</style>


<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1><?=$pagetitle?></h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="<?=$this->dasboard_url?>">แผนควบคุม</a></div>
        <div class="breadcrumb-item"><?=$pagetitle?></div>
      </div>
    </div>
    <div class="section-body">
      <div class="row mt-sm-4">
        <div class="col-12 col-md-12">
          <div class="card">
            <div class="card-header">
              <!-- <h4>รายละเอียดติดต่อเรา</h4> -->
              <div class="custom-btn-action">
                <a href="<?=$data['url_create']?>" class="btn btn-success m-1"><i class="fas fa-plus"></i> เพิ่มแบรนด์สินค้า</a>
              </div>
            </div>
            <div class="card-body">
              <div class="section-body">
              <div class="card shadow">
                <div class="table-responsive p-2">
                  <table class="table align-items-center table-striped" id="example">
                    <thead class="thead-light">
                      <tr>
                        <th scope="col">รูป</th>
                        <th scope="col">ชื่อแบรนด์</th>
                        <th scope="col">สถานะแบรนด์</th>
                        <th scope="col"></th>
                        <th scope="col">ปี-เดือน-วัน เวลา ( เพิ่ม )</th>
                        <th scope="col">ปี-เดือน-วัน เวลา ( แก้ไข )</th>
                      </tr>
                    </thead>
                    <tbody>

                    <?php 
                      if(!empty($data['brands'])):
                        foreach($data['brands'] as $item):
                    ?>
                    <tr>
                      <th>
                      <div class="media align-items-center">
                          <a data-fancybox="preview" href="<?=base_url($item->brands_img)?>">
                            <img alt="<?=$item->brands_name? $item->brands_name : ''?>" src="<?=base_url($item->brands_img)?>" width="80" onerror="this.src='<?=base_url("upload/error/error.jpg");?>'">
                          </a>
                        </div>
                      </th>
                      <th scope="row">
                        <div class="media align-items-center">
                          <div class="media-body">
                            <span class="mb-0 text-sm"><?=$item->brands_name? $item->brands_name : ''?></span>
                          </div>
                        </div>
                      </th>              
                      <td>
                      <?php

                        $id = base64_encode($item->brands_no);
                        if($item->is_active < 1){
                          $text_is_active = 'checked';
                        }else{
                          $text_is_active = '';
                          $disabled        = 'disabled';
                        }
                      ?>
                        <label class="switch" for="active_<?=$id?>">
                          <input type="checkbox" class="checkbox_active" id="active_<?=$id?>" data-id="<?=$id?>" <?=$text_is_active?>>
                          <div class="slider round"></div>
                        </label>
                      </td>
                      <td class="text-center" width="90px">
                        <a href="<?=base_url().'admin/brands/edit/'.$id?>"><i class="fas fa-edit"></i></a>
                        <a href="javascript:void(0)" data-id="<?=$id?>" class="delete-item"><i class="fas fa-trash-alt"></i></a>
                      </td>
                      <td><?=$item->system_add_date? $item->system_add_date : ''?></td>
                      <td><?=$item->system_update_date? $item->system_update_date : ''?></td>
                    </tr>
                    <?php 
                        endforeach; 
                      endif; 
                    ?>
                    </tbody>
                    <tfoot class="thead-light">
                      <tr>
                        <th scope="col">รูป</th>
                        <th scope="col">ชื่อแบรนด์</th>
                        <th scope="col">สถานะแบรนด์</th>
                        <th scope="col"></th>
                        <th scope="col">ปี-เดือน-วัน เวลา ( เพิ่ม )</th>
                        <th scope="col">ปี-เดือน-วัน เวลา ( แก้ไข )</th>
                       
                      </tr>
                    </tfoot>
                  </table>
                </div>
               </div>
              </div>
            </div>
            <!-- <div class="card-footer bg-whitesmoke">
              This is card footer
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </section>
</div>


<script>
  var post_url = '<?=base_url()?>api/admin'; 

  $(document).on('click', '.checkbox_active', function () {
    var active    = $(this).is(':checked');
    var brands_no = $(this).attr('data-id');

    $.ajax({
    type: "POST",
    url: post_url + '/brands/active',
      data: {
        active : active,
        brands_no : brands_no
      },
      success: function (data) {
        if(data.status > 0){
            iziToast.success({title: data.title,message: data.message,position: 'topRight'
          });
        }else{
          iziToast.error({title: data.title,message: data.message,position: 'topRight'
          });
        }
        setTimeout(function(){
            location.reload();
        }, 5000);    
      },
      error: function (data) {},
    });

  });

  $(document).on('click', '.delete-item', function () {
    var brands_no = $(this).attr('data-id');
    swal({
      title: "ต้องการลบข้อมูล หรือไหม?",
      text: "กรุณากดปุมยืนยัน",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "ยืนยัน",
      cancelButtonClass: "btn-secondary",
      cancelButtonText: "ยกเลิก",
      closeOnConfirm: true,
      closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {   
        $.ajax({
        type: "POST",
        url: post_url + '/brands/delete',
          data: {
            brands_no : brands_no,
          },
          success: function (data) {
            if(data.status > 0){
                iziToast.success({title: data.title,message: data.message,position: 'topRight'
              });
            }else{
              iziToast.error({title: data.title,message: data.message,position: 'topRight'
              });
            }
            setTimeout(function(){
                location.reload();
            }, 5000);    
          },
          error: function (data) {},
        });
      }
    });
  });

</script>