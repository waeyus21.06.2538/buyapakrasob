<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>ระบบจัดการ | Buya ผ้ากระสอบ</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

<!-- General CSS Files -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />

<!-- Theme style / Template CSS  -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/style.css">
<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/components.css">
<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/custom.css">
<!-- <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/AdminLTE.css"> -->

<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<!-- iziToast -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css" rel="stylesheet">
<!-- fancybox -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css" rel="stylesheet">
<!-- bootstrap-sweetalert -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" rel="stylesheet">

<!-- bootstrap notify master -->
<!-- <script src="<?=base_url()?>assets/bower_components/bootstrap-notify-master/js/bootstrap-notify.js"></script> -->

<!-- General JS Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

<!-- chart -->
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>




</head>
<body>

<div class="main-wrapper">	
    <?php $this->load->view('template/admin/'.$header);?>
    <?php $this->load->view('template/admin/'.$menu);?>
    <?php $this->load->view('page/admin/'.$content);?>
    <?php $this->load->view('template/admin/'.$footer);?>
</div>


<!-- General JS Scripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="<?=base_url()?>assets/admin/js/stisla.js"></script>

<!-- Template JS File -->
<script src="<?=base_url()?>assets/admin/js/scripts.js"></script>
<script src="<?=base_url()?>assets/admin/js/custom.js"></script> 

<!-- เพื่อใช้งานตัว setButtonLoading(action) &&  setButtonReset(action,time) -->
<script src="<?=base_url()?>assets/admin/js/bootstrap3.2.0/bootstrap.min.js"></script>

<!-- chart -->
<!-- <script src="<?=base_url()?>assets/admin/js/chart.min.js"></script>  
<script src="<?=base_url()?>assets/admin/js/modules-chartjs.js"></script>   -->


<!-- Page Specific JS File -->
<!-- <script src="<?=base_url()?>assets/admin/js/page/index.js"></script> -->

<!-- bootstrap-sweetalert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>


<!-- Page Specific JS File -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js"></script>
<!-- <script src="<?=base_url()?>assets/admin/js/page/modules-toastr.js"></script> -->

<!-- include summernote css/js -->
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

<!-- include fancybox css/js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.js"></script>

<!-- piexif.min.js is needed for auto orienting image files OR when restoring exif data in resized images and when you 
    wish to resize images before upload. This must be loaded before fileinput.min.js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/js/plugins/piexif.min.js" type="text/javascript"></script>
<!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview. 
    This must be loaded before fileinput.min.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/js/plugins/sortable.min.js" type="text/javascript"></script>
<!-- purify.min.js is only needed if you wish to purify HTML content in your preview for 
    HTML files. This must be loaded before fileinput.min.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/js/plugins/purify.min.js" type="text/javascript"></script>
<!-- the main fileinput plugin file -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/js/fileinput.min.js"></script>
<!-- optionally if you need a theme like font awesome theme you can include it as mentioned below -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/themes/fa/theme.js"></script>
<!-- optionally if you need translation for your language then include  locale file as mentioned below -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/js/locales/(lang).js"></script> -->

<script>
    $(function () {
	
        // $('.textarea').wysihtml5();
        // $('.select2').select2();
        
        // $('.datepicker').datepicker({
        //     autoclose: true,
        //     todayHighlight: true
        // })

        // $(document).on('mouseover','[data-toggle="tooltip"]', function(){
        //     $(this).tooltip('show');
        // });

        // $('[data-toggle="popover"]').popover();

        // $('.summernote').summernote();

        $('.summernote-1').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ]
        });
	
    });
    
    function load_notify(icon, message, title, color){
        $.notify({
            // options
            icon: icon
            ,title: '<h4><strong>'+title+' !</strong></h4>'
            ,message: message
        },{
            // settings success danger warning
            type: color
        });
    }

    // set funtion loading button > class or id
    function setButtonLoading(action){
        $(action).button('loading');
    }

    // set funtion reset button > class or id and time
    function setButtonReset(action,time){
        setTimeout(function(){
            $(action).button('reset');
        }, time);
    }

    $(document).ready(function() {
        $('#example').DataTable({
            "order": [[ 0, "desc" ]], //or asc 
        });
    } );

    $(document).ready(function() {
        $('#exampleLives').DataTable();
    } );

    $("#input-b6").fileinput({
        showUpload: false,
        dropZoneEnabled: false,
        // maxFileCount: 10,
        mainClass: "input-group-sm",
        browseClass: "btn btn-info",
        browseLabel: "เลือกรูปภาพ",
        browseIcon: "<i class=\"fas fa-image\"></i> ",
        removeClass: "btn btn-danger",
        removeLabel: "ลบรูปภาพ",
        removeIcon: "<i class=\"fas fa-trash\"></i> ",
        layoutTemplates: {
            main1: "{preview}\n" +
            "<div class=\'input-group {class}\'>\n" +
            "   <div class=\'input-group-btn\ input-group-prepend'>\n" +
            "       {browse}\n" +
            "       {upload}\n" +
            "       {remove}\n" +
            "   </div>\n" +
            "   {caption}\n" +
            "</div>",
        },
        previewZoomButtonIcons : {
            close: '<i class="fas fa-times"></i>'
        }
    });

    $("#input-b7").fileinput({
      showUpload: false,
      dropZoneEnabled: false,
      maxFileCount: 4,
      mainClass: "input-group-sm",
      browseClass: "btn btn-info",
        browseLabel: "เลือกรูปภาพ",
        browseIcon: "<i class=\"fas fa-image\"></i> ",
        removeClass: "btn btn-danger",
        removeLabel: "ลบรูปภาพ",
        removeIcon: "<i class=\"fas fa-trash\"></i> ",
        layoutTemplates: {
            main1: "{preview}\n" +
            "<div class=\'input-group {class}\'>\n" +
            "   <div class=\'input-group-btn\ input-group-prepend'>\n" +
            "       {browse}\n" +
            "       {upload}\n" +
            "       {remove}\n" +
            "   </div>\n" +
            "   {caption}\n" +
            "</div>"
        },
        previewZoomButtonIcons : {
            close: '<i class="fas fa-times"></i>'
        }
    });


</script>
</body>
</html>