<?php
	$_session_in = $this->session->userdata('logged_in');
	$_user_name_head 	= $_session_in['user_name'];
  $_user_img 			= base_url().'upload/users/'.$_session_in['user_img'];

?>
   <div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="<?=base_url('admin/dasboard');?>">Buya-Pakrasob</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="<?=base_url('admin/dasboard');?>">BP</a>
          </div>
          <ul class="sidebar-menu">
              <li class="menu-header">เมนู</li>
              <?php
                if(isset($loadMenu) && count($loadMenu) > 0){
                  foreach($loadMenu as $item){
                    $_active = '';
                    if($data['menuHeader'] == $item['menu_path']){
                      $_active = 'active';
                    }
                    if($item['is_group'] < 1){
                    echo '<li class="'.$_active.'">';
                      echo '<a href="'.base_url().$item['menu_path'].$item['menu_url'].'" class="nav-link ">';
                        echo '<i class="'.$item['menu_icon'].'"></i> <span>'.$item['menu_name'].'</span>';
                          // echo '<span class="pull-right-container">';
                          //   if($item['menu_code'] == 15){
                          //     echo ' <small class="label pull-right bg-yellow text-notimail-num">0</small>';
                          //   }
                          // echo '</span>';
                      echo '</a>';
                    echo '</li>';
                    }

                    if($item['is_group'] > 0){
                      echo '<li class="nav-item dropdown '.$_active.'">';
                        echo '<a href="javascript:viod(0);" class="nav-link has-dropdown">';
                          echo '<i class="'.$item['menu_icon'].'"></i>';
                          echo '<span>'.$item['menu_name'].'</span>';
                        echo '</a>';
                        echo '<ul class="dropdown-menu">';
                          if(isset($item['map_menu_obj']) && count($item['map_menu_obj']) > 0){
                            foreach($item['map_menu_obj'] as $menu_list){
                              $_activeLine = '';
                              if($data['menuLineList'] == $menu_list['menu_code']){
                                $_activeLine = 'active';
                              }
                              echo '<li  class="'.$_activeLine.'">';
                                echo '<a href="'.base_url().$menu_list['menu_path'].$menu_list['menu_url'].'" class="nav-link">';
                                  echo $menu_list['menu_name'];
                                echo '</a>';
                              echo '</li>';
                            }
                          }
                        
                        echo '</ul>';
                      echo '</li>';
                    }
                  }
                }
                ?>

              <!-- <li class="active"><a href="#" class="nav-link "><i class="fas fa-fire"></i><span>Dashboard</span></a></li>
              
              <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="index-0.html">General Dashboard</a></li>
                  <li class="active"><a class="nav-link" href="index.html">Ecommerce Dashboard</a></li>
                </ul>
              </li>


              <li><a class="nav-link" href="blank.html"><i class="far fa-square"></i> <span>Blank Page</span></a></li>
              <li><a class="nav-link" href="credits.html"><i class="fas fa-pencil-ruler"></i> <span>Credits</span></a></li> -->
            
            
            
            
            </ul>

            <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
              <!-- <a href="https://getstisla.com/docs" class="btn btn-primary btn-lg btn-block btn-icon-split"> -->
              <a href="<?=base_url();?>" class="btn btn-primary btn-lg btn-block btn-icon-split" target ="_blank">
                <i class="fas fa-rocket"></i> หน้าแสดงเว็บไซต์
              </a>
            </div>
        </aside>
      </div>