<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>BUYA ผ้ากระสอบ</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- META SEO SET -->
  <meta name="description" content="ขาย เสื้อผ้า กางเกง มื้อสอง มีเบรนดังๆ adidas puma niky" />
  <meta name="keywords" content="ขายเสื้อผ้ามื้อสอง,ขายกางเกงมื้อสอง," />
  <meta http-equiv="content-language" content="th" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <!-- <meta name="revisit-after" content="7 days" /> -->
  <meta name="robots" content="index" />
  <!-- //META SEO SET -->

  <!-- META SHARE SET -->
  <meta property="og:locale" content="th_TH"/>
  <meta property="og:type" content="web"/>
  <meta property="og:title" content="Buya-Pakrasob"/>
  <meta property="og:url" content="http://buyapakrasob.com/"/>
  <meta property="og:site_name" content="buyapakrasob.com"/>
  <meta property="og:image" content="<?= base_url();?>assets/frontend/icon/icon.ico"/>
  <!-- <meta property="og:image" content="<?= base_url();?>assets/frontend/images/logo.png"/> -->
  <!-- //META SHARE SET -->
  
  <link rel="icon" href="<?= base_url();?>assets/frontend/icon/icon.ico" type="icon" sizes="16x16"> 
  <!-- <link rel="shortcut icon" href="favicon.ico" /> -->
  <link rel="stylesheet" type="text/css" media="screen" href="<?= base_url();?>assets/frontend/css/style.css" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?= base_url();?>assets/frontend/css/bootstrap/bootstrap.min.css" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?= base_url();?>assets/frontend/css/fontawesome/all.min.css" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?= base_url();?>assets/frontend/css/swiper/swiper.min.css?v=1001" />  

	<!-- OwlCarousel2 -->
	<link rel="stylesheet" href="<?=base_url('assets/frontend/OwlCarousel2/owl.carousel.min.css');?>">
	<link rel="stylesheet" href="<?=base_url('assets/frontend/OwlCarousel2/owl.theme.default.min.css');?>">

  <!-- fancybox -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css" rel="stylesheet">
  
  <!-- bootstrap-sweetalert -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" rel="stylesheet">
  

  <!-- photon -->
  <link rel="stylesheet" href="<?= base_url();?>assets/photon/css/style.css">
  
  <!-- yaseen -->
  <link rel="stylesheet" href="<?= base_url();?>assets/yaseen/css/nice-select.css">
  <link rel="stylesheet" href="<?= base_url();?>assets/yaseen/css/main.css">

  <!-- portfolio -->
  <link rel="stylesheet" href="<?= base_url();?>assets/portfolio/css/flaticon.css">
  <link rel="stylesheet" href="<?= base_url();?>assets/portfolio/css/gijgo.css">
  <link rel="stylesheet" href="<?= base_url();?>assets/portfolio/css/animate.min.css">
  <link rel="stylesheet" href="<?= base_url();?>assets/portfolio/css/slick.css">
  <link rel="stylesheet" href="<?= base_url();?>assets/portfolio/css/slicknav.css">
  <link rel="stylesheet" href="<?= base_url();?>assets/portfolio/css/style.css">


  <script src="<?= base_url();?>assets/frontend/js/jquery/jquery3-3-1.min.js"></script>
  <script src="<?= base_url();?>assets/frontend/js/bootstrap/bootstrap.min.js"></script>
  <script src="<?= base_url();?>assets/frontend/js/fontawesome/all.min.js"></script>
  <script src="<?= base_url();?>assets/frontend/js/swiper/swiper.min.js?v=1001"></script>

  <script>
    // SCROLL TOP NAVBAR
    $(document).ready(function() {
    // $(window).scroll(function() {
    $(window).scroll(function() {
    var scroll = $(window).scrollTop();
    // console.log(scroll);
    if (scroll >= 100) {
      $('.my-navbar').addClass('down');
      $('.my-navbar-button').addClass('down');  
      $('.menu-point').addClass('down');
      $('.menu-point-ins').addClass('down');
      $('#my-navbar-button').addClass('down'); 

      $('#logoW').addClass('d-none');    
      $('#logoW').removeClass('d-block');    
      $('#logoB').removeClass('d-none'); 
   
    } else {
      $('.my-navbar').removeClass('down');
      $('.my-navbar-button').removeClass('down'); 
      $('.menu-point').removeClass('down');
      $('.menu-point-ins').removeClass('down'); 
      $('#my-navbar-button').removeClass('down'); 
        
      $('#logoW').addClass('d-block'); 
      $('#logoW').removeClass('d-none');     
      $('#logoB').addClass('d-none');  
    }
    });
    });

    $(window).on('load',function(){
      $('.preloader-logo').addClass('display-none');
      $('.preloader').addClass('complete');
      // $('.my-body').removeClass('of-x-hidden');
      setTimeout(function(){ 
        $('.preloader').addClass('display-none');
        
       }, 1000);
    })
  </script>
</head>

<style>
  html,body{
    font-family: myFirstFont;
  }
  .img-black-white {
  -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
  filter: grayscale(100%);
}
</style>
<body class="my-body of-x-hidden">
  <div class="main-wrapper">	
    <?php $this->load->view('template/frontend/'.$header);?>
    <?php $this->load->view('page/frontend/'.$content);?>
    <?php $this->load->view('template/frontend/'.$footer);?>
  </div>
  
</body>
</html>

  
  <!-- photon -->
  <script src="<?= base_url();?>assets/photon/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="<?= base_url();?>assets/photon/js/owl.carousel.min.js"></script>
  <script src="<?= base_url();?>assets/photon/js/jquery.stellar.min.js"></script>
  <script src="<?= base_url();?>assets/photon/js/jquery.countdown.min.js"></script>
  <script src="<?= base_url();?>assets/photon/js/jquery.magnific-popup.min.js"></script>
  <script src="<?= base_url();?>assets/photon/js/aos.js"></script>
  <script src="<?= base_url();?>assets/photon/js/main.js"></script>

  <!-- OwlCarousel2 -->
  <script type="text/javascript" src="<?=base_url('assets/frontend/OwlCarousel2/owl.carousel.min.js')?>"></script>

  <!-- include fancybox css/js -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.js"></script>
  <!-- bootstrap-sweetalert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
  
  <script>
      $(".my-navbar-button").click(function() {
        $('.navbar-menu').addClass('show');
      });
      $(".my-menu-button").click(function() {
        $('.navbar-menu').removeClass('show');
      });

      $('.owl-carousel').owlCarousel({
      stagePadding: 50,
      loop:true,
      items:2,
      margin:10,
      autoplay:true,
      autoplayTimeout:5000,
      autoplayHoverPause:true,
      nav:false,
      responsive:{
          0:{
              items:1
          }
      }
    })


  </script>

