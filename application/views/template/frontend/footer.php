<footer>
      <hr color="gray">
      <div class="text-center">
      <?php if ( isset($db_footer_row) && is_array($db_footer_row) ): ?>
            <?php foreach ( $db_footer_row as $key ): ?>
        <button><a href="https://<?= $key->tbl_footer_social_link;?>" target="_BLANK"><i class="fab <?= $key->tbl_icon_code;?>"></i></a></button>       
        <?php endforeach; ?>
          <?php endif; ?>  
      </div>      
    
    <p>Copyright &copy;	Buya Pakrasob 2020 | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" class="text-white">Colorlib</a>
     <br>Created By ProgrammerKampung <!-- | <a href="" class="text-white">092-9965667</a></p> -->
</footer>