  <div class="preloader">
    <div class="preloader-logo"><img src="<?= base_url();?>/assets/frontend/images/logo.png" alt="">
      <div>

      </div>
    </div>
  </div>

  <nav class="my-navbar">
    <div class="row">
      <div class="col-6"><a href="<?= base_url();?>">
        <img id="logoW" class="" src="<?= base_url();?>/assets/frontend/images/logo.png" alt="">
        <img id="logoB" class="d-none" src="<?= base_url();?>/assets/frontend/images/logoB.png" alt="">
      </a></div>
      <!-- <div class="col-6 text-right">
        <button class="bg-none border-none my-navbar-button" id="my-navbar-button">
          
            <div class="menu-point"><div class="menu-point-ins"></div></div>
            <p>
              เมนู
            </p>
        </button>
      </div> -->
    </div>
  </nav>

  <div class="navbar-menu" id="navbar-menu">
    <div class="navbar-menu-frm">
        <button class="border-none my-menu-button" id="my-menu-button">
          <i class="fas fa-times fa-3x"></i>
        </button>
      <div class="row">
        <div class="col-md-4 menu-bg pc" style="background-image: url('<?=base_url();?>assets/frontend/images/m8.jpg');"></div>
        <div class="col-md-8">
          <ul>
            <!-- <li><a href="<?=base_url().'contact'?>" class="list-menu">Contact</a></li> -->
          <?php if ( isset($db_nav_row)): ?>
            <?php foreach ( $db_nav_row as $key ): ?>
            <li><a href="<?=base_url().$key->tbl_menu_code;?>" class="list-menu"><?=$key->tbl_menu_name;?></a></li>
            <?php endforeach; ?>
          <?php endif; ?>
          </ul>
          <!-- <ul class="social">
            <li>
              <a href="" class="social-list-menu"><i class="fab fa-facebook-f"></i></a>
            </li>
            <li>
              <a href="" class="social-list-menu"><i class="fab fa-youtube"></i></a>
            </li>
            <li>
              <a href="" class="social-list-menu"><i class="fab fa-instagram"></i></a>
            </li>          
          </ul> -->
        </div>
      </div>
    </div>
  </div>