<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*********************	Controller	*********************/
/* Project		: 										*/
/* File name	: m_generalproducts						*/
/* Version		: 1.0.0									*/
/* Create Date	: 13/03/2020							*/
/* Create by	: waeyusree								*/
/* Email		: -										*/
/* Description	: -										*/
/********************************************************/

class M_generalproducts extends CI_Model {
	
	private $table			= 'tbl_generalproducts';
	private $table2			= 'tbl_generalproducts_img_detail';
	private $id 			= 'generalproducts_no';
	private $id2			= 'generalproducts_no';
	private $is_active 		= 'is_active';
	private $is_delete 		= 'is_delete';

    function __construct() {
        
    }
	
	// Table ---tbl_generalproducts---
	function countAll() {
		$this->db->from($this->table);
		return  $this->db->count_all_results();
	}

	function check_generalproductsCode($code) {
		$this->db->where('generalproducts_code', $code);
		$this->db->from($this->table);
		return  $this->db->count_all_results();
	}

	public function insert($value) {
		$this->db->insert($this->table, $value);
		return $this->db->insert_id();
	}

	public function update($id,$value) {	
		$this->db->where($this->id, $id);
		$query = $this->db->update($this->table, $value);
		return $query;
	}
	
	public function get_generalproducts() {
		// $this->db->where($this->is_active, 0);
		$this->db->where($this->is_delete, 0);
		$this->db->order_by($this->id, 'DESC');
		$this->db->select('*');
		$query = $this->db->get($this->table);
		return $query;
	}
	
	public function get_generalproductsById($id) {
		$this->db->where($this->id, $id);
		// $this->db->where($this->is_active, 0);
		$this->db->where($this->is_delete, 0);
		$this->db->select('*');
		$query = $this->db->get($this->table);
		return $query;
	}
	
	public function get_generalproductsLatest() {
		$this->db->limit(1);
		$this->db->order_by('system_add_date','desc');
		$this->db->select('*');
		$query = $this->db->get($this->table);
		return $query;
	}

	public function view_generalproduct() {
		$this->db->where($this->is_active, 0);
		$this->db->where($this->is_delete, 0);
		$this->db->order_by('system_add_date','desc');
		$this->db->select('*');
		$query = $this->db->get($this->table);
		return $query;
	}

	public function view_generalproductsLimit($start,$length) {
		$this->db->where($this->is_active, 0);
		$this->db->where($this->is_delete, 0);
		$this->db->limit($length, $start);
		$this->db->order_by('system_add_date','desc');
		$this->db->select('*');
		$query = $this->db->get($this->table);
		return $query;
	}

	public function plus_view($id)
	{
	    $sql = "UPDATE tbl_generalproducts SET number_view = (number_view+1) WHERE generalproducts_no=?";
	    $this->db->query($sql, array($id));
	}
	
	// Table ---tbl_recommendproducts_img_detail---
	public function get_generalproductsImgDetailById($id) {
		$this->db->where($this->id2, $id);
		$this->db->select('*');
		$query = $this->db->get($this->table2);
		return $query;
	}

	public function insert_multiple($value) {
        if($this->db->insert_batch($this->table2, $value)){
            return true;
        }else{
            return false;
        }
	}
	
	public function deletegeneralproductsImgDetailByid($id) {
		$this->db->where($this->id2, $id);
        $query = $this->db->delete($this->table2);
        return $query;
	}

}
/* End of file m_generalproducts.php */
/* Location: ./application/models/admin/m_generalproducts.php */