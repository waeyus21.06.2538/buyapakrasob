<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*********************	Controller	*********************/
/* Project		: 										*/
/* File name	: m_recommendproducts					*/
/* Version		: 1.0.0									*/
/* Create Date	: 13/03/2020							*/
/* Create by	: waeyusree								*/
/* Email		: -										*/
/* Description	: -										*/
/********************************************************/

class M_recommendproducts extends CI_Model {
	
	private $table			= 'tbl_recommendproducts';
	private $table2			= 'tbl_recommendproducts_img_detail';
	private $id 			= 'recommendproducts_no';
	private $id2			= 'recommendproducts_no';
	private $is_active 		= 'is_active';
	private $is_delete 		= 'is_delete';

    function __construct() {
        
    }
	
	// Table ---tbl_recommendproducts---
	function countAll() {
		$this->db->from($this->table);
		return  $this->db->count_all_results();
	}

	function check_recommendproductsCode($code) {
		$this->db->where('recommendproducts_code', $code);
		$this->db->from($this->table);
		return  $this->db->count_all_results();
	}

	public function insert($value) {
		$this->db->insert($this->table, $value);
		return $this->db->insert_id();
	}

	public function update($id,$value) {	
		$this->db->where($this->id, $id);
		$query = $this->db->update($this->table, $value);
		return $query;
	}

	// public function delete($id) {
	// 	$this->db->where($this->id, $id);
    //     $query = $this->db->delete($this->table);
    //     return $query;
	// }
	
	public function get_recommendproducts() {
		// $this->db->where($this->is_active, 0);
		$this->db->where($this->is_delete, 0);
		$this->db->order_by($this->id, 'DESC');
		$this->db->select('*');
		$query = $this->db->get($this->table);
		return $query;
	}
	
	public function get_recommendproductsById($id) {
		$this->db->where($this->id, $id);
		// $this->db->where($this->is_active, 0);
		$this->db->where($this->is_delete, 0);
		$this->db->select('*');
		$query = $this->db->get($this->table);
		return $query;
	}
	
	public function get_recommendproductsLatest() {
		$this->db->limit(1);
		$this->db->order_by('system_add_date','desc');
		$this->db->select('*');
		$query = $this->db->get($this->table);
		return $query;
	}

	public function view_recommendproductsLimit($start,$length) {
		$this->db->where($this->is_active, 0);
		$this->db->where($this->is_delete, 0);
		$this->db->limit($length, $start);
		$this->db->order_by('system_add_date','desc');
		$this->db->select('*');
		$query = $this->db->get($this->table);
		return $query;
	}

	public function view_recommendproducts() {
		$this->db->where($this->is_active, 0);
		$this->db->where($this->is_delete, 0);
		$this->db->order_by('system_add_date','desc');
		$this->db->select('*');
		$query = $this->db->get($this->table);
		return $query;
	}

	public function plus_view($id)
	{
	    $sql = "UPDATE tbl_recommendproducts SET number_view = (number_view+1) WHERE recommendproducts_no=?";
	    $this->db->query($sql, array($id));
	}
	
	// Table ---tbl_recommendproducts_img_detail---
	public function get_recommendproductsImgDetailById($id) {
		$this->db->where($this->id2, $id);
		$this->db->select('*');
		$query = $this->db->get($this->table2);
		return $query;
	}

	public function insert_multiple($value) {
        if($this->db->insert_batch($this->table2, $value)){
            return true;
        }else{
            return false;
        }
	}
	
	public function deleteRecommendproductsImgDetailByid($id) {
		$this->db->where($this->id2, $id);
        $query = $this->db->delete($this->table2);
        return $query;
	}

}
/* End of file m_recommendproducts.php */
/* Location: ./application/models/admin/m_recommendproducts.php */