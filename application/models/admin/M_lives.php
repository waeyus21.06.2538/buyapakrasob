<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*********************	Controller	*********************/
/* Project		: 										*/
/* File name	: m_lives								*/
/* Version		: 1.0.0									*/
/* Create Date	: 13/03/2020							*/
/* Create by	: waeyusree								*/
/* Email		: -										*/
/* Description	: -										*/
/********************************************************/

class M_lives extends CI_Model {
	
	private $table			= 'tbl_lives';
	private $id 			= 'lives_no';
	private $is_active 		= 'is_active';
	private $is_delete 		= 'is_delete';

    function __construct() {
        
    }
	
	// Table ---tbl_lives---
	function countAll() {
		$this->db->from($this->table);
		return  $this->db->count_all_results();
	}

	public function insert($value) {
		$this->db->insert($this->table, $value);
		return $this->db->insert_id();
	}

	public function get_lives() {
		// $this->db->where($this->is_active, 0);
		$this->db->where($this->is_delete, 0);
		$this->db->order_by('system_add_date','desc');
		$this->db->select('*');
		$query = $this->db->get($this->table);
		return $query;
	}

	public function get_livesById($id) {
		// $this->db->where($this->is_active, 0);
		$this->db->where($this->is_delete, 0);
		$this->db->where($this->id, $id);
		$this->db->select('*');
		$query = $this->db->get($this->table);
		return $query;
	}
	
	public function get_livesLatest() {
		$this->db->limit(1);
		$this->db->order_by('system_add_date','desc');
		$this->db->select('*');
		$query = $this->db->get($this->table);
		return $query;
	}

	public function plus_view($id)
	{
	    $sql = "UPDATE tbl_lives SET number_view = (number_view+1) WHERE lives_no=?";
	    $this->db->query($sql, array($id));
	}

	public function delete($id)
	{
	   	$this->db->where($this->id, $id);
		$this->db->delete($this->table);
	}

}
/* End of file m_lives.php */
/* Location: ./application/models/admin/m_lives.php */