<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*********************	Controller	*********************/
/* Project		: 										*/
/* File name	: m_contact								*/
/* Version		: 1.0.0									*/
/* Create Date	: 02/07/2019							*/
/* Create by	: waeyusree								*/
/* Email		: -										*/
/* Description	: -										*/
/********************************************************/

class M_contact extends CI_Model {
	
	private $table			= 'tbl_contact';
	private $table2			= 'tbl_contact_social';
	private $id 			= 'contact_id';
	private $is_active 		= 'is_active';
	private $is_delete 		= 'is_delete';

    function __construct() {
        
    }


	// Table ---tbl_contact---
	function countAll() {
		$this->db->from($this->table);
		return  $this->db->count_all_results();
	}

	public function insert($value) {
        $this->db->insert($this->table, $value);
        return $this->db->insert_id();
	}

	public function update($id,$value) {	
		$this->db->where($this->id, $id);
		$query = $this->db->update($this->table, $value);
		return $query;
	}
	
	public function get_contact() {
		$this->db->where($this->is_active, 0);
		$this->db->where($this->is_delete, 0);
        $this->db->select('*');
        $query = $this->db->get($this->table);
        return $query;
	}
	
	public function get_contactById($id) {
		$this->db->where($this->id, $id);
		$this->db->where($this->is_active, 0);
		$this->db->where($this->is_delete, 0);
        $this->db->select('*');
        $query = $this->db->get($this->table);
        return $query;
	}
	
	// Table ---tbl_contact_social---
	function count_social() {
		$this->db->from($this->table2);
		return  $this->db->count_all_results();
	}

	public function insert_contact_social($value_arr) {
		$query = $this->db->insert($this->table2, $value_arr);
        return $query;
	}

	public function get_contact_social() {
        $this->db->select('*');
        $query = $this->db->get($this->table2);
        return $query;
	}

	public function delete() {
        $query = $this->db->delete($this->table2);
        return $query;
	}
	
	public function truncate() {
        $query = $this->db->truncate($this->table2);
        return $query;
	}
	


}
/* End of file m_contact.php */
/* Location: ./application/models/m_contact.php */