<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*********************	Controller	*********************/
/* Project		: BuyaPakrasob							*/
/* File name	: m_statistics							*/
/* Version		: 1.0.0									*/
/* Create Date	: 21/09/2020							*/
/* Create by	: waeyusree								*/
/* Email		: -										*/
/* Description	: -										*/
/********************************************************/

class M_statistics extends CI_Model {
	
	private $table1			= 'tbl_statistic_browser';
	private $table2			= 'tbl_statistic_phone';
	private $table3			= 'tbl_statistic_system';
	private $table4			= 'tbl_statistic_lives';
	private $table5			= 'tbl_statistic_sale';
	private $name1 			= 'statistic_browser_name';
	private $name2 			= 'statistic_phone_name';
	private $name3 			= 'statistic_system_name';
	private $no4 			= 'lives_no';


    function __construct() { 

    }
	
	// Table ---tbl_statistic_browser---
	function countAllBrowser() {
		$this->db->from($this->table1);
		return  $this->db->count_all_results();
	}

	function countAllBrowserByDateTime($y, $m, $d, $h) {
		$this->db->where('statistic_browser_d', $d);
		$this->db->where('statistic_browser_m', $m);
		$this->db->where('statistic_browser_y', $y);
		$this->db->where('statistic_browser_h', $h);
		$this->db->from($this->table1);
		return  $this->db->count_all_results();
	}

	function countAllBrowserByDate($y, $m, $d) {
		$this->db->where('statistic_browser_d', $d);
		$this->db->where('statistic_browser_m', $m);
		$this->db->where('statistic_browser_y', $y);
		$this->db->from($this->table1);
		return  $this->db->count_all_results();
	}

	function countAllChrome() {
		$this->db->where($this->name1, 'Chrome');
		$this->db->from($this->table1);
		return  $this->db->count_all_results();
	}
	function countAllFirefox() {
		$this->db->where($this->name1, 'Firefox');
		$this->db->from($this->table1);
		return  $this->db->count_all_results();
	}
	function countAllSafari() {
		$this->db->where($this->name1, 'Safari');
		$this->db->from($this->table1);
		return  $this->db->count_all_results();
	}

	// Table ---tbl_statistic_phone---
	function countAllPhone() {
		$this->db->from($this->table2);
		return  $this->db->count_all_results();
	}

	function countAlliPhone() {
		$this->db->where($this->name2, 'Apple iPhone');
		$this->db->from($this->table2);
		return  $this->db->count_all_results();
	}
	function countAlliPad() {
		$this->db->where($this->name2, 'iPad');
		$this->db->from($this->table2);
		return  $this->db->count_all_results();
	}
	function countAllAndroid() {
		$this->db->where($this->name2, 'Android');
		$this->db->from($this->table2);
		return  $this->db->count_all_results();
	}
	function countAllOrther() {
		$this->db->where($this->name2, 'orther');
		$this->db->from($this->table2);
		return  $this->db->count_all_results();
	}

	// Table ---tbl_statistic_system---
	function countAllSystem() {
		$this->db->from($this->table3);
		return  $this->db->count_all_results();
	}

	function countSystemWindows() {
		$this->db->like($this->name3, 'Windows');
		$this->db->from($this->table3);
		return  $this->db->count_all_results();
	}
	function countSystemiOS() {
		$this->db->where($this->name3, 'iOS');
		$this->db->from($this->table3);
		return  $this->db->count_all_results();
	}
	function countSystemAndroid() {
		$this->db->where($this->name3, 'Android');
		$this->db->from($this->table3);
		return  $this->db->count_all_results();
	}




	// Table ---tbl_statistic_lives---
	public function insert_lives($value) {
		$this->db->insert($this->table4, $value);
		return $this->db->insert_id();
	}

	function countByDateTime($y, $m, $d, $h) {
		$this->db->where('statistic_lives_d', $d);
		$this->db->where('statistic_lives_m', $m);
		$this->db->where('statistic_lives_y', $y);
		$this->db->where('statistic_lives_h', $h);
		$this->db->from($this->table4);
		return  $this->db->count_all_results();
	}

	public function get_livesTitle($y, $m, $d) {
		$this->db->where('statistic_lives_d', $d);
		$this->db->where('statistic_lives_m', $m);
		$this->db->where('statistic_lives_y', $y);
		$this->db->limit(1);
		$this->db->order_by('lives_no','desc');
		$this->db->select('*');
		$query = $this->db->get($this->table4);
		return $query;
	}
	
	// tbl_statistic_sale
	function countAllSale() {
		$this->db->from($this->table5);
		return  $this->db->count_all_results();
	}

	function countByYear($y) {
		$this->db->where('statistic_sale_y', $y);
		$this->db->from($this->table5);
		return  $this->db->count_all_results();
	}
	function countByYearGradeA($y) {
		$this->db->where('statistic_sale_y', $y);
		$this->db->where('statistic_sale_products_grade', 0);
		$this->db->from($this->table5);
		return  $this->db->count_all_results();
	}
	function countByYearGradeB($y) {
		$this->db->where('statistic_sale_y', $y);
		$this->db->where('statistic_sale_products_grade', 1);
		$this->db->from($this->table5);
		return  $this->db->count_all_results();
	}

	function countByMonthGradeA($y, $m) {
		$this->db->where('statistic_sale_m', $m);
		$this->db->where('statistic_sale_y', $y);
		$this->db->where('statistic_sale_products_grade', 0);
		$this->db->from($this->table5);
		return  $this->db->count_all_results();
	}
	function countByMonthGradeB($y, $m) {
		$this->db->where('statistic_sale_m', $m);
		$this->db->where('statistic_sale_y', $y);
		$this->db->where('statistic_sale_products_grade', 1);
		$this->db->from($this->table5);
		return  $this->db->count_all_results();
	}
	function countBrandsByGradeA($y, $id) {
		$this->db->where('statistic_sale_y', $y);
		$this->db->where('statistic_sale_products_brand', $id);
		$this->db->where('statistic_sale_products_grade', 0);
		$this->db->from($this->table5);
		return  $this->db->count_all_results();
	}
	function countBrandsByGradeB($y, $id) {
		$this->db->where('statistic_sale_y', $y);
		$this->db->where('statistic_sale_products_brand', $id);
		$this->db->where('statistic_sale_products_grade', 1);
		$this->db->from($this->table5);
		return  $this->db->count_all_results();
	}

	function count_chec($code) {
		$this->db->where('statistic_sale_products_code', $code);
		$this->db->from($this->table5);
		return  $this->db->count_all_results();
	}

	public function insert_sale($value) {
		$this->db->insert($this->table5, $value);
		return $this->db->insert_id();
	}

	public function get_salenormal($y, $m) {
		$this->db->select_sum('statistic_sale_products_discount');
		$this->db->where('statistic_sale_sale_type', 0);
		$this->db->where('statistic_sale_m', $m);
		$this->db->where('statistic_sale_y', $y);
		$query = $this->db->get($this->table5);
		return $query;
	}

	public function get_saleNonormal($y, $m) {
		$this->db->select_sum('statistic_sale_products_discount');
		$this->db->where('statistic_sale_sale_type', 1);
		$this->db->where('statistic_sale_m', $m);
		$this->db->where('statistic_sale_y', $y);
		$query = $this->db->get($this->table5);
		return $query;
	}

	public function get_sumAllSaleNormal($y) {
		$this->db->select_sum('statistic_sale_products_discount');
		$this->db->where('statistic_sale_sale_type', 0);
		$this->db->where('statistic_sale_y', $y);
		$query = $this->db->get($this->table5);
		return $query;
	}
	public function get_sumAllSaleNONormal($y) {
		$this->db->select_sum('statistic_sale_products_discount');
		$this->db->where('statistic_sale_sale_type', 1);
		$this->db->where('statistic_sale_y', $y);
		$query = $this->db->get($this->table5);
		return $query;
	}
	public function get_sumAllSale($y) {
		$this->db->select_sum('statistic_sale_products_discount');
		$this->db->where('statistic_sale_y', $y);
		$query = $this->db->get($this->table5);
		return $query;
	}

}
/* End of file ----------.php */
/* Location: ./application/models/-------.php */