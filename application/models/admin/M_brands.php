<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*********************	Controller	*********************/
/* Project		: 										*/
/* File name	: M_brands								*/
/* Version		: 1.0.0									*/
/* Create Date	: 01/10/2020							*/
/* Create by	: waeyusree								*/
/* Email		: -										*/
/* Description	: -										*/
/********************************************************/

class M_brands extends CI_Model {
	
	private $table			= 'tbl_brands';
	private $id 			= 'brands_no';
	private $is_active 		= 'is_active';
	private $is_delete 		= 'is_delete';

    function __construct() {
        
    }
	
	// Table ---tbl_contact---
	function countAll() {
		$this->db->from($this->table);
		return  $this->db->count_all_results();
	}

	public function insert($value) {
        $this->db->insert($this->table, $value);
        return $this->db->insert_id();
	}

	public function update($id,$value) {	
		$this->db->where($this->id, $id);
		$query = $this->db->update($this->table, $value);
		return $query;
	}
	
	public function veiw_brands() {
		$this->db->where($this->is_active, 0);
		$this->db->where($this->is_delete, 0);
		$this->db->order_by($this->id,'desc');
        $this->db->select('*');
        $query = $this->db->get($this->table);
        return $query;
	}

	public function get_brands() {
		$this->db->where($this->is_delete, 0);
		$this->db->order_by($this->id,'desc');
        $this->db->select('*');
        $query = $this->db->get($this->table);
        return $query;
	}
	
	public function get_brandsById($id) {
		$this->db->where($this->id, $id);
		$this->db->where($this->is_delete, 0);
        $this->db->select('*');
        $query = $this->db->get($this->table);
        return $query;
	}

}
/* End of file m_brands.php */
/* Location: ./application/models/m_brands.php */