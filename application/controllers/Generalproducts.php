<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generalproducts extends LI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model(
			array(
				'admin/m_generalproducts',
			));
	}

	public function index(){
		// $_start 			= 0;
        // $_length			= 6;	
		// $this->_data['generalproducts'] = $this->m_generalproducts->view_generalproductsLimit($_start, $_length)->result();

		$send = array(
			'header' 		=> 'header'
			,'content' 		=> 'generalproducts/index'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'ติดต่อเรา'
			,'data'			=> $this->_data
		);

		$this->loadPageFrontend($send);
	}

	public function detail($id=null){
		$id 	 										= $this->setBase64_decode($id);

		// นับการดูสินค้า
		$this->m_generalproducts->plus_view($id);
		
		$this->_data['generalproducts'] 				= $this->m_generalproducts->get_generalproductsById($id)->row();
		$this->_data['generalproductsimagesdetail']		= $this->m_generalproducts->get_generalproductsImgDetailById($id)->result();

		$this->veiw_brands();
		$this->veiw_contact();
		
	

		$send = array(
			'header' 		=> 'header'
			,'content' 		=> 'generalproducts/generalproducts_detail'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'รายละเอียดสินค้า'
			,'data'			=> $this->_data
		);

		$this->loadPageFrontend($send);
	}

	public function more(){	
		$this->_data['generalproducts'] 		= $this->m_generalproducts->view_generalproduct()->result();
		$this->_data['generalproducts_test'] 	= $this->m_generalproducts->get_generalproductsById(3)->row();
		
		$this->veiw_brands();
		$this->veiw_contact();

		$send = array(
			 'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'generalproducts/generalproducts_more'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'สินค้าเพิ่มเติม'
			,'data'			=> $this->_data
		);
		
		$this->loadPageFrontend($send);
	}


}
