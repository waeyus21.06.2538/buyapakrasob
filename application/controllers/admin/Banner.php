<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends LI_Controller {
	
	function __construct() {
        parent::__construct();
		
		$this->_set_user_login();
		$this->_init();
		
		$this->load->model(
			array(
				'admin/m_banner'
			));
	}
	
	public function _init(){
		
		$this->is_active			= 0; 
		$this->is_delete			= 0;
		$this->user_code			= $this->session->userdata('logged_in')['user_code'];
		$this->_data['menuHeader']	= 'admin/banner';
		$this->_data['menuLineList']= 9;
	}
	
	public function _set_user_login(){
		if(empty($this->session->userdata('logged_in'))){
			redirect(base_url().'login');
		}
	}
	
	public function index(){

		$count 	= $this->m_banner->countAll();
		if($count > 0):
			$this->_data['banner'] = $this->m_banner->get_banner()->result();
		endif;

		// url
		$this->_data['url_create'] = base_url()."admin/".$this->router->fetch_class()."/create";

		$send = array(
			'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'banner/index'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'ป้ายโฆษณา'
			,'data'			=> $this->_data
		);
		
		$this->loadPageView($send);
	}
	
	public function create(){

		$send = array(
			 'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'banner/from'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'สร้าง'
			,'mode'			=> 'create'
			,'data'			=> $this->_data
		);
		
		$send['form_action'] = base_url().'api/admin/'.$this->router->fetch_class().'/storage';
		$this->loadPageView($send);
	}
	
	public function edit($id=null){
		
		$id 	 				= $this->setBase64_decode($id);
		$this->_data['banner'] = $this->m_banner->get_bannerById($id)->row();

		$send = array(
			 'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'banner/from'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'แก้ไข'
			,'mode'			=> 'edit'
			,'data'			=> $this->_data
		);

		$send['form_action'] = base_url().'api/admin/'.$this->router->fetch_class().'/update';
		$this->loadPageView($send);
	}

	public function detail($id=null){
		
		$id 	 				= $this->setBase64_decode($id);
		$this->_data['banner'] = $this->m_banner->get_bannerById($id)->row();

		$send = array(
			 'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'banner/detail'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'รายละเอียดป้ายโฆษณา'
			,'mode'			=> 'edit'
			,'data'			=> $this->_data
		);

		$this->loadPageView($send);
	}
}
