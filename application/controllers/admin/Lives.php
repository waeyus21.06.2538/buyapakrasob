<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class lives extends LI_Controller {
	
	function __construct() {
        parent::__construct();
		
		$this->_set_user_login();
		$this->_init();
		
		$this->load->model(
			array(
				'admin/m_lives',
			));
	}
	
	public function _init(){
		
		$this->is_active			= 0; 
		$this->is_delete			= 0;
		$this->user_code			= $this->session->userdata('logged_in')['user_code'];
		$this->_data['menuHeader']	= 'admin/lives';
		$this->_data['menuLineList']= 6;
	}
	
	public function _set_user_login(){
		if(empty($this->session->userdata('logged_in'))){
			redirect(base_url().'login');
		}
	}
	
	public function index(){
		$this->_data['lives'] = $this->m_lives->get_livesLatest()->row();
		
		$send = array(
			'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'lives/index'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'สร้าง'
			,'data'			=> $this->_data
		);
		
		$send['form_action'] = base_url().'api/admin/'.$this->router->fetch_class().'/storage';
		$this->loadPageView($send);
	}
	
	public function detail(){
		$this->_data['lives'] = $this->m_lives->get_lives()->result();

		$send = array(
			 'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'lives/lives_detail'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'รายการไลฟ์สด'
			,'data'			=> $this->_data
		);
		
		$send['url_create'] = base_url().'admin/'.$this->router->fetch_class();
		$this->loadPageView($send);
	}
	
	
}
