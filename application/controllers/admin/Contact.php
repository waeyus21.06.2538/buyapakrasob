<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends LI_Controller {
	
	function __construct() {
        parent::__construct();
		
		$this->_set_user_login();
		$this->_init();
		
		$this->load->model(
			array(
				'admin/m_contact'
			));
	}
	
	public function _init(){
		
		$this->is_active			= 0; 
		$this->is_delete			= 0;
		$this->user_code			= $this->session->userdata('logged_in')['user_code'];
		$this->_data['menuHeader']	= 'management';
		$this->_data['menuLineList']= 3;
	}
	
	public function _set_user_login(){
		if(empty($this->session->userdata('logged_in'))){
			redirect(base_url().'login');
		}
	}
	
	public function index(){
		$id			= '';
		$social_arr = array();

		$count 	= $this->m_contact->countAll();
		if($count > 0):
			$this->_data['contact'] = $this->m_contact->get_contact()->row();
			$id = $this->setBase64_encode($this->_data['contact']->contact_id);

			$count_social = $this->m_contact->count_social();
			if($count_social > 0){
				$this->_data['contact_social'] = $this->m_contact->get_contact_social()->result();
			}
		endif;

		// url
		$this->_data['url_create'] = base_url()."admin/".$this->router->fetch_class()."/create";
		$this->_data['url_edit'] = base_url()."admin/".$this->router->fetch_class()."/edit/{$id}";

		$send = array(
			'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'contact/index'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'ติดต่อเรา'
			,'data'			=> $this->_data
		);
		
		$this->loadPageView($send);
	}
	
	public function create(){
		
		$send = array(
			 'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'contact/from'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'สร้าง'
			,'mode'			=> 'create'
			,'data'			=> $this->_data
		);
		
		$send['form_action'] = base_url().'api/admin/'.$this->router->fetch_class().'/storage';
		$this->loadPageView($send);
	}
	
	public function edit($id=null){
		
		$id 	 			= $this->setBase64_decode($id);
		$this->_data['contact'] = $this->m_contact->get_contactById($id)->row();
		$this->_data['contact_social'] = $this->m_contact->get_contact_social()->result();

		// $this->arr($this->_data['contact_social']);
		// exit();

		$send = array(
			 'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'contact/from'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'แก้ไข'
			,'mode'			=> 'edit'
			,'data'			=> $this->_data
		);

		$send['form_action'] = base_url().'api/admin/'.$this->router->fetch_class().'/update';
		$this->loadPageView($send);
	}
	
	public function delete(){
		
		$_message_title 		= 'ข้อความแจ้งเตือน !';
		$_message				= '';
		$_error					= 0;
		$_warning				= '';
		$_item_code 			= $this->input->post('food_code');
		$_system_date 			= date('Y-m-d H:i:s');
			
		if(count($_item_code) > 0){

			$this->db->trans_begin();
			foreach($_item_code as $code){
				/* delete table tbl_item */
				$_item_arr = array(
					'is_delete' 	  			=> 1
					,'system_update_date' 	=> $_system_date
				);
					
				if(!$this->db->update('tbl_items', $_item_arr, array('item_code' => $code, 'user_key' => $this->user_key))){
					$_error++;
				}
			}
		}
		
		if ($this->db->trans_status() === FALSE || $_error > 0){
			$this->db->trans_rollback();
			$_message = 'ไม่สามารถลบข้อมูล กรุณาลองใหม่';
			$_warning = 'warning';
		}else{
			$_message = 'ลบข้อมูลเรียบร้อยแล้ว';
			$_warning = 'success';
			$this->db->trans_commit();
		}
		
		$this->session->set_flashdata('message', $_message);
		$this->session->set_flashdata('message_title', $_message_title);
		$this->session->set_flashdata('message_check', $_warning);
		
		redirect(base_url().'items');
	}
}
