<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dasboard extends LI_Controller {
	
	function __construct() {
        parent::__construct();
		
		$this->_set_user_login();
		$this->_init();
		
		$this->load->model(
			array(
				 'admin/m_statistics'
			));
	}
	
	public function _init(){
		
		$this->is_active			= 0; 
		$this->is_delete			= 0;
		$this->user_code			= $this->session->userdata('logged_in')['user_code'];
		$this->_data['menuHeader']	= 'admin/dasboard';
		$this->_data['menuLineList']= 2;
	}
	
	public function _set_user_login(){
		if(empty($this->session->userdata('logged_in'))){
			redirect(base_url().'login');
		}
	}
	
	public function index(){
		$this->viewBrowser();
		$this->viewSystem();
		$this->viewPhone();
		
		$send = array(
			 'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'dasboard/index'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'แผนควบคุม'
			,'data'			=> $this->_data
		);
		
		$this->loadPageView($send);
	}
	
	public function add(){
		
		$send = array(
			 'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'services/add'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'เพิ่มข้อมูลบริการเรา'
			,'data'			=> $this->_data
		);
		
		$this->loadPageView($send);
	}

	public function viewBrowser(){
		$this->_data['countAllBrowser'] 	= $this->m_statistics->countAllBrowser();
		$this->_data['countAllChrome'] 		= $this->m_statistics->countAllChrome();
		$this->_data['countAllFirefox'] 	= $this->m_statistics->countAllFirefox();
		$this->_data['countAllSafari'] 		= $this->m_statistics->countAllSafari();
		
		// $send = array(
		// 	 'header' 		=> 'header'
		// 	,'menu' 		=> 'menu'
		// 	,'content' 		=> 'services/add'
		// 	,'footer' 		=> 'footer'
		// 	,'pagetitle' 	=> 'เพิ่มข้อมูลบริการเรา'
		// 	,'data'			=> $this->_data
		// );
		
		// $this->loadPageView($send);
		// return $this->_data;
	}

	public function viewSystem(){
		$this->_data['countAllSystem'] 		= $this->m_statistics->countAllSystem();
		$this->_data['countSystemWindows'] 	= $this->m_statistics->countSystemWindows();
		$this->_data['countSystemiOS'] 		= $this->m_statistics->countSystemiOS();
		$this->_data['countSystemAndroid'] 	= $this->m_statistics->countSystemAndroid();
	}

	public function viewPhone(){
		$this->_data['countAlliPhone'] 		= $this->m_statistics->countAlliPhone();
		$this->_data['countAlliPad'] 		= $this->m_statistics->countAlliPad();
		$this->_data['countAllAndroid'] 	= $this->m_statistics->countAllAndroid();
		$this->_data['countAllOrther'] 		= $this->m_statistics->countAllOrther();
	}
	
	
	
	
}
