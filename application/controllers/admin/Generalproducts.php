<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class generalproducts extends LI_Controller {
	
	function __construct() {
        parent::__construct();
		
		$this->_set_user_login();
		$this->_init();
	
		$this->load->model(
			array(
				'admin/m_generalproducts',
				'admin/m_brands',
			));
	}
	
	public function _init(){
		
		$this->is_active			= 0; 
		$this->is_delete			= 0;
		$this->user_code			= $this->session->userdata('logged_in')['user_code'];
		$this->_data['menuHeader']	= 'admin/generalproducts';
		$this->_data['menuLineList']= 7;
	}
	
	public function _set_user_login(){
		if(empty($this->session->userdata('logged_in'))){
			redirect(base_url().'login');
		}
	}
	
	public function index(){

		$count 	= $this->m_generalproducts->countAll();
		if($count > 0):
			$this->_data['generalproducts'] = $this->m_generalproducts->get_generalproducts()->result();
		endif;
		$this->veiw_brands();

		// url
		$this->_data['url_create'] = base_url()."admin/".$this->router->fetch_class()."/create";

		$send = array(
			'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'generalproducts/index'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'สินค้าทั่วไป'
			,'data'			=> $this->_data
		);
		
		$this->loadPageView($send);
	}
	
	public function create(){
		$count 	= $this->m_generalproducts->countAll();
		if($count > 0):
			$this->_data['generalproducts_code'] = $this->m_generalproducts->get_generalproductsLatest()->row();
		endif;
		$this->veiw_brands();
		
		$send = array(
			 'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'generalproducts/from'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'สร้าง'
			,'mode'			=> 'create'
			,'data'			=> $this->_data
		);
		
		$send['form_action'] = base_url().'api/admin/'.$this->router->fetch_class().'/storage';
		$this->loadPageView($send);
	}
	
	public function edit($id=null){
		
		$id 	 				= $this->setBase64_decode($id);
		$this->_data['generalproducts'] = $this->m_generalproducts->get_generalproductsById($id)->row();
		$this->veiw_brands();

		$send = array(
			 'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'generalproducts/from'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'แก้ไข'
			,'mode'			=> 'edit'
			,'data'			=> $this->_data
		);

		$send['form_action'] = base_url().'api/admin/'.$this->router->fetch_class().'/update';
		$this->loadPageView($send);
	}

}
