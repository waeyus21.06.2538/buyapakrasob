<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brands extends LI_Controller {
	
	function __construct() {
        parent::__construct();
		
		$this->_set_user_login();
		$this->_init();
		
		$this->load->model(
			array(
				'admin/m_brands'
			));
	}
	
	public function _init(){
		
		$this->is_active			= 0; 
		$this->is_delete			= 0;
		$this->user_code			= $this->session->userdata('logged_in')['user_code'];
		$this->_data['menuHeader']	= 'admin/brands';
		$this->_data['menuLineList']= 8;
	}
	
	public function _set_user_login(){
		if(empty($this->session->userdata('logged_in'))){
			redirect(base_url().'login');
		}
	}
	
	public function index(){

		$count 	= $this->m_brands->countAll();
		if($count > 0):
			$this->_data['brands'] = $this->m_brands->get_brands()->result();
		endif;

		// url
		$this->_data['url_create'] = base_url()."admin/".$this->router->fetch_class()."/create";

		$send = array(
			'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'brands/index'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'แบรนด์สินค้า'
			,'data'			=> $this->_data
		);
		
		$this->loadPageView($send);
	}
	
	public function create(){

		$send = array(
			 'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'brands/from'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'สร้าง'
			,'mode'			=> 'create'
			,'data'			=> $this->_data
		);
		
		$send['form_action'] = base_url().'api/admin/'.$this->router->fetch_class().'/storage';
		$this->loadPageView($send);
	}
	
	public function edit($id=null){
		
		$id 	 				= $this->setBase64_decode($id);
		$this->_data['brands'] = $this->m_brands->get_brandsById($id)->row();

		$send = array(
			 'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'brands/from'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'แก้ไข'
			,'mode'			=> 'edit'
			,'data'			=> $this->_data
		);

		$send['form_action'] = base_url().'api/admin/'.$this->router->fetch_class().'/update';
		$this->loadPageView($send);
	}
}
