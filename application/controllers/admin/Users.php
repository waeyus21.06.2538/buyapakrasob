<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends LI_Controller {
	
	function __construct() {
        parent::__construct();
		
		$this->_set_user_login();
		$this->_init();
		
		$this->load->model(
				array(
					'm_user_type'
					,'m_user'
				));
	}
	
	public function _init(){
		
		$this->is_active			= 0; 
		$this->is_delete			= 0;
		$this->user_code			= $this->session->userdata('logged_in')['user_code'];
		$this->_data['menuHeader']	= 'setting';
		$this->_data['menuLineList']= '1';
	}
	
	public function _set_user_login(){
		if(empty($this->session->userdata('logged_in'))){
			redirect(base_url().'login');
		}
	}
	
	public function index(){
		
		$send = array(
			 'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'users/index'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'จัดการข้อมูลผู้ใช้งาน'
			,'data'			=> $this->_data
		);
		
		$this->loadPageView($send);
	}

	public function profile(){
		
		$send = array(
			 'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'users/profile'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'รายละเอียดผู้ใช้งาน'
			,'data'			=> $this->_data
		);
		
		$this->loadPageView($send);
	}
	
	public function add(){
		$_user_type_arr = array();
		
		$_obj_user_type = $this->m_user_type->view($this->is_active, $this->is_delete);
        if ($_obj_user_type['status'] == true && $_obj_user_type['results'] != null) {
            foreach ($_obj_user_type['results'] as $item) {
				array_push($_user_type_arr, array(
					 'user_type_code' 	=> $item->user_type_code
					,'user_type_name'	=> $item->user_type_name
				));
			}
		}
		unset($_obj_user_type);
		$this->_data['user_type'] = $_user_type_arr;
		
		$send = array(
			 'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'users/add'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'เพิ่มข้อมูลผู้ใช้งาน'
			,'data'			=> $this->_data
		);
		
		$this->loadPageView($send);
	}
	
	public function edit($code=null){
		
		$_code 	 		= $this->setBase64_decode($code);
		$_user_type_arr = array();

		$_obj_user = $this->m_user->viewByKey($_code);
		if($_obj_user['status'] == true && $_obj_user['results'] != null){
			$this->_data['user_code'] 		= $_obj_user['results']->user_code;
			$this->_data['user_name'] 		= $_obj_user['results']->user_name;
			$this->_data['user_login_name'] = $_obj_user['results']->user_login_name;
			$this->_data['user_login_pass'] = $_obj_user['results']->user_login_pass;
			$this->_data['is_login'] 		= $_obj_user['results']->is_login;
			$this->_data['user_type_code'] 	= $_obj_user['results']->user_type_code;
			
		}
		unset($_obj_user);

		$_obj_user_type = $this->m_user_type->view($this->is_active, $this->is_delete);
        if ($_obj_user_type['status'] == true && $_obj_user_type['results'] != null) {
            foreach ($_obj_user_type['results'] as $item) {
				array_push($_user_type_arr, array(
					 'user_type_code' 	=> $item->user_type_code
					,'user_type_name'	=> $item->user_type_name
				));
			}
		}
		unset($_obj_user_type);

		$this->_data['user_type'] = $_user_type_arr;
		
		$send = array(
			 'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'users/edit'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'แก้ไขข้อมูลผู้ใช้งาน'
			,'data'			=> $this->_data
		);
		
		$this->loadPageView($send);
	}
	
	public function delete(){
		
		$_message_title 		= 'ข้อความแจ้งเตือน !';
		$_message				= '';
		$_error					= 0;
		$_warning				= '';
		$_item_code 			= $this->input->post('food_code');
		$_system_date 			= date('Y-m-d H:i:s');
			
		if(count($_item_code) > 0){

			$this->db->trans_begin();
			foreach($_item_code as $code){
				/* delete table tbl_item */
				$_item_arr = array(
					'is_delete' 	  			=> 1
					,'system_update_date' 	=> $_system_date
				);
					
				if(!$this->db->update('tbl_items', $_item_arr, array('item_code' => $code, 'user_key' => $this->user_key))){
					$_error++;
				}
			}
		}
		
		if ($this->db->trans_status() === FALSE || $_error > 0){
			$this->db->trans_rollback();
			$_message = 'ไม่สามารถลบข้อมูล กรุณาลองใหม่';
			$_warning = 'warning';
		}else{
			$_message = 'ลบข้อมูลเรียบร้อยแล้ว';
			$_warning = 'success';
			$this->db->trans_commit();
		}
		
		$this->session->set_flashdata('message', $_message);
		$this->session->set_flashdata('message_title', $_message_title);
		$this->session->set_flashdata('message_check', $_warning);
		
		redirect(base_url().'items');
	}
}
