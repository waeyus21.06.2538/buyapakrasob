<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends LI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->__statistic_storage();
		$this->load->model(
			array(
				'admin/m_recommendproducts',
				'admin/m_generalproducts',
				'admin/m_lives',
				'admin/m_banner',
			));
	}

	public function index(){
		$_start 			= 0;
        $_length			= 6;
	
		$this->_data['recommendproducts'] 	= $this->m_recommendproducts->view_recommendproductsLimit($_start, $_length)->result();
		$this->_data['generalproducts'] 	= $this->m_generalproducts->view_generalproductsLimit(0, 9)->result();
		$this->_data['banner'] 	= $this->m_banner->veiw_banner()->result();

		// เรียกใช้ functoin ของตัวเอง
		$this->get_lives();
		$this->veiw_brands();
		$this->veiw_contact();
		
		$send = array(
			'header' 		=> 'header'
			,'content' 		=> 'home/index'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'ติดต่อเรา'
			,'data'			=> $this->_data
		);

		$this->loadPageFrontend($send);
	}
    
    public function __statistic_storage(){		
		$_system_d 		= date('d');
		$_system_m		= date('m');
		$_system_y 		= date('Y');
		$_system_h 		= date('H');
		$_system_i 		= date('i');
		$_system_s 		= date('s');

		$results = $this->statistic();
		
		if(!empty($results['agent_browser'])){
				$data = array(
					'statistic_browser_name' => $results['agent_browser'],
					'statistic_browser_d' => $_system_d,
					'statistic_browser_m' => $_system_m,
					'statistic_browser_y' => $_system_y,
					'statistic_browser_h' => $_system_h,
					'statistic_browser_i' => $_system_i,
					'statistic_browser_s' => $_system_s,
		
				);
				$this->db->insert('tbl_statistic_browser', $data);
			}else{
				$data = array(
					'statistic_browser_name' => 'orther',
					'statistic_browser_d' => $_system_d,
					'statistic_browser_m' => $_system_m,
					'statistic_browser_y' => $_system_y,
					'statistic_browser_h' => $_system_h,
					'statistic_browser_i' => $_system_i,
					'statistic_browser_s' => $_system_s,
				);
				$this->db->insert('tbl_statistic_browser', $data);
		}

		if(!empty($results['agent_phone'])){
				$data = array(
					'statistic_phone_name' => $results['agent_phone'],
					'statistic_phone_d' => $_system_d,
					'statistic_phone_m' => $_system_m,
					'statistic_phone_y' => $_system_y,
					'statistic_phone_h' => $_system_h,
					'statistic_phone_i' => $_system_i,
					'statistic_phone_s' => $_system_s,
				);
				$this->db->insert('tbl_statistic_phone', $data);
			}else{
				$data = array(
					'statistic_phone_name' => 'orther',
					'statistic_phone_d' => $_system_d,
					'statistic_phone_m' => $_system_m,
					'statistic_phone_y' => $_system_y,
					'statistic_phone_h' => $_system_h,
					'statistic_phone_i' => $_system_i,
					'statistic_phone_s' => $_system_s,
				);
				$this->db->insert('tbl_statistic_phone', $data);
		}

		if(!empty($results['agent_system'])){
				$data = array(
					'statistic_system_name' => $results['agent_system'],
					'statistic_system_d' => $_system_d,
					'statistic_system_m' => $_system_m,
					'statistic_system_y' => $_system_y,
					'statistic_system_h' => $_system_h,
					'statistic_system_i' => $_system_i,
					'statistic_system_s' => $_system_s,
				);
				$this->db->insert('tbl_statistic_system', $data);
			}else{
				$data = array(
					'statistic_system_name' => 'orther',
					'statistic_system_d' => $_system_d,
					'statistic_system_m' => $_system_m,
					'statistic_system_y' => $_system_y,
					'statistic_system_h' => $_system_h,
					'statistic_system_i' => $_system_i,
					'statistic_system_s' => $_system_s,
				);
				$this->db->insert('tbl_statistic_system', $data);
			}

	}
	
	public function get_lives(){
		$this->_data['lives'] = $this->m_lives->get_livesLatest()->row();
	}

}
