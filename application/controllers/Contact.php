<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends LI_Controller {
	
	function __construct() {
		parent::__construct();
		// $this->load->model(
		// 	array(
		// 		'admin/m_slides'
		// 	));
	}

	public function index(){
		
		$send = array(
			'header' 		=> 'header'
			,'content' 		=> 'contact/index'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'ติดต่อเรา'
			//,'data'			=> $this->_data
		);

		$this->loadPageFrontend($send);
	}

}
