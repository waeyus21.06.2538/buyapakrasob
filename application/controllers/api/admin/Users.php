<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends LI_Controller {
	
	function __construct() {
		
		parent::__construct();
		
		$this->_init();
		
		$this->load->model(
				array(
                     'm_user'
					,'m_user_type'
				));
	}
	
	public function _init(){
		$this->is_active		= 0; 
		$this->is_delete		= 0;
        $this->user_code		= $this->session->userdata('logged_in')['user_code'];
		$this->output->set_content_type('application/json');
		
	}
	
	public function view(){
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
		$_error				= 0;
        $_status            = 0;
        $_start 			= 0;
        $_length			= 10;
        $_all_result		= 0;
		$_reservation_arr 	= array();
		
        if ($this->input->post('start') == TRUE) {
            $_start = intval($this->input->post('start'));
        }
        
        if ($this->input->post('length') == TRUE) {
            $_length = intval($this->input->post('length'));
		}
		
		$_user_name	= $this->input->post('user_name');
        
        $_all_result 	= $this->m_user->countBySearchActiveDelete($_user_name, $this->is_active, $this->is_delete);
        $_obj_users 	=  $this->m_user->viewSearchDataTable($_user_name, $this->is_active, $this->is_delete, $_start, $_length); 
		if($_obj_users['status'] == true && $_obj_users['results'] != null){
			foreach($_obj_users['results'] as $item){
				$user_type_name = 'ไม่ระบุ';
				$_obj_user_type =  $this->m_user_type->viewByKey($item->user_type_code);
				if($_obj_user_type['status'] == true && $_obj_user_type['results'] != null){
					$user_type_name = $_obj_user_type['results']->user_type_name;
				}
				unset($_obj_user_type);
				
				if($item->is_login > 0){
					$_text_status_name = '<span class="badge bg-red">ปิดสิทธิการใช้งาน</span>';
				}else{
					$_text_status_name = '<span class="badge bg-green">ใช้งาน</span>';
				}
				array_push($_reservation_arr,
					array(
						 'CHECKBOK'         => '<label for="select-by-item"><input type="checkbox" class="minimal select-by-item" value="'.$item->user_code.'"  data-name-item="'.$item->user_name.'"></label>'
						,'user_code'		=> $item->user_code
						,'user_name'		=> $item->user_name
						,'status_name'		=> $_text_status_name
						,'user_type_name'	=> $user_type_name
                        ,'update_date'	    => $item->system_add_date
                        ,'action'           => '<a href="'.base_url().'admin/users/edit/'.$this->setBase64_encode($item->user_code).'" class="btn btn-warning btn-flat" data-toggle="tooltip" data-placement="left" title="แก้ไขข้อมูล" ><i class="fa fa-edit" aria-hidden="true"></i></a>'
					));
			}
		}
		unset($_obj_reservation);
        
        
        $_data['recordsTotal']      = $_all_result;
        $_data['recordsFiltered']   = $_all_result;
        $_data['data']              = $_reservation_arr;
        
        $this->output->set_output(json_encode($_data));
    }
    
    public function add(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
        $_status            = 0;
		$_error				= 0;
		$_system_date 		= date('Y-m-d H:i:s');
		
		$this->load->library('form_validation');

		$this->form_validation->set_rules('user_login_name', 'user_login_name', 'required');
		$this->form_validation->set_rules('user_login_pass', 'user_login_pass', 'required');
		$this->form_validation->set_rules('user_login_pass_cf', 'user_login_pass_cf', 'required');
		$this->form_validation->set_rules('user_type_code', 'user_type_code', 'required');
		
		if ($this->form_validation->run() == TRUE){
			$_user_code 			= strtoupper(uniqid());
			$_user_login_name 		= $this->input->post('user_login_name');
			$_user_login_pass 		= $this->input->post('user_login_pass');
			$_user_login_pass_cf 	= $this->input->post('user_login_pass_cf');
			$_user_type_code 		= $this->input->post('user_type_code');
			$_user_name 			= $this->input->post('user_name');
			if($_user_login_pass == $_user_login_pass_cf){
				$_count_user = $this->m_user->countByUserCodeLogin($_user_login_name);
				if($_count_user < 1){
					$this->db->trans_begin();
					$_user_arr = array
						(
							'user_code' 			=> $_user_code
							,'user_login_name' 		=> $_user_login_name
							,'user_login_pass' 		=> $_user_login_pass
							,'user_type_code'		=> $_user_type_code
							,'user_name' 			=> $_user_name
							,'user_img' 			=> '20180223071946.jpeg'
							,'system_add_date' 		=> $_system_date
							,'system_update_date' 	=> $_system_date
							
						);
							
					if(!$this->db->insert('tbl_user', $_user_arr)){
						$_error++;
					}
					
					if ($this->db->trans_status() === FALSE || $_error > 0){
						$this->db->trans_rollback();
						$_message = $this->getMessage('ERROR');
						$_warning = 'warning';
						$_status  = 0;
					}else{
						$_message = $this->getMessage('ADD');
						$_warning = 'success';
						$_status  = 1;
						$this->db->trans_commit();
					}
				}else{
					$_message = $this->getMessage('USERVALIDATION');
					$_warning = 'warning';
					$_status  = 0;
				}
			}else{
				$_message = $this->getMessage('PASSWORDCOMFIRM');
				$_warning = 'warning';
				$_status  = 0;
			}
			
		}else{
			$_message = $this->getMessage('VALIDATION');
			$_warning = 'warning';
			$_status  = 0;	
		}
        
        $_data = array(
             'message'  => $_message['message_detail']
            ,'warning'  => $_warning
            ,'status'   => $_status
            ,'title'    => $_message_title
        );
        
        $this->output->set_output(json_encode($_data));
    }
    
    public function edit(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
        $_status            = 0;
		$_error				= 0;
		$_system_date 		= date('Y-m-d H:i:s');
		
		$this->load->library('form_validation');

		$this->form_validation->set_rules('user_code', 'user_code', 'required');
		$this->form_validation->set_rules('user_login_name', 'user_login_name', 'required');
		$this->form_validation->set_rules('user_login_name_old', 'user_login_name_old', 'required');
		$this->form_validation->set_rules('user_login_pass', 'user_login_pass', 'required');
		$this->form_validation->set_rules('user_login_pass_cf', 'user_login_pass_cf', 'required');
		$this->form_validation->set_rules('user_type_code', 'user_type_code', 'required');
		
		if ($this->form_validation->run() == TRUE){
			$_user_code 			= $this->input->post('user_code');
			$_user_login_name 		= $this->input->post('user_login_name');
			$_user_login_name_old 	= $this->input->post('user_login_name_old');
			$_user_login_pass 		= $this->input->post('user_login_pass');
			$_user_login_pass_cf 	= $this->input->post('user_login_pass_cf');
			$_user_type_code 		= $this->input->post('user_type_code');
			$_user_name 			= $this->input->post('user_name');

			if($_user_login_pass == $_user_login_pass_cf){
				
				if($_user_login_name == $_user_login_name_old){
					$_count_user = 0;
				}else{
					$_count_user = $this->m_user->countByUserCodeLogin($_user_login_name);
				}

				if($_count_user < 1){
					$this->db->trans_begin();

					$_user_arr = array
						(
							'user_login_name' 		=> $_user_login_name
							,'user_login_pass' 		=> $_user_login_pass
							,'user_type_code'		=> $_user_type_code
							,'user_name' 			=> $_user_name
							,'system_update_date' 	=> $_system_date
							
						);
							
					if(!$this->db->update('tbl_user', $_user_arr, array('user_code' => $_user_code))){
						$_error++;
					}
					
					if ($this->db->trans_status() === FALSE || $_error > 0){
						$this->db->trans_rollback();
						$_message = $this->getMessage('ERROR');
						$_warning = 'warning';
						$_status  = 0;
					}else{
						$_message = $this->getMessage('EDIT');
						$_warning = 'success';
						$_status  = 1;
						$this->db->trans_commit();
					}
				}else{
					$_message = $this->getMessage('USERVALIDATION');
					$_warning = 'warning';
					$_status  = 0;
				}
			}else{
				$_message = $this->getMessage('PASSWORDCOMFIRM');
				$_warning = 'warning';
				$_status  = 0;
			}
			
		}else{
			$_message = $this->getMessage('VALIDATION');
			$_warning = 'warning';
			$_status  = 0;	
		}
        
        $_data = array(
             'message'  => $_message['message_detail']
            ,'warning'  => $_warning
            ,'status'   => $_status
            ,'title'    => $_message_title
        );
        
        $this->output->set_output(json_encode($_data));
	}
	
	public function resetpassword(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
        $_status            = 0;
		$_error				= 0;
		$_system_date 		= date('Y-m-d H:i:s');
		$_url				= '';
		
		$this->load->library('form_validation');

		$this->form_validation->set_rules('user_code', 'user_code', 'required');
		$this->form_validation->set_rules('user_type_code', 'user_type_code', 'required');
		$this->form_validation->set_rules('user_pass_old', 'user_pass_old', 'required');
		$this->form_validation->set_rules('user_pass_new', 'user_pass_new', 'required');
		$this->form_validation->set_rules('user_pass_confirm', 'user_pass_confirm', 'required');
		
		if ($this->form_validation->run() == TRUE){
			$_user_code				= $this->setBase64_decode($this->input->post('user_code'));
			$_user_type_code		= $this->setBase64_decode($this->input->post('user_type_code'));
			$_user_pass_old 		= $this->input->post('user_pass_old');
			$_user_pass_new 		= $this->input->post('user_pass_new');
			$_user_pass_confirm 	= $this->input->post('user_pass_confirm');
			if($_user_pass_new == $_user_pass_confirm){
				$_count_user = $this->m_user->countByUserCodePasswordLogin($_user_code, $_user_pass_old);
				if($_count_user > 0){
					$this->db->trans_begin();
					
					$_user_arr = array
						(
							'user_login_pass' 		=> $_user_pass_new
							,'is_resetpw' 			=> 1
							,'system_update_date' 	=> $_system_date
							
						);
							
					if(!$this->db->update('tbl_user', $_user_arr, array('user_code' => $_user_code))){
						$_error++;
					}
					
					if ($this->db->trans_status() === FALSE || $_error > 0){
						$this->db->trans_rollback();
						$_message = $this->getMessage('ERROR');
						$_warning = 'warning';
						$_status  = 0;
					}else{
						$_message = $this->getMessage('ADD');
						$_warning = 'success';
						$_status  = 1;
						$this->db->trans_commit();

						switch($_user_type_code){
							case '5A7F141E7E2A1': // Admin
								$_url = base_url().'report';
							break;
							default: // user
								$_url = base_url().'calendar';
						}
					}
				}else{
					$_message = $this->getMessage('USERVALIDATIONPASSWORD');
					$_warning = 'warning';
					$_status  = 0;
				}
			}else{
				$_message = $this->getMessage('PASSWORDCOMFIRM');
				$_warning = 'warning';
				$_status  = 0;
			}
			
		}else{
			$_message = $this->getMessage('VALIDATION');
			$_warning = 'warning';
			$_status  = 0;	
		}
        
        $_data = array(
             'message'  => $_message['message_detail']
            ,'warning'  => $_warning
            ,'status'   => $_status
			,'title'    => $_message_title
			,'url'		=> $_url
        );
        
        $this->output->set_output(json_encode($_data));
	}
	
	public function register(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
        $_status            = 0;
		$_error				= 0;
		$_system_date 		= date('Y-m-d H:i:s');
		$_error_msg			= '';
		$error				= '';
		$this->load->library('form_validation');

		$this->form_validation->set_rules('user_name', 'ชื่อ-สกุล', 'required');
		$this->form_validation->set_rules('user_login_name', 'ชื่อผู้ใช้งาน', 'required');
		$this->form_validation->set_rules('user_login_pass', 'รหัสผ่าน', 'required');
		$this->form_validation->set_rules('user_login_pass_confirm', 'ยืนยันรหัสผ่าน', 'required');

		if ($this->form_validation->run() == TRUE){
			$_user_code 				= strtoupper(uniqid());
			$_user_name 				= $this->input->post('user_name');
			$_user_login_name 			= $this->input->post('user_login_name');
			$_user_login_pass 			= $this->input->post('user_login_pass');
			$_user_login_pass_confirm 	= $this->input->post('user_login_pass_confirm');

			if($_user_login_pass == $_user_login_pass_confirm){
				$_count_user = $this->m_user->countByUserCodeLogin($_user_login_name);
				if($_count_user < 1){
					$this->db->trans_begin();
				
					$_user_arr = array
						(
							'user_code' 			=> $_user_code
							,'user_login_name' 		=> $_user_login_name
							,'user_login_pass' 		=> $_user_login_pass
							,'user_type_code'		=> '5A7F14B70BB44'
							,'user_name' 			=> $_user_name
							,'user_img' 			=> '20180223071946.jpeg'
							,'system_add_date' 		=> $_system_date
							,'system_update_date' 	=> $_system_date
							
						);
							
					if(!$this->db->insert('tbl_user', $_user_arr)){
						$_error++;
					}
					
					if ($this->db->trans_status() === FALSE || $_error > 0){
						$this->db->trans_rollback();
						$_message = $this->getMessage('ERROR');
						$_warning = 'warning';
						$_status  = 0;
					}else{
						$_message = $this->getMessage('ADD');
						$_warning = 'success';
						$_status  = 1;
						$this->db->trans_commit();
					}
				}else{
					$_message = $this->getMessage('USERVALIDATION');
					$_warning = 'warning';
					$_status  = 0;
				}
			}else{
				$_message = $this->getMessage('PASSWORDCOMFIRM');
				$_warning = 'warning';
				$_status  = 0;
			}

		}else{
			$_message 		= $this->getMessage('VALIDATION');
			$_warning 		= 'warning';
			$_status  		= 0;
			$_error_msg   	= validation_errors();
		}
        
        $_data = array(
             'message'  	=> $_message['message_detail']
            ,'warning'  	=> $_warning
            ,'status'   	=> $_status
			,'title'    	=> $_message_title
			,'error_msg'    => $_error_msg
			,'$error'		=> $error
        );
        
        $this->output->set_output(json_encode($_data));
    }
    
    public function deletes(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
        $_status            = 0;
		$_error				= 0;
		$_system_date 		= date('Y-m-d H:i:s');
		
        $this->load->library('form_validation');
		$this->form_validation->set_rules('item_code[]', 'item_code[]', 'required');

		if ($this->form_validation->run() == TRUE){
			$_item_code 	    = $this->input->post('item_code');
			
			$this->db->trans_begin();	
			if(count($_item_code) > 0){
				foreach($_item_code as $code){
					/* delete table tbl_item */
					$_item_arr = array(
						'is_delete' 	  		=> 1
						,'system_update_date' 	=> $_system_date
					);
						
					if(!$this->db->update('tbl_user', $_item_arr, array('user_code' => $code))){
						$_error++;
					}
				}
			}else{
				$_error = 1;
			}
			
			if ($this->db->trans_status() === FALSE || $_error > 0){
				$this->db->trans_rollback();
				$_message = $this->getMessage('ERROR');
				$_warning = 'warning';
				$_status  = 0;
			}else{
				$_message = $this->getMessage('DELETE');
				$_warning = 'success';
				$_status  = 1;
				$this->db->trans_commit();
			}
			
        }else{
			$_message = $this->getMessage('VALIDATION');
			$_warning = 'warning';
			$_status  = 0;	
		}
        $_data = array(
             'message'  => $_message['message_detail']
            ,'warning'  => $_warning
            ,'status'   => $_status
            ,'title'    => $_message_title
        );
        
        $this->output->set_output(json_encode($_data));
    }
}
