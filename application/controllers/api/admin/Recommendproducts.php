<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class recommendproducts extends LI_Controller {
	
	function __construct() {
		
		parent::__construct();
		
		$this->_init();
		
		$this->load->model(
				array(
					'admin/m_recommendproducts',
					'admin/m_statistics'
				));
	}
	
	public function _init(){
		$this->is_sale			= 0; 
		$this->is_active		= 0; 
		$this->is_delete		= 0;
        $this->user_code		= $this->session->userdata('logged_in')['user_code'];
		$this->output->set_content_type('application/json');
		
	}
	
	public function view(){
		$_data['recommendproducts'] = $this->m_recommendproducts->get_recommendproducts()->result();
		// $this->veiw_brands();
        // $_data['recordsTotal']      = $_all_result;
        // $_data['recordsFiltered']   = $_all_result;
        // $_data['data']              = $_services_arr;
        
        $this->output->set_output(json_encode($_data));
    }
    
    public function storage(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
        $_status            = 0;
		$_error				= 0;
		$_error_msg   		= '';
		$_system_date 		= date('Y-m-d H:i:s');

		$input = $this->input->post(null, true);
		$check = $this->m_recommendproducts->check_recommendproductsCode($input['recommendproducts_code']);

		if($check > 0){
			$_message = $this->getMessage('WARNING');
			$_warning = 'warning';
			$_status  = -1;
		}else{
			$value = $this->_build_data($input);
			$this->db->trans_begin();
			$result = $this->m_recommendproducts->insert($value);
		
			if ($result){
				$_message = $this->getMessage('ADD');
				$_warning = 'success';
				$_status  = 1;
				$this->db->trans_commit();
			
			}else{
				$this->db->trans_rollback();
				$_message = $this->getMessage('ERROR');
				$_warning = 'warning';
				$_status  = 0;
			}
		}
        
        $_data = array(
             'message'  	=> $_message['message_detail']
            ,'warning'  	=> $_warning
            ,'status'   	=> $_status
			,'title'    	=> $_message_title
			,'error_msg'    => $_error_msg
        );
        
        $this->output->set_output(json_encode($_data));
    }
    
    public function update(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
        $_status            = 0;
		$_error				= 0;
		$_error_msg   		= '';
		$_system_date 		= date('Y-m-d H:i:s');

		$input = $this->input->post(null, true);
		$value = $this->_build_data($input);
		$id	   = $input['recommendproducts_id'];

		$this->db->trans_begin();
		$result = $this->m_recommendproducts->update($id, $value);
		
		if ($result){
			$_message = $this->getMessage('ADD');
			$_warning = 'success';
			$_status  = 1;
			$this->db->trans_commit();
		
		}else{
			$this->db->trans_rollback();
			$_message = $this->getMessage('ERROR');
			$_warning = 'warning';
			$_status  = 0;
		}
        
        $_data = array(
             'message'  	=> $_message['message_detail']
            ,'warning'  	=> $_warning
            ,'status'   	=> $_status
			,'title'    	=> $_message_title
			,'error_msg'    => $_error_msg
        );
        
        $this->output->set_output(json_encode($_data));
	}

	public function _build_data($input){
		$_system_date 		= date('Y-m-d H:i:s');

        $value['recommendproducts_code'] 		= $input['recommendproducts_code'];
		$value['recommendproducts_title'] 		= $input['recommendproducts_title'];
		$value['recommendproducts_type'] 		= $input['recommendproducts_type'];
		$value['recommendproducts_type_detail'] = $input['recommendproducts_type_detail'];
		$value['recommendproducts_brand'] 		= $input['recommendproducts_brand'];
		$value['recommendproducts_price'] 		= $input['recommendproducts_price'];
		$value['recommendproducts_discount'] 	= $input['recommendproducts_discount'];
		$value['recommendproducts_detail'] 		= html_escape($input['recommendproducts_detail'], FALSE);

		//upload Image
		$path 		= 'upload/recommendproducts'.'/'.date('Y').'/'.date('m').'/';
		$pathName 	= 'recommendproducts'.'/'.date('Y').'/'.date('m').'/';
	
		if(!is_dir($path)){
			mkdir($path,0755,TRUE);
		} 
		
		$_file_name		= '';
		$this->load->library('upload');
		$this->upload->initialize($this->setUploadOptions($pathName));
		if ($this->upload->do_upload('recommendproducts_img')){  
			$_file_name 		= $this->upload->data('file_name');
	   	}  

	   	$this->load->library('image_lib');
		if(!empty($_file_name)){
			// ปรับขนาดไฟล์ภาพ
			$config['image_library'] = 'gd2';  
			$config['source_image'] = $path.$_file_name; 
			$config['create_thumb'] = FALSE;  
			$config['maintain_ratio'] = FALSE; 

			$this->load->library('user_agent');
			$agent_system = $this->agent->platform();
			if($agent_system == "iOS" || $agent_system == "Android"){
				$config['rotation_angle'] = '270';	
				$this->image_lib->initialize($config);
				$this->image_lib->rotate();
			}
		
			$config['quality'] = '60%'; 
			$config['width'] = 900; 
			$config['height'] = 1200; 

			$this->image_lib->clear(); 
			$this->image_lib->initialize($config);
			$this->image_lib->resize();

			// กำหนดค่าที่จะบันทึกใน database
			$value['recommendproducts_img'] 	= $path.$_file_name;

			//ลบภาพเก่าก่อน เมื่อแก้ไข
			@unlink($input['recommendproducts_img_edit']);
		}else{
			$value['recommendproducts_img']		= $input['recommendproducts_img_edit'];
		}
		

        if ( $input['mode'] == 'create' ) {
			$value['system_add_date']	 = $_system_date;
			$value['system_update_date'] = $_system_date;
            // $value['created_by'] = $this->session->users['user_id'];
        } else {
            $value['system_update_date'] = $_system_date;
            // $value['updated_by'] = $this->session->users['user_id'];
        }
        return $value;
	}
    
	public function delete(){
		$_message_title 		= 'ข้อความแจ้งเตือน !';
		$_message				= '';
		$_error					= 0;
		$_warning				= '';
		$_error_msg   		= '';
		$_system_date 			= date('Y-m-d H:i:s');

		$_recommendproducts_code	    = $this->setBase64_decode($this->input->post('recommendproducts_code'));
		$item 							= $this->m_recommendproducts->get_recommendproductsById($_recommendproducts_code)->row();
		$_item_arr ='';	
	
		if(!empty($_recommendproducts_code)){
			$_item_arr = array('is_delete' => 1,'system_update_date' => $_system_date);
		}

		$this->db->trans_begin();
		$result = $this->db->update('tbl_recommendproducts', $_item_arr, array('recommendproducts_no' => $_recommendproducts_code));
		if ($result){
			@unlink($item->recommendproducts_img);
			$this->deleteImgDetail($_recommendproducts_code);

			$_message = $this->getMessage('DELETE');
			$_warning = 'success';
			$_status  = 1;
			$this->db->trans_commit();
		
		}else{
			$this->db->trans_rollback();
			$_message = $this->getMessage('ERROR');
			$_warning = 'warning';
			$_status  = 0;
		}
        
        $_data = array(
             'message'  	=> $_message['message_detail']
            ,'warning'  	=> $_warning
            ,'status'   	=> $_status
			,'title'    	=> $_message_title
			,'error_msg'    => $_error_msg
        );
        
        $this->output->set_output(json_encode($_data));
	}

	// นับการดู
	// public function countRecommendproducts(){
	// 	$_recommendproducts_code	    = $this->input->post('recommendproducts_code');
	// 	$this->m_recommendproducts->plus_view($_recommendproducts_code);
	// }

	// โชว์สินค้า1รายการ
	public function showItem(){
		$_recommendproducts_code		= $this->setBase64_decode($this->input->post('recommendproducts_code'));
		$result 					= $this->m_recommendproducts->get_recommendproductsById($_recommendproducts_code)->row();
		$result_img_detai 			= $this->m_recommendproducts->get_recommendproductsImgDetailById($_recommendproducts_code)->result();

		// แปลงวันที่ให้เป็นไทย
		$_date = $this->getDate($result->system_add_date);
		
		// แปลงวันที่ให้เป็นไทย และมีเวลา		
		if($result->system_sale_date != null){
			$_date_time = $this->getDateTime($result->system_sale_date);
		}else{
			$_date_time = '-';
		}
	
		if ($result){
			$_data = array(
				'recommendproducts_code'  			=> $result->recommendproducts_code
			   ,'recommendproducts_title'  			=> $result->recommendproducts_title
			   ,'recommendproducts_type'  			=> $result->recommendproducts_type
			   ,'recommendproducts_type_detail'  	=> $result->recommendproducts_type_detail
			   ,'recommendproducts_brand'  			=> $result->recommendproducts_brand
			   ,'recommendproducts_price'  			=> $result->recommendproducts_price
			   ,'recommendproducts_discount'  		=> $result->recommendproducts_discount
			   ,'recommendproducts_detail'  		=> $result->recommendproducts_detail
			   ,'recommendproducts_img'  			=> base_url().$result->recommendproducts_img
			   ,'is_sale'  							=> $result->is_sale
			   ,'system_sale_date'  				=> $_date_time
			   ,'number_view'  						=> number_format($result->number_view)
			   ,'system_add_date'  					=> $_date
			   ,'image_error'  						=> base_url()."upload/error/error.jpg"
			   ,'images_detail'  					=> $result_img_detai
			);	
		}
        $this->output->set_output(json_encode($_data));
	}
	
	// เปิด-ปิด สถานะสินค้า
	public function active(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
		$_status            = 0;
		$_error_msg   		= '';
		$_system_date 		= date('Y-m-d H:i:s');
		
		$_active	    				= $this->input->post('active');
		$_recommendproducts_code	    = $this->setBase64_decode($this->input->post('recommendproducts_code'));
	
		$_item_arr ='';	
		if(!empty($_recommendproducts_code)){
			$this->db->trans_begin();
			if($_active === 'true'){
				$_item_arr = array('is_active' => 0,'system_update_date' => $_system_date);
			}else{
				$_item_arr = array(
					'is_active' => 1,
					'system_update_date' => $_system_date);
			}
			$result ='';
			$result = $this->db->update('tbl_recommendproducts', $_item_arr, array('recommendproducts_no' => $_recommendproducts_code));
			
			if ($result){
			$_message = $this->getMessage('EDIT');
			$_warning = 'success';
			$_status  = 1;
			$this->db->trans_commit();
		
			}else{
				$this->db->trans_rollback();
				$_message = $this->getMessage('ERROR');
				$_warning = 'warning';
				$_status  = 0;
			}		
		}
	
			
		$_data = array(
			'message'  	=> $_message['message_detail']
		   ,'warning'  	=> $_warning
		   ,'status'   	=> $_status
		   ,'title'    	=> $_message_title
		   ,'error_msg'    => $_error_msg
	   );
		
		$this->output->set_output(json_encode($_data));
	}

	// เปิด-ปิด สถานะการขาย
	public function sale(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
		$_status            = 0;
		$_error_msg   		= '';
		$_system_date 		= date('Y-m-d H:i:s');
		$_system_d 			= date('d');
		$_system_m			= date('m');
		$_system_y 			= date('Y');
		$_system_h 			= date('H');
		$_system_i 			= date('i');
		$_system_s 			= date('s');

		
		$_sale	    					= $this->input->post('sale');
		$_recommendproducts_code	    = $this->setBase64_decode($this->input->post('recommendproducts_code'));
		$_value_sale	    			= $this->input->post('value_sale');
	
		$_item_arr ='';	
		if(!empty($_recommendproducts_code)){
			$this->db->trans_begin();
			if($_sale === 'true'){
				$_item_arr = array('is_sale' => 0,'system_sale_date' => $_system_date);
				$result = $this->db->update('tbl_recommendproducts', $_item_arr, array('recommendproducts_no' => $_recommendproducts_code));
				
				if($result){
					$result_p  = $this->m_recommendproducts->get_recommendproductsById($_recommendproducts_code)->row();

					$value['statistic_sale_sale_type'] 				= $_value_sale;
					$value['statistic_sale_products_code'] 			= $result_p->recommendproducts_code;
					$value['statistic_sale_products_type'] 			= $result_p->recommendproducts_type;
					$value['statistic_sale_products_type_detail'] 	= $result_p->recommendproducts_type_detail;
					$value['statistic_sale_products_brand'] 		= $result_p->recommendproducts_brand;
					$value['statistic_sale_products_price'] 		= $result_p->recommendproducts_price;
					$value['statistic_sale_products_discount']	 	= $result_p->recommendproducts_discount;
					$value['statistic_sale_products_grade']	 		= $result_p->recommendproducts_grade;
					$value['statistic_sale_d']	 					= $_system_d;
					$value['statistic_sale_m']						= $_system_m;
					$value['statistic_sale_y']						= $_system_y;
					$value['statistic_sale_h']						= $_system_h;
					$value['statistic_sale_i']						= $_system_i;
					$value['statistic_sale_s']						= $_system_s;
					$value['statistic_sale_date_time']				= $_system_date;

					$chec = $this->m_statistics->count_chec($result_p->recommendproducts_code);
					if($chec == 0){
						$this->m_statistics->insert_sale($value);
					}
				}

			}else{
				$_item_arr = array(
					'is_sale' => 1,
					'system_sale_date' => null,
					'system_update_date' => $_system_date);

					$result = $this->db->update('tbl_recommendproducts', $_item_arr, array('recommendproducts_no' => $_recommendproducts_code));
			}
		
			if ($result){
			$_message = $this->getMessage('EDIT');
			$_warning = 'success';
			$_status  = 1;
			$this->db->trans_commit();
		
			}else{
				$this->db->trans_rollback();
				$_message = $this->getMessage('ERROR');
				$_warning = 'warning';
				$_status  = 0;
			}		
		}
	
			
		$_data = array(
			'message'  	=> $_message['message_detail']
		   ,'warning'  	=> $_warning
		   ,'status'   	=> $_status
		   ,'title'    	=> $_message_title
		   ,'error_msg'    => $_error_msg
	   );
		
		$this->output->set_output(json_encode($_data));
	}

	// เพิ่มรายละเอียดรูปภาพ (หลายรูป)
	public function addImgDetail(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
		$_status            = 0;
		$_error_msg   		= '';
		$_system_date 		= date('Y-m-d H:i:s');
		
		$_recommendproducts_code	    = $this->setBase64_decode($this->input->post('code'));

		if(count($this->input->post('filemultiple-edit[]')) > 0){
			foreach($this->input->post('filemultiple-edit[]') as $value){
				@unlink($value);
				$this->m_recommendproducts->deleteRecommendproductsImgDetailByid($_recommendproducts_code);
			}
		}
	
		// ทำการบันทึกไฟล์ภาพ
		if (!empty($_recommendproducts_code)) {

			//upload Image
			$path 		= 'upload/recommendproducts_detail'.'/'.date('Y').'/'.date('m').'/';
			$pathName 	= 'recommendproducts_detail'.'/'.date('Y').'/'.date('m').'/'; 

			$upload = $this->do_uploadMultiple('filemultiple[]',TRUE,$pathName);

			$arr_file_recommendproducts_img_detail = array();
			if(isset($upload['filenames']) && count($upload['filenames']) > 0){

				foreach($upload['filenames'] as $item){
		
					$file_path_db = 	$path.$item['index']['file_name'];

					array_push($arr_file_recommendproducts_img_detail, array(
						'recommendproducts_img_detail_path'  	=> $file_path_db
						,'recommendproducts_no'            		=> $_recommendproducts_code
						,'system_add_date' 						=> $_system_date
					));  
				}

				// ปรับขนาดไฟล์ภาพ
				$this->load->library('image_lib');
				foreach($upload['filenames'] as $item_img){
	
					$config['image_library'] = 'gd2';  
					$config['source_image'] = $path.$item_img['index']['file_name'];
					$config['create_thumb'] = FALSE;  
					$config['maintain_ratio'] = FALSE; 

					$this->load->library('user_agent');
					$agent_system = $this->agent->platform();
					if($agent_system == "iOS" || $agent_system == "Android"){
						$config['rotation_angle'] = '270';	
						$this->image_lib->initialize($config);
						$this->image_lib->rotate();
					}
				
					$config['quality'] = '60%'; 
					$config['width'] = 900; 
					$config['height'] = 1200; 

					$this->image_lib->clear(); 
					$this->image_lib->initialize($config);
					$this->image_lib->resize();
				}

				if ($this->m_recommendproducts->insert_multiple($arr_file_recommendproducts_img_detail)):
						$_message = $this->getMessage('ADD');
						$_warning = 'success';
						$_status  = 1;
						$this->db->trans_commit();
				endif;
			}
		} else {
			$this->db->trans_rollback();
			$_message = $this->getMessage('ERROR');
			$_warning = 'warning';
			$_status  = 0;
		}
	
			
		$_data = array(
			'message'  	=> $_message['message_detail']
		   ,'warning'  	=> $_warning
		   ,'status'   	=> $_status
		   ,'title'    	=> $_message_title
		   ,'error_msg'    => $_error_msg
	   );
		
		$this->output->set_output(json_encode($_data));
	}

	public function deleteImgDetail($_recommendproducts_code){

		$result = $this->m_recommendproducts->get_recommendproductsImgDetailById($_recommendproducts_code)->result();
		if($result){
			foreach($result as $value){
				@unlink($value->recommendproducts_img_detail_path);
				$this->m_recommendproducts->deleteRecommendproductsImgDetailByid($_recommendproducts_code);
			}
		}

	}
}
