<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class statistics extends LI_Controller {
	
	function __construct() {
		
		parent::__construct();
		
		$this->_init();
		
		$this->load->model(
				array(
					'admin/m_recommendproducts',
					'admin/m_statistics',
					'admin/m_lives',
					'admin/m_brands',
				));
	}
	
	public function _init(){

        $this->user_code		= $this->session->userdata('logged_in')['user_code'];
		$this->output->set_content_type('application/json');
		
	}
	
	public function view(){
		$_data['statistics'] = $this->m_recommendproducts->get_recommendproducts()->result();
        
        // $_data['recordsTotal']      = $_all_result;
        // $_data['recordsFiltered']   = $_all_result;
        // $_data['data']              = $_services_arr;
        
        $this->output->set_output(json_encode($_data));
	}
	
	// เก็บสถิติการกดปุ่มเข้าไปดุไลฟ์สดจากหน้าเว็บ
	public function add(){
		$_system_d 		= date('d');
		$_system_m		= date('m');
		$_system_y 		= date('Y');
		$_system_h 		= date('H');
		$_system_i 		= date('i');
		$_system_s 		= date('s');
		$_system_date 	= date('Y-m-d H:i:s');
	
		$_lives_no	    = $this->input->post('lives_code');
		$_lives_title	    = $this->input->post('lives_title');
		
        $value['lives_no'] 					= $_lives_no;
        $value['lives_title'] 				= $_lives_title;
		$value['statistic_lives_d'] 		= $_system_d;
		$value['statistic_lives_m'] 		= $_system_m;
		$value['statistic_lives_y'] 		= $_system_y;
		$value['statistic_lives_h']	 		= $_system_h;
		$value['statistic_lives_i']	 		= $_system_i;
		$value['statistic_lives_s']	 		= $_system_s;
		$value['statistic_lives_date_time']	= $_system_date;
	
		 $this->m_statistics->insert_lives($value);
		 $this->m_lives->plus_view($_lives_no);
	}

	public function das_browser_date(){
		$hh 				= ['01', '02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','00'];
		$cuont_date_time	= array();

		$_date = $this->input->post('date');
		list($yy,$mm,$dd) = explode("-",$_date);
			
		for ($x = 0; $x <= 23; $x++) {
			$result = $this->m_statistics->countAllBrowserByDateTime($yy, $mm, $dd, $hh[$x]);
			array_push($cuont_date_time, $result);
		}

		if($cuont_date_time){
			$_data['status'] 		= 1;
			$_data['countDateTime'] = $cuont_date_time;
		}else{
			$_data['status'] 		= 0;
		}	
		
		$count_all_browser_date = $this->m_statistics->countAllBrowserByDate($yy, $mm, $dd);
		if($count_all_browser_date){
		$count_all_browser_date = $this->m_statistics->countAllBrowserByDate($yy, $mm, $dd);
			$_data['CountAllBrowserDate'] = $count_all_browser_date;
		}
		
        $this->output->set_output(json_encode($_data));
	}

	public function das_viewLives(){
		$hh 				= ['01', '02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','00'];
		$cuont_date_time	= array();

		$_date = $this->input->post('date');
		list($yy,$mm,$dd) = explode("-",$_date);
			
		for ($x = 0; $x <= 23; $x++) {
			$result = $this->m_statistics->countByDateTime($yy, $mm, $dd, $hh[$x]);
			array_push($cuont_date_time, $result);
		}

		if($cuont_date_time){
			$_data['status'] 		= 1;
			$_data['countDateTime'] = $cuont_date_time;
		}else{
			$_data['status'] 		= 0;
		}	
		
		$result_title = $this->m_statistics->get_livesTitle($yy, $mm, $dd)->row();
		if($result_title){
			$_data['livesTitle'] = $result_title->lives_title;
		}else{
			$_data['livesTitle'] = "ไม่มีหัวข้อ";
		}
		
        $this->output->set_output(json_encode($_data));
	}

	public function das_sale_prduct_grade_month(){
		$month			= ['01', '02','03','04','05','06','07','08','09','10','11','12'];
		$cuont_month_gradeA	= array();
		$cuont_month_gradeB	= array();

		// $_date = $this->input->post('date');
		// list($yy,$mm,$dd) = explode("-",$_date);
		
		$yy = "2020";

		for ($x = 0; $x <= 11; $x++) {
			$resultGradeA = $this->m_statistics->countByMonthGradeA($yy, $month[$x]);
			array_push($cuont_month_gradeA, $resultGradeA);

			$resultGradeB = $this->m_statistics->countByMonthGradeB($yy, $month[$x]);
			array_push($cuont_month_gradeB, $resultGradeB);
		}

		if($cuont_month_gradeA){
			$_data['status'] 			= 1;
			$_data['countMonthGradeA'] 	= $cuont_month_gradeA;
			$_data['countMonthGradeB'] 	= $cuont_month_gradeB;
			$_data['countAllGradeA'] 	= $this->m_statistics->countByYearGradeA($yy);
			$_data['countAllGradeB'] 	= $this->m_statistics->countByYearGradeB($yy);
			$_data['countAll'] 			= $this->m_statistics->countByYear($yy);
		}else{
			$_data['status'] 		= 0;
		}	
		
		// $count_all_browser_date = $this->m_statistics->countAllBrowserByDate($yy, $mm, $dd);
		// if($count_all_browser_date){
		// $count_all_browser_date = $this->m_statistics->countAllBrowserByDate($yy, $mm, $dd);
		// 	$_data['CountAllBrowserDate'] = $count_all_browser_date;
		// }
		
        $this->output->set_output(json_encode($_data));
	}

	public function das_sale_month(){
		$month			= ['01', '02','03','04','05','06','07','08','09','10','11','12'];
		$sale_normal	= array();
		$sale_No_normal	= array();

		// $_date = $this->input->post('date');
		// list($yy,$mm,$dd) = explode("-",$_date);
		
		$yy = "2020";

		for ($x = 0; $x <= 11; $x++) {
			$resultSaleNormal = $this->m_statistics->get_salenormal($yy, $month[$x])->row();
			if($resultSaleNormal->statistic_sale_products_discount == null){
				array_push($sale_normal, 0);
			}else{
				array_push($sale_normal, $resultSaleNormal->statistic_sale_products_discount);
			}
			

			$resultSaleNoNormal = $this->m_statistics->get_saleNonormal($yy, $month[$x])->row();
			if($resultSaleNoNormal->statistic_sale_products_discount == null){
				array_push($sale_No_normal, 0);
			}else{
				array_push($sale_No_normal, $resultSaleNoNormal->statistic_sale_products_discount);
			}
			
		}
		// $this->arr($sale_No_normal);

		if($sale_normal){
			$_data['status'] 				= 1;
			$_data['salenormal'] 			= $sale_normal;
			$_data['saleNonormal'] 			= $sale_No_normal;
			$_data['sumallsalenormal'] 		= number_format( $this->m_statistics->get_sumAllSaleNormal($yy)->row()->statistic_sale_products_discount);
			$_data['sumallsaleNonormal'] 	= number_format( $this->m_statistics->get_sumAllSaleNONormal($yy)->row()->statistic_sale_products_discount);
			$_data['sumallsale'] 			= number_format( $this->m_statistics->get_sumAllSale($yy)->row()->statistic_sale_products_discount);
		}else{
			$_data['status'] 		= 0;
		}	
	
        $this->output->set_output(json_encode($_data));
	}

	public function das_sale_brands(){

		$brands_no 			= array();
		$brands_name 		= array();
		$result_brands		= $this->m_brands->veiw_brands()->result();

		if($result_brands):
			foreach($result_brands as $item):
				array_push($brands_no, $item->brands_no);
				array_push($brands_name, $item->brands_name);
			endforeach;
			array_push($brands_no, "0");
			array_push($brands_name, "ไม่มีเบรน");
		endif;

		$cuont_brands_gradeA	= array();
		$cuont_brands_gradeB	= array();

		// $_date = $this->input->post('date');
		// list($yy,$mm,$dd) = explode("-",$_date);
		
		$yy = "2020";

		foreach ($brands_no as $value) {
			$resultGradeA = $this->m_statistics->countBrandsByGradeA($yy, $value);
			array_push($cuont_brands_gradeA, $resultGradeA);

			$resultGradeB = $this->m_statistics->countBrandsByGradeB($yy, $value);
			array_push($cuont_brands_gradeB, $resultGradeB);
		}

		if($brands_name){
			$_data['status'] 		= 1;
			$_data['brands'] 		= $brands_name;
			$_data['brandsgradeA'] 	= $cuont_brands_gradeA;
			$_data['brandsgradeB'] 	= $cuont_brands_gradeB;
			
		}else{
			$_data['status'] 		= 0;
		}	
	
        $this->output->set_output(json_encode($_data));
	}

}
