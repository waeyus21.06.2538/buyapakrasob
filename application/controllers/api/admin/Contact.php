<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends LI_Controller {
	
	function __construct() {
		
		parent::__construct();
		
		$this->_init();
		
		$this->load->model(
				array(
                    'admin/m_contact'
				));
	}
	
	public function _init(){
		$this->is_active		= 0; 
		$this->is_delete		= 0;
        $this->user_code		= $this->session->userdata('logged_in')['user_code'];
		$this->output->set_content_type('application/json');
		
	}
	
	public function view(){
		$_data	            = array();

		$_data['contact'] = $this->m_contact->get_contact()->row();
		$_data['contact_social'] = $this->m_contact->get_contact_social()->result();
        
        // $_data['recordsTotal']      = $_all_result;
        // $_data['recordsFiltered']   = $_all_result;
        // $_data['data']              = $_services_arr;
        
        $this->output->set_output(json_encode($_data));
    }
    
    public function storage(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
        $_status            = 0;
		$_error				= 0;
		$_error_msg   		= '';
		$_system_date 		= date('Y-m-d H:i:s');

		$input = $this->input->post(null, true);
		$value = $this->_build_data($input);

		$this->db->trans_begin();
		$result = $this->m_contact->insert($value);
		if ($result){
			if(count($input['contact_socialcol_title']) > 0){
				foreach($input['contact_socialcol_title'] as $key => $item):
					$value_arr = array(
						'contact_socialcol_title' => $item,
						'contact_socialcol_link' => $input['contact_socialcol_link'][$key],
						'contact_socialcol_icon'  => $input['contact_socialcol_icon'][$key]
					);
					$result2 = $this->m_contact->insert_contact_social($value_arr);
				endforeach;
			}
			
			if ($result2):
				$_message = $this->getMessage('ADD');
				$_warning = 'success';
				$_status  = 1;
				$this->db->trans_commit();
			else:
				$this->db->trans_rollback();
				$_message = $this->getMessage('ERROR');
				$_warning = 'warning';
				$_status  = 0;
			endif;
		
		}else{
			$this->db->trans_rollback();
			$_message = $this->getMessage('ERROR');
			$_warning = 'warning';
			$_status  = 0;
		}
        
        $_data = array(
             'message'  	=> $_message['message_detail']
            ,'warning'  	=> $_warning
            ,'status'   	=> $_status
			,'title'    	=> $_message_title
			,'error_msg'    => $_error_msg
        );
        
        $this->output->set_output(json_encode($_data));
    }
    
    public function update(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
        $_status            = 0;
		$_error				= 0;
		$_error_msg   		= '';
		$_system_date 		= date('Y-m-d H:i:s');

		$input = $this->input->post(null, true);
		$value = $this->_build_data($input);
		$id	   = $input['contact_id'];

		$this->db->trans_begin();
		$result = $this->m_contact->update($id, $value);
		if ($result){
			$rs = $this->m_contact->truncate();
			if($rs):
				if(count($input['contact_socialcol_title']) > 0){
					foreach($input['contact_socialcol_title'] as $key => $item):
						$value_arr = array(
							'contact_socialcol_title' => $item,
							'contact_socialcol_link'  => $input['contact_socialcol_link'][$key],
							'contact_socialcol_icon'  => $input['contact_socialcol_icon'][$key]
						);
						$result2 = $this->m_contact->insert_contact_social($value_arr);
					endforeach;

					if ($result2):
						$_message = $this->getMessage('EDIT');
						$_warning = 'success';
						$_status  = 1;
						$this->db->trans_commit();
					else:
						$this->db->trans_rollback();
						$_message = $this->getMessage('ERROR');
						$_warning = 'warning';
						$_status  = 0;
					endif;
				}
			endif;
		}else{
			$this->db->trans_rollback();
			$_message = $this->getMessage('ERROR');
			$_warning = 'warning';
			$_status  = 0;
		}
        
        $_data = array(
             'message'  	=> $_message['message_detail']
            ,'warning'  	=> $_warning
            ,'status'   	=> $_status
			,'title'    	=> $_message_title
			,'error_msg'    => $_error_msg
        );
        
        $this->output->set_output(json_encode($_data));
	}

	public function _build_data($input){
		$_system_date 		= date('Y-m-d H:i:s');

		$value['contact_address'] 	= html_escape($input['contact_address'], FALSE);
        $value['contact_tel'] 		= $input['contact_tel'];
		$value['contact_email'] 	= $input['contact_email'];
		
		$rest = substr($input['contact_map'], 16, -94);
		$rest_front = substr($input['contact_map'], 0, 16);
		$rest_back = substr($input['contact_map'], -94);

        $value['contact_map'] 		= $rest;
        $value['cut_text_front'] 	= $rest_front;
        $value['cut_text_back'] 	= $rest_back;
        if ( $input['mode'] == 'create' ) {
			$value['system_add_date']	 = $_system_date;
			$value['system_update_date'] = $_system_date;
            // $value['created_by'] = $this->session->users['user_id'];
        } else {
            $value['system_update_date'] = $_system_date;
            // $value['updated_by'] = $this->session->users['user_id'];
        }
        return $value;
	}
    
    public function deletes(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
        $_status            = 0;
		$_error				= 0;
		$_system_date 		= date('Y-m-d H:i:s');
		
        $this->load->library('form_validation');
		$this->form_validation->set_rules('item_code[]', 'item_code[]', 'required');

		if ($this->form_validation->run() == TRUE){
			$_item_code 	    = $this->input->post('item_code');
			
			$this->db->trans_begin();	
			if(count($_item_code) > 0){
				foreach($_item_code as $code){
					/* delete table */
					$_item_arr = array(
						'is_delete' 	  		=> 1
						,'system_update_date' 	=> $_system_date
					);
						
					if(!$this->db->update('tbl_services', $_item_arr, array('services_code' => $code))){
						$_error++;
					}

					if(!$this->db->update('tbl_services_list', $_item_arr, array('services_code' => $code))){
						$_error++;
					}
				}
			}else{
				$_error = 1;
			}
			
			if ($this->db->trans_status() === FALSE || $_error > 0){
				$this->db->trans_rollback();
				$_message = $this->getMessage('ERROR');
				$_warning = 'warning';
				$_status  = 0;
			}else{
				$_message = $this->getMessage('DELETE');
				$_warning = 'success';
				$_status  = 1;
				$this->db->trans_commit();
			}
			
        }else{
			$_message = $this->getMessage('VALIDATION');
			$_warning = 'warning';
			$_status  = 0;	
		}
        $_data = array(
             'message'  => $_message['message_detail']
            ,'warning'  => $_warning
            ,'status'   => $_status
            ,'title'    => $_message_title
        );
        
        $this->output->set_output(json_encode($_data));
    }
}
