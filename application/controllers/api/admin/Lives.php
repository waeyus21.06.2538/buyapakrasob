<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class lives extends LI_Controller {
	
	function __construct() {
		
		parent::__construct();
		
		$this->_init();
		
		$this->load->model(
				array(
					'admin/m_lives',
				));
	}
	
	public function _init(){
		$this->is_sale			= 0; 
		$this->is_active		= 0; 
		$this->is_delete		= 0;
        $this->user_code		= $this->session->userdata('logged_in')['user_code'];
		$this->output->set_content_type('application/json');
		
	}
	
	public function view(){
		// $_data['recommendproducts'] = $this->m_recommendproducts->get_recommendproducts()->result();
        // $this->output->set_output(json_encode($_data));
    }
    
    public function storage(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
        $_status            = 0;
		$_error				= 0;
		$_error_msg   		= '';
		$_system_date 		= date('Y-m-d H:i:s');

		$input = $this->input->post(null, true);
		$value = $this->_build_data($input);

		$this->db->trans_begin();
		$result = $this->m_lives->insert($value);
	
		if ($result){
			$_message = $this->getMessage('ADD');
			$_warning = 'success';
			$_status  = 1;
			$this->db->trans_commit();
		
		}else{
			$this->db->trans_rollback();
			$_message = $this->getMessage('ERROR');
			$_warning = 'warning';
			$_status  = 0;
		}
        
        $_data = array(
             'message'  	=> $_message['message_detail']
            ,'warning'  	=> $_warning
            ,'status'   	=> $_status
			,'title'    	=> $_message_title
			,'error_msg'    => $_error_msg
        );
        
        $this->output->set_output(json_encode($_data));
    }
    
	public function _build_data($input){
		$_system_date 		= date('Y-m-d H:i:s');
		
		// if(!empty($value['lives_id'])){
		// 	$this->m_lives->delete($value['lives_id']);
		// }

        $value['lives_link'] 		= $input['lives_link'];
		$value['lives_title'] 		= $input['lives_title'];
		$value['lives_date'] 		= $input['lives_date'];
		$value['lives_link_detail'] = $input['lives_link_detail'];
		$value['system_add_date']	 = $_system_date;
	
        return $value;
	}

	public function delete(){
		$_message_title 		= 'ข้อความแจ้งเตือน !';
		$_message				= '';
		$_error					= 0;
		$_warning				= '';
		$_error_msg   			= '';
		$_system_date 			= date('Y-m-d H:i:s');

		$_lives_no    = $this->input->post('lives_no');
	
		$_item_arr ='';	
	
		if(!empty($_lives_no)){
			$_item_arr = array('is_delete' => 1);
		}

		$this->db->trans_begin();
		$result = $this->db->update('tbl_lives', $_item_arr, array('lives_no' => $_lives_no));
		if ($result){
			$_message = $this->getMessage('DELETE');
			$_warning = 'success';
			$_status  = 1;
			$this->db->trans_commit();
		
		}else{
			$this->db->trans_rollback();
			$_message = $this->getMessage('ERROR');
			$_warning = 'warning';
			$_status  = 0;
		}
        
        $_data = array(
             'message'  	=> $_message['message_detail']
            ,'warning'  	=> $_warning
            ,'status'   	=> $_status
			,'title'    	=> $_message_title
			,'error_msg'    => $_error_msg
        );
        
        $this->output->set_output(json_encode($_data));
	}

	// โชว์ไลฟ์สด 1 รายการ
	public function showLives(){
		$_lives_no	= $this->input->post('lives_no');
		$result 	= $this->m_lives->get_livesById($_lives_no)->row();
	
		// แปลงวันที่ให้เป็นไทย และมีเวลา		
		if($result->system_add_date != null){
			$_date_time = $this->getDateTime($result->system_add_date);
		}else{
			$_date_time = '-';
		}
	
		if ($result){
			$_data = array(
				'lives_title'  			=> $result->lives_title
			   ,'lives_link'  			=> $result->lives_link
			   ,'number_view'  			=> number_format($result->number_view)
			   ,'system_add_date'  		=> $_date_time
			
			);	
		}
        $this->output->set_output(json_encode($_data));
	}
    
}
