<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class generalproducts extends LI_Controller {
	
	function __construct() {
		
		parent::__construct();
		
		$this->_init();
		
		$this->load->model(
				array(
					'admin/m_generalproducts',
					'admin/m_brands',
					'admin/m_statistics',
				));
	}
	
	public function _init(){
		$this->is_sale			= 0; 
		$this->is_active		= 0; 
		$this->is_delete		= 0;
        $this->user_code		= $this->session->userdata('logged_in')['user_code'];
		$this->output->set_content_type('application/json');
		
	}
	
	public function view(){
		$_data['generalproducts'] = $this->m_generalproducts->get_generalproducts()->result();
		$this->veiw_brands();
        // $_data['recordsTotal']      = $_all_result;
        // $_data['recordsFiltered']   = $_all_result;
        // $_data['data']              = $_services_arr;
        
        $this->output->set_output(json_encode($_data));
    }
    
    public function storage(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
        $_status            = 0;
		$_error				= 0;
		$_error_msg   		= '';
		$_system_date 		= date('Y-m-d H:i:s');

		$input = $this->input->post(null, true);
		$check = $this->m_generalproducts->check_generalproductsCode($input['generalproducts_code']);

		if($check > 0){
			$_message = $this->getMessage('WARNING');
			$_warning = 'warning';
			$_status  = -1;
		}else{
			$value = $this->_build_data($input);
			$this->db->trans_begin();
			$result = $this->m_generalproducts->insert($value);
		
			if ($result){
				$_message = $this->getMessage('ADD');
				$_warning = 'success';
				$_status  = 1;
				$this->db->trans_commit();
			
			}else{
				$this->db->trans_rollback();
				$_message = $this->getMessage('ERROR');
				$_warning = 'warning';
				$_status  = 0;
			}
		}
		
		
        $_data = array(
             'message'  	=> $_message['message_detail']
            ,'warning'  	=> $_warning
            ,'status'   	=> $_status
			,'title'    	=> $_message_title
			,'error_msg'    => $_error_msg
        );
        
        $this->output->set_output(json_encode($_data));
    }
    
    public function update(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
        $_status            = 0;
		$_error				= 0;
		$_error_msg   		= '';
		$_system_date 		= date('Y-m-d H:i:s');

		$input = $this->input->post(null, true);
		$value = $this->_build_data($input);
		$id	   = $input['generalproducts_id'];

		$this->db->trans_begin();
		$result = $this->m_generalproducts->update($id, $value);
		
		if ($result){
			$_message = $this->getMessage('ADD');
			$_warning = 'success';
			$_status  = 1;
			$this->db->trans_commit();
		
		}else{
			$this->db->trans_rollback();
			$_message = $this->getMessage('ERROR');
			$_warning = 'warning';
			$_status  = 0;
		}
        
        $_data = array(
             'message'  	=> $_message['message_detail']
            ,'warning'  	=> $_warning
            ,'status'   	=> $_status
			,'title'    	=> $_message_title
			,'error_msg'    => $_error_msg
        );
        
        $this->output->set_output(json_encode($_data));
	}

	public function _build_data($input){
		$_system_date 		= date('Y-m-d H:i:s');

        $value['generalproducts_code'] 			= $input['generalproducts_code'];
		$value['generalproducts_title'] 		= $input['generalproducts_title'];
		$value['generalproducts_type'] 			= $input['generalproducts_type'];
		$value['generalproducts_type_detail'] 	= $input['generalproducts_type_detail'];
		$value['generalproducts_brand'] 		= $input['generalproducts_brand'];
		$value['generalproducts_price'] 		= $input['generalproducts_price'];
		$value['generalproducts_discount'] 		= $input['generalproducts_discount'];
		$value['generalproducts_detail'] 		= html_escape($input['generalproducts_detail'], FALSE);

		//upload Image
		$path 		= 'upload/generalproducts'.'/'.date('Y').'/'.date('m').'/';
		$pathName 	= 'generalproducts'.'/'.date('Y').'/'.date('m').'/';
	
		if(!is_dir($path)){
			mkdir($path,0755,TRUE);
		} 
		
		$_file_name		= '';
		$this->load->library('upload');
		$this->upload->initialize($this->setUploadOptions($pathName));
		if ($this->upload->do_upload('generalproducts_img')){
			$_file_name 		= $this->upload->data('file_name');
		}
		$this->load->library('image_lib');  
		if(!empty($_file_name)){
			// ปรับขนาดไฟล์ภาพ
			$config['image_library'] = 'gd2';  
			$config['source_image'] = $path.$_file_name; 
			$config['create_thumb'] = FALSE;  
			$config['maintain_ratio'] = FALSE;  
		
			$this->load->library('user_agent');
			$agent_system = $this->agent->platform();
			if($agent_system == "iOS" || $agent_system == "Android"){
				$config['rotation_angle'] = '270';	
				$this->image_lib->initialize($config);
				$this->image_lib->rotate();
			}
		
			$config['quality'] = '60%'; 
			$config['width'] = 900; 
			$config['height'] = 1200; 

			$this->image_lib->clear(); 
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
		
			// กำหนดค่าที่จะบันทึกใน database
			$value['generalproducts_img'] 	= $path.$_file_name;

			//ลบภาพเก่าก่อน เมื่อแก้ไข
			@unlink($input['generalproducts_img_edit']);
		}else{
			$value['generalproducts_img']		= $input['generalproducts_img_edit'];
		}
		

        if ( $input['mode'] == 'create' ) {
			$value['system_add_date']	 = $_system_date;
			$value['system_update_date'] = $_system_date;
            // $value['created_by'] = $this->session->users['user_id'];
        } else {
            $value['system_update_date'] = $_system_date;
            // $value['updated_by'] = $this->session->users['user_id'];
        }
        return $value;
	}
    
	public function delete(){
		$_message_title 		= 'ข้อความแจ้งเตือน !';
		$_message				= '';
		$_error					= 0;
		$_warning				= '';
		$_error_msg   		= '';
		$_system_date 			= date('Y-m-d H:i:s');

		$_generalproducts_code	    = $this->setBase64_decode($this->input->post('generalproducts_code'));
		$item 						= $this->m_generalproducts->get_generalproductsById($_generalproducts_code)->row();
	
		$_item_arr ='';	
	
		if(!empty($_generalproducts_code)){
			$_item_arr = array('is_delete' => 1,'system_update_date' => $_system_date);
		}

		$this->db->trans_begin();
		$result = $this->db->update('tbl_generalproducts', $_item_arr, array('generalproducts_no' => $_generalproducts_code));
		if ($result){
			@unlink($item->generalproducts_img);
			$this->deleteImgDetail($_generalproducts_code);

			$_message = $this->getMessage('DELETE');
			$_warning = 'success';
			$_status  = 1;
			$this->db->trans_commit();
		
		}else{
			$this->db->trans_rollback();
			$_message = $this->getMessage('ERROR');
			$_warning = 'warning';
			$_status  = 0;
		}
        
        $_data = array(
             'message'  	=> $_message['message_detail']
            ,'warning'  	=> $_warning
            ,'status'   	=> $_status
			,'title'    	=> $_message_title
			,'error_msg'    => $_error_msg
        );
        
        $this->output->set_output(json_encode($_data));
	}

	// นับการดู
	public function countgeneralproducts(){
		$_generalproducts_code	    = $this->setBase64_decode($this->input->post('generalproducts_code'));
		$this->m_generalproducts->plus_view($_generalproducts_code);
	}

	// โชว์สินค้า1รายการ
	public function showItem(){
		$_generalproducts_code		= $this->setBase64_decode($this->input->post('generalproducts_code'));
		$result 					= $this->m_generalproducts->get_generalproductsById($_generalproducts_code)->row();
		$result_img_detai 			= $this->m_generalproducts->get_generalproductsImgDetailById($_generalproducts_code)->result();

		// จัดการเบรนก่อน
		$_brand 		= '';
		$count_brands 	= $this->m_brands->countAll();
		if($count_brands > 0):
			$result_brands = $this->m_brands->veiw_brands()->result();
			foreach($result_brands as $item):
				if(!empty($result->generalproducts_brand) && $result->generalproducts_brand == $item->brands_no){
					$_brand = $item->brands_name;
				}elseif( $result->generalproducts_brand == '0'){
			  		$_brand = 'ไม่มีเบรน';
				}
			endforeach;
		endif;

		// แปลงวันที่ให้เป็นไทย
		$_date = $this->getDate($result->system_add_date);
		
		// แปลงวันที่ให้เป็นไทย และมีเวลา		
		if($result->system_sale_date != null){
			$_date_time = $this->getDateTime($result->system_sale_date);
		}else{
			$_date_time = '-';
		}
	
		if ($result){
			$_data = array(
				'generalproducts_code'  			=> $result->generalproducts_code
			   ,'generalproducts_title'  			=> $result->generalproducts_title
			   ,'generalproducts_type'  			=> $result->generalproducts_type
			   ,'generalproducts_type_detail'  		=> $result->generalproducts_type_detail
			   ,'generalproducts_brand'  			=> $_brand
			   ,'generalproducts_price'  			=> $result->generalproducts_price
			   ,'generalproducts_discount'  		=> $result->generalproducts_discount
			   ,'generalproducts_detail'  			=> html_entity_decode($result->generalproducts_detail)
			   ,'generalproducts_img'  				=> base_url().$result->generalproducts_img
			   ,'is_sale'  							=> $result->is_sale
			   ,'system_sale_date'  				=> $_date_time
			   ,'number_view'  						=> number_format($result->number_view)
			   ,'system_add_date'  					=> $_date
			   ,'image_error'  						=> base_url()."upload/error/error.jpg"
			   ,'images_detail'  					=> $result_img_detai
			);	
		}
        $this->output->set_output(json_encode($_data));
	}
	
	// เปิด-ปิด สถานะสินค้า
	public function active(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
		$_status            = 0;
		$_error_msg   		= '';
		$_system_date 		= date('Y-m-d H:i:s');
		
		$_active	    			= $this->input->post('active');
		$_generalproducts_code	    = $this->setBase64_decode($this->input->post('generalproducts_code'));
	
		$_item_arr ='';	
		if(!empty($_generalproducts_code)){
			$this->db->trans_begin();
			if($_active === 'true'){
				$_item_arr = array('is_active' => 0,'system_update_date' => $_system_date);
			}else{
				$_item_arr = array(
					'is_active' => 1,
					'system_update_date' => $_system_date);
			}
			$result ='';
			$result = $this->db->update('tbl_generalproducts', $_item_arr, array('generalproducts_no' => $_generalproducts_code));
			
			if ($result){
			$_message = $this->getMessage('EDIT');
			$_warning = 'success';
			$_status  = 1;
			$this->db->trans_commit();
		
			}else{
				$this->db->trans_rollback();
				$_message = $this->getMessage('ERROR');
				$_warning = 'warning';
				$_status  = 0;
			}		
		}
	
			
		$_data = array(
			'message'  	=> $_message['message_detail']
		   ,'warning'  	=> $_warning
		   ,'status'   	=> $_status
		   ,'title'    	=> $_message_title
		   ,'error_msg'    => $_error_msg
	   );
		
		$this->output->set_output(json_encode($_data));
	}

	// เปิด-ปิด สถานะการขาย
	public function sale(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
		$_status            = 0;
		$_error_msg   		= '';
		$_system_date 		= date('Y-m-d H:i:s');
		$_system_d 			= date('d');
		$_system_m			= date('m');
		$_system_y 			= date('Y');
		$_system_h 			= date('H');
		$_system_i 			= date('i');
		$_system_s 			= date('s');

		
		$_sale	    					= $this->input->post('sale');
		$_generalproducts_code	    = $this->setBase64_decode($this->input->post('generalproducts_code'));
		$_value_sale	    			= $this->input->post('value_sale');
	
		$_item_arr ='';	
		if(!empty($_generalproducts_code)){
			$this->db->trans_begin();
			if($_sale === 'true'){
				$_item_arr = array('is_sale' => 0,'system_sale_date' => $_system_date);
				$result = $this->db->update('tbl_generalproducts', $_item_arr, array('generalproducts_no' => $_generalproducts_code));
				
				if($result){
					$result_p  = $this->m_generalproducts->get_generalproductsById($_generalproducts_code)->row();

					$value['statistic_sale_sale_type'] 				= $_value_sale;
					$value['statistic_sale_products_code'] 			= $result_p->generalproducts_code;
					$value['statistic_sale_products_type'] 			= $result_p->generalproducts_type;
					$value['statistic_sale_products_type_detail'] 	= $result_p->generalproducts_type_detail;
					$value['statistic_sale_products_brand'] 		= $result_p->generalproducts_brand;
					$value['statistic_sale_products_price'] 		= $result_p->generalproducts_price;
					$value['statistic_sale_products_discount']	 	= $result_p->generalproducts_discount;
					$value['statistic_sale_products_grade']	 		= $result_p->generalproducts_grade;
					$value['statistic_sale_d']	 					= $_system_d;
					$value['statistic_sale_m']						= $_system_m;
					$value['statistic_sale_y']						= $_system_y;
					$value['statistic_sale_h']						= $_system_h;
					$value['statistic_sale_i']						= $_system_i;
					$value['statistic_sale_s']						= $_system_s;
					$value['statistic_sale_date_time']				= $_system_date;

					$chec = $this->m_statistics->count_chec($result_p->generalproducts_code);
					if($chec == 0){
						$this->m_statistics->insert_sale($value);
					}
				}

			}else{
				$_item_arr = array(
					'is_sale' => 1,
					'system_sale_date' => null,
					'system_update_date' => $_system_date);

					$result = $this->db->update('tbl_generalproducts', $_item_arr, array('generalproducts_no' => $_generalproducts_code));
			}
		
			if ($result){
			$_message = $this->getMessage('EDIT');
			$_warning = 'success';
			$_status  = 1;
			$this->db->trans_commit();
		
			}else{
				$this->db->trans_rollback();
				$_message = $this->getMessage('ERROR');
				$_warning = 'warning';
				$_status  = 0;
			}		
		}
	
			
		$_data = array(
			'message'  	=> $_message['message_detail']
		   ,'warning'  	=> $_warning
		   ,'status'   	=> $_status
		   ,'title'    	=> $_message_title
		   ,'error_msg'    => $_error_msg
	   );
		
		$this->output->set_output(json_encode($_data));
	}

	// เพิ่มรายละเอียดรูปภาพ (หลายรูป)
	public function addImgDetail(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
		$_status            = 0;
		$_error_msg   		= '';
		$_system_date 		= date('Y-m-d H:i:s');
		
		$_generalproducts_code	    = $this->setBase64_decode($this->input->post('code'));

		if(count($this->input->post('filemultiple-edit[]')) > 0){
			foreach($this->input->post('filemultiple-edit[]') as $value){
				@unlink($value);
				$this->m_generalproducts->deletegeneralproductsImgDetailByid($_generalproducts_code);
			}
		}
	
		// ทำการบันทึกไฟล์ภาพ
		if (!empty($_generalproducts_code)) {

			//upload Image
			$path 		= 'upload/generalproducts_detail'.'/'.date('Y').'/'.date('m').'/';
			$pathName 	= 'generalproducts_detail'.'/'.date('Y').'/'.date('m').'/'; 

			$upload = $this->do_uploadMultiple('filemultiple[]',TRUE,$pathName);

			$arr_file_generalproducts_img_detail = array();
			if(isset($upload['filenames']) && count($upload['filenames']) > 0){
				foreach($upload['filenames'] as $item){
					
					$file_path_db = 	$path.$item['index']['file_name'];
					
					array_push($arr_file_generalproducts_img_detail, array(
						'generalproducts_img_detail_path'  	=> $file_path_db
						,'generalproducts_no'            		=> $_generalproducts_code
						,'system_add_date' 						=> $_system_date
					));  
				}

				// ปรับขนาดไฟล์ภาพ
				$this->load->library('image_lib');
				foreach($upload['filenames'] as $item_img){
	
					$config['image_library'] = 'gd2';  
					$config['source_image'] = $path.$item_img['index']['file_name'];
					$config['create_thumb'] = FALSE;  
					$config['maintain_ratio'] = FALSE;
					  
					$this->load->library('user_agent');
					$agent_system = $this->agent->platform();
					if($agent_system == "iOS" || $agent_system == "Android"){
						$config['rotation_angle'] = '270';	
						$this->image_lib->initialize($config);
						$this->image_lib->rotate();
					}
				
					$config['quality'] = '60%'; 
					$config['width'] = 900; 
					$config['height'] = 1200; 

					$this->image_lib->clear(); 
					$this->image_lib->initialize($config);
					$this->image_lib->resize();
				}

				if ($this->m_generalproducts->insert_multiple($arr_file_generalproducts_img_detail)):
						$_message = $this->getMessage('ADD');
						$_warning = 'success';
						$_status  = 1;
						$this->db->trans_commit();
				endif;
			}
		} else {
			$this->db->trans_rollback();
			$_message = $this->getMessage('ERROR');
			$_warning = 'warning';
			$_status  = 0;
		}
	
			
		$_data = array(
			'message'  	=> $_message['message_detail']
		   ,'warning'  	=> $_warning
		   ,'status'   	=> $_status
		   ,'title'    	=> $_message_title
		   ,'error_msg'    => $_error_msg
	   );
		
		$this->output->set_output(json_encode($_data));
	}

	public function deleteImgDetail($_generalproducts_code){

		$result = $this->m_generalproducts->get_generalproductsImgDetailById($_generalproducts_code)->result();
		if($result){
			foreach($result as $value){
				@unlink($value->generalproducts_img_detail_path);
				$this->m_generalproducts->deletegeneralproductsImgDetailByid($_generalproducts_code);
			}
		}

	}
}
