<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class banner extends LI_Controller {
	
	function __construct() {
		
		parent::__construct();
		
		$this->_init();
		
		$this->load->model(
				array(
					'admin/m_generalproducts',
					'admin/m_brands',
					'admin/m_statistics',
					'admin/m_banner',
				));
	}
	
	public function _init(){
		$this->is_sale			= 0; 
		$this->is_active		= 0; 
		$this->is_delete		= 0;
        $this->user_code		= $this->session->userdata('logged_in')['user_code'];
		$this->output->set_content_type('application/json');
		
	}
	
	public function view(){
		$_data['generalproducts'] = $this->m_generalproducts->get_generalproducts()->result();
		$this->veiw_brands();
        // $_data['recordsTotal']      = $_all_result;
        // $_data['recordsFiltered']   = $_all_result;
        // $_data['data']              = $_services_arr;
        
        $this->output->set_output(json_encode($_data));
    }
    
    public function storage(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
        $_status            = 0;
		$_error				= 0;
		$_error_msg   		= '';
		$_system_date 		= date('Y-m-d H:i:s');

		$input = $this->input->post(null, true);

		// $this->arr($input);
		// exit;

		$value = $this->_build_data($input);
		$this->db->trans_begin();
		$result = $this->m_banner->insert($value);
	
		if ($result){
			$_message = $this->getMessage('ADD');
			$_warning = 'success';
			$_status  = 1;
			$this->db->trans_commit();
		
		}else{
			$this->db->trans_rollback();
			$_message = $this->getMessage('ERROR');
			$_warning = 'warning';
			$_status  = 0;
		}
		
		
		
        $_data = array(
             'message'  	=> $_message['message_detail']
            ,'warning'  	=> $_warning
            ,'status'   	=> $_status
			,'title'    	=> $_message_title
			,'error_msg'    => $_error_msg
        );
        
        $this->output->set_output(json_encode($_data));
    }
    
    public function update(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
        $_status            = 0;
		$_error				= 0;
		$_error_msg   		= '';
		$_system_date 		= date('Y-m-d H:i:s');

		$input = $this->input->post(null, true);
		$value = $this->_build_data($input);
		$id	   = $input['banner_no'];

		$this->db->trans_begin();
		$result = $this->m_banner->update($id, $value);
		
		if ($result){
			$_message = $this->getMessage('ADD');
			$_warning = 'success';
			$_status  = 1;
			$this->db->trans_commit();
		
		}else{
			$this->db->trans_rollback();
			$_message = $this->getMessage('ERROR');
			$_warning = 'warning';
			$_status  = 0;
		}
        
        $_data = array(
             'message'  	=> $_message['message_detail']
            ,'warning'  	=> $_warning
            ,'status'   	=> $_status
			,'title'    	=> $_message_title
			,'error_msg'    => $_error_msg
        );
        
        $this->output->set_output(json_encode($_data));
	}

	public function _build_data($input){
		$_system_date 		= date('Y-m-d H:i:s');

        $value['banner_title'] 			= $input['banner_title'];
		$value['banner_startDate'] 		= $input['banner_startDate'];
		$value['banner_endDate'] 		= $input['banner_endDate'];
		$value['banner_detail'] 		= html_escape($input['banner_detail'], FALSE);

		//upload Image
		$path 		= 'upload/banner'.'/'.date('Y').'/'.date('m').'/';
		$pathName 	= 'banner'.'/'.date('Y').'/'.date('m').'/';
	
		if(!is_dir($path)){
			mkdir($path,0755,TRUE);
		} 
		
		$_file_name		= '';
		$this->load->library('upload');
		$this->upload->initialize($this->setUploadOptions($pathName));
		if ($this->upload->do_upload('banner_img')){
			$_file_name 		= $this->upload->data('file_name');
		}

		if(!empty($_file_name)){
			//ปรับขนาดไฟล์ภาพ
			$config['image_library'] = 'gd2';  
			$config['source_image'] = $path.$_file_name; 
			$config['create_thumb'] = FALSE;  
			$config['maintain_ratio'] = FALSE;  
			$config['quality'] = '60%';  
			$config['width'] = 1280;  
			$config['height'] = 480;  
			$this->load->library('image_lib', $config);  
			$this->image_lib->resize();

			// กำหนดค่าที่จะบันทึกใน database
			$value['banner_img'] 	= $path.$_file_name;

			//ลบภาพเก่าก่อน เมื่อแก้ไข
			@unlink($input['banner_img_edit']);
		}else{
			$value['banner_img']	= $input['banner_img_edit'];
		}
		

        if ( $input['mode'] == 'create' ) {
			$value['system_add_date']	 = $_system_date;
			$value['system_update_date'] = $_system_date;
            // $value['created_by'] = $this->session->users['user_id'];
        } else {
            $value['system_update_date'] = $_system_date;
            // $value['updated_by'] = $this->session->users['user_id'];
        }
        return $value;
	}
    
	public function delete(){
		$_message_title 		= 'ข้อความแจ้งเตือน !';
		$_message				= '';
		$_error					= 0;
		$_warning				= '';
		$_error_msg   			= '';
		$_system_date 			= date('Y-m-d H:i:s');

		$_banner_no	    = $this->setBase64_decode($this->input->post('banner_no'));
		$item 						= $this->m_banner->get_bannerById($_banner_no)->row();
	
		$_item_arr ='';	
	
		if(!empty($_banner_no)){
			$_item_arr = array('is_delete' => 1,'system_update_date' => $_system_date);
		}

		$this->db->trans_begin();
		$result = $this->db->update('tbl_banner', $_item_arr, array('banner_no' => $_banner_no));
		if ($result){
			@unlink($item->banner_img);

			$_message = $this->getMessage('DELETE');
			$_warning = 'success';
			$_status  = 1;
			$this->db->trans_commit();
		
		}else{
			$this->db->trans_rollback();
			$_message = $this->getMessage('ERROR');
			$_warning = 'warning';
			$_status  = 0;
		}
        
        $_data = array(
             'message'  	=> $_message['message_detail']
            ,'warning'  	=> $_warning
            ,'status'   	=> $_status
			,'title'    	=> $_message_title
			,'error_msg'    => $_error_msg
        );
        
        $this->output->set_output(json_encode($_data));
	}

	// นับการดู
	// public function countgeneralproducts(){
	// 	$_generalproducts_code	    = $this->setBase64_decode($this->input->post('generalproducts_code'));
	// 	$this->m_generalproducts->plus_view($_generalproducts_code);
	// }

	// โชว์ป้ายโฆษณา 1 รายการ
	public function showItem(){
		$_generalproducts_code		= $this->setBase64_decode($this->input->post('generalproducts_code'));
		$result 					= $this->m_generalproducts->get_generalproductsById($_generalproducts_code)->row();
		$result_img_detai 			= $this->m_generalproducts->get_generalproductsImgDetailById($_generalproducts_code)->result();

		// จัดการเบรนก่อน
		$_brand 		= '';
		$count_brands 	= $this->m_brands->countAll();
		if($count_brands > 0):
			$result_brands = $this->m_brands->veiw_brands()->result();
			foreach($result_brands as $item):
				if(!empty($result->generalproducts_brand) && $result->generalproducts_brand == $item->brands_no){
					$_brand = $item->brands_name;
				}elseif( $result->generalproducts_brand == '0'){
			  		$_brand = 'ไม่มีเบรน';
				}
			endforeach;
		endif;

		// แปลงวันที่ให้เป็นไทย
		$_date = $this->getDate($result->system_add_date);
		
		// แปลงวันที่ให้เป็นไทย และมีเวลา		
		if($result->system_sale_date != null){
			$_date_time = $this->getDateTime($result->system_sale_date);
		}else{
			$_date_time = '-';
		}
	
		if ($result){
			$_data = array(
				'generalproducts_code'  			=> $result->generalproducts_code
			   ,'generalproducts_title'  			=> $result->generalproducts_title
			   ,'generalproducts_type'  			=> $result->generalproducts_type
			   ,'generalproducts_type_detail'  		=> $result->generalproducts_type_detail
			   ,'generalproducts_brand'  			=> $_brand
			   ,'generalproducts_price'  			=> $result->generalproducts_price
			   ,'generalproducts_discount'  		=> $result->generalproducts_discount
			   ,'generalproducts_detail'  			=> html_entity_decode($result->generalproducts_detail)
			   ,'generalproducts_img'  				=> base_url().$result->generalproducts_img
			   ,'is_sale'  							=> $result->is_sale
			   ,'system_sale_date'  				=> $_date_time
			   ,'number_view'  						=> number_format($result->number_view)
			   ,'system_add_date'  					=> $_date
			   ,'image_error'  						=> base_url()."upload/error/error.jpg"
			   ,'images_detail'  					=> $result_img_detai
			);	
		}
        $this->output->set_output(json_encode($_data));
	}
	
	// เปิด-ปิด สถานะป้ายโฆษณา
	public function active(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
		$_status            = 0;
		$_error_msg   		= '';
		$_system_date 		= date('Y-m-d H:i:s');
		
		$_active	    = $this->input->post('active');
		$_banner_no	    = $this->setBase64_decode($this->input->post('banner_no'));
	
		$_item_arr ='';	
		if(!empty($_banner_no)){
			$this->db->trans_begin();
			if($_active === 'true'){
				$_item_arr = array('is_active' => 0,'system_update_date' => $_system_date);
			}else{
				$_item_arr = array(
					'is_active' => 1,
					'system_update_date' => $_system_date);
			}
			$result ='';
			$result = $this->db->update('tbl_banner', $_item_arr, array('banner_no' => $_banner_no));
			
			if ($result){
			$_message = $this->getMessage('EDIT');
			$_warning = 'success';
			$_status  = 1;
			$this->db->trans_commit();
		
			}else{
				$this->db->trans_rollback();
				$_message = $this->getMessage('ERROR');
				$_warning = 'warning';
				$_status  = 0;
			}		
		}
	
			
		$_data = array(
			'message'  	=> $_message['message_detail']
		   ,'warning'  	=> $_warning
		   ,'status'   	=> $_status
		   ,'title'    	=> $_message_title
		   ,'error_msg'    => $_error_msg
	   );
		
		$this->output->set_output(json_encode($_data));
	}

	// ปิด สถานะป้ายโฆษณา (เมื่อหมดเวลาจะปิดอัตโนมัต)
	public function d_active(){
		$_data	            = array();
		$_status            = 0;
		$_system_date 		= date('Y-m-d H:i:s');
		
	
		$_banner_no	    = $this->input->post('banner_no');
	
		$_item_arr ='';	
		if(!empty($_banner_no)){
			$this->db->trans_begin();
		
			$_item_arr = array('is_active' => 1, 'system_update_date' => $_system_date);
			$result = $this->db->update('tbl_banner', $_item_arr, array('banner_no' => $_banner_no));
			
			if ($result){
				$_status  = 1;
				$this->db->trans_commit();
			}else{
				$this->db->trans_rollback();
				$_status  = 0;
			}		
		}

		$_data = array(
		   	'status'   	=> $_status
	   );
		
		$this->output->set_output(json_encode($_data));
	}

}
