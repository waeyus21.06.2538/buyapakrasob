<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class brands extends LI_Controller {
	
	function __construct() {
		
		parent::__construct();
		
		$this->_init();
		
		$this->load->model(
				array(
					'admin/m_brands'
				));
	}
	
	public function _init(){
		$this->is_sale			= 0; 
		$this->is_active		= 0; 
		$this->is_delete		= 0;
        $this->user_code		= $this->session->userdata('logged_in')['user_code'];
		$this->output->set_content_type('application/json');
		
	}
	
	public function view(){
		$_data['recommendproducts'] = $this->m_recommendproducts->get_recommendproducts()->result();
        
        // $_data['recordsTotal']      = $_all_result;
        // $_data['recordsFiltered']   = $_all_result;
        // $_data['data']              = $_services_arr;
        
        $this->output->set_output(json_encode($_data));
    }
    
    public function storage(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
        $_status            = 0;
		$_error				= 0;
		$_error_msg   		= '';
		$_system_date 		= date('Y-m-d H:i:s');

		$input = $this->input->post(null, true);
		$value = $this->_build_data($input);

		$this->db->trans_begin();
		$result = $this->m_brands->insert($value);
	
		if ($result){
			$_message = $this->getMessage('ADD');
			$_warning = 'success';
			$_status  = 1;
			$this->db->trans_commit();
		
		}else{
			$this->db->trans_rollback();
			$_message = $this->getMessage('ERROR');
			$_warning = 'warning';
			$_status  = 0;
		}
        
        $_data = array(
             'message'  	=> $_message['message_detail']
            ,'warning'  	=> $_warning
            ,'status'   	=> $_status
			,'title'    	=> $_message_title
			,'error_msg'    => $_error_msg
        );
        
        $this->output->set_output(json_encode($_data));
    }
    
    public function update(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
        $_status            = 0;
		$_error				= 0;
		$_error_msg   		= '';
		$_system_date 		= date('Y-m-d H:i:s');

		$input = $this->input->post(null, true);
		$value = $this->_build_data($input);
		$id	   = $input['brands_no'];

		$this->db->trans_begin();
		$result = $this->m_brands->update($id, $value);
		
		if ($result){
			$_message = $this->getMessage('ADD');
			$_warning = 'success';
			$_status  = 1;
			$this->db->trans_commit();
		
		}else{
			$this->db->trans_rollback();
			$_message = $this->getMessage('ERROR');
			$_warning = 'warning';
			$_status  = 0;
		}
        
        $_data = array(
             'message'  	=> $_message['message_detail']
            ,'warning'  	=> $_warning
            ,'status'   	=> $_status
			,'title'    	=> $_message_title
			,'error_msg'    => $_error_msg
        );
        
        $this->output->set_output(json_encode($_data));
	}

	public function _build_data($input){
		$_system_date 		= date('Y-m-d H:i:s');

        $value['brands_name'] 		= $input['brands_name'];
	
		//upload Image
		$path 		= 'upload/brands'.'/'.date('Y').'/'.date('m').'/';
		$pathName 	= 'brands'.'/'.date('Y').'/'.date('m').'/';
	
		if(!is_dir($path)){
			mkdir($path,0755,TRUE);
		} 
		
		$_file_name		= '';
		$this->load->library('upload');
		$this->upload->initialize($this->setUploadOptions($pathName));
		if ($this->upload->do_upload('brands_img')){
			$_file_name 		= $this->upload->data('file_name');
		}

		if(!empty($_file_name)){
			$value['brands_img'] 	= $path.$_file_name;
			//ลบภาพเก่าก่อน เมื่อแก้ไข
			@unlink($input['brands_img_edit']);
		}else{
			$value['brands_img']		= $input['brands_img_edit'];
		}
		

        if ( $input['mode'] == 'create' ) {
			$value['system_add_date']	 = $_system_date;
			$value['system_update_date'] = $_system_date;
            // $value['created_by'] = $this->session->users['user_id'];
        } else {
            $value['system_update_date'] = $_system_date;
            // $value['updated_by'] = $this->session->users['user_id'];
        }
        return $value;
	}
    
	public function delete(){
		$_message_title 		= 'ข้อความแจ้งเตือน !';
		$_message				= '';
		$_error					= 0;
		$_warning				= '';
		$_error_msg   			= '';
		$_system_date 			= date('Y-m-d H:i:s');

		$_brands_no	    = $this->setBase64_decode($this->input->post('brands_no'));
	
		$_item_arr ='';	
	
		if(!empty($_brands_no)){
			$_item_arr = array('is_delete' => 1,'system_update_date' => $_system_date);
		}

		$this->db->trans_begin();
		$result = $this->db->update('tbl_brands', $_item_arr, array('brands_no' => $_brands_no));
		if ($result){
			$_message = $this->getMessage('DELETE');
			$_warning = 'success';
			$_status  = 1;
			$this->db->trans_commit();
		
		}else{
			$this->db->trans_rollback();
			$_message = $this->getMessage('ERROR');
			$_warning = 'warning';
			$_status  = 0;
		}
        
        $_data = array(
             'message'  	=> $_message['message_detail']
            ,'warning'  	=> $_warning
            ,'status'   	=> $_status
			,'title'    	=> $_message_title
			,'error_msg'    => $_error_msg
        );
        
        $this->output->set_output(json_encode($_data));
	}

	// เปิด-ปิด สถานะสินค้า
	public function active(){
		$_data	            = array();
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		$_message			= '';
		$_status            = 0;
		$_error_msg   		= '';
		$_system_date 		= date('Y-m-d H:i:s');
		
		$_active	    				= $this->input->post('active');
		$_brands_no	    = $this->setBase64_decode($this->input->post('brands_no'));
	
		$_item_arr ='';	
		if(!empty($_brands_no)){
			$this->db->trans_begin();
			if($_active === 'true'){
				$_item_arr = array('is_active' => 0,'system_update_date' => $_system_date);
			}else{
				$_item_arr = array(
					'is_active' => 1,
					'system_update_date' => $_system_date);
			}
			$result ='';
			$result = $this->db->update('tbl_brands', $_item_arr, array('brands_no' => $_brands_no));
			
			if ($result){
			$_message = $this->getMessage('EDIT');
			$_warning = 'success';
			$_status  = 1;
			$this->db->trans_commit();
		
			}else{
				$this->db->trans_rollback();
				$_message = $this->getMessage('ERROR');
				$_warning = 'warning';
				$_status  = 0;
			}		
		}
	
			
		$_data = array(
			'message'  	=> $_message['message_detail']
		   ,'warning'  	=> $_warning
		   ,'status'   	=> $_status
		   ,'title'    	=> $_message_title
		   ,'error_msg'    => $_error_msg
	   );
		
		$this->output->set_output(json_encode($_data));
	}

}
