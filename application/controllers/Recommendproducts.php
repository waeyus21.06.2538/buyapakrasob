<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recommendproducts extends LI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model(
			array(
				'admin/m_recommendproducts'
			));
	}

	public function index(){
		$_start 			= 0;
        $_length			= 14;
		
		$this->_data['recommendproducts_more'] = $this->m_recommendproducts->view_recommendproductsLimit($_start, $_length)->result();
		// $this->_data['recommendproducts_more'] = $this->m_recommendproducts->view_recommendproducts()->result();
		// $send = array(
		// 	'header' 		=> 'header'
		// 	,'content' 		=> 'recommendproducts/index'
		// 	,'footer' 		=> 'footer'
		// 	,'pagetitle' 	=> 'ติดต่อเรา'
		// 	,'data'			=> $this->_data
		// );

		// $this->loadPageFrontend($send);
	}

	public function detail($id=null){
		$id 	 										= $this->setBase64_decode($id);

		// นับการดูสินค้า
		$this->m_recommendproducts->plus_view($id);
		
		$this->_data['recommendproducts'] 				= $this->m_recommendproducts->get_recommendproductsById($id)->row();
		$this->_data['recommendproductsimagesdetail']	= $this->m_recommendproducts->get_recommendproductsImgDetailById($id)->result();
		
		$this->veiw_brands();
		$this->veiw_contact();

		$this->index();
		$send = array(
			'header' 		=> 'header'
			,'content' 		=> 'recommendproducts/recommendproducts_detail'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'รายละเอียดสินค้าแนะนำ'
			,'data'			=> $this->_data
		);

		$this->loadPageFrontend($send);
	}

	public function more(){	
		
		$this->veiw_contact();

		$this->_data['recommendproducts_test'] = $this->m_recommendproducts->get_recommendproducts()->result();
		$this->_data['recommendproducts'] = $this->m_recommendproducts->get_recommendproductsById(29)->row();
		$send = array(
			 'header' 		=> 'header'
			,'menu' 		=> 'menu'
			,'content' 		=> 'recommendproducts/recommendproducts_more'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'สินค้าแนะนำเพิ่มเติม'
			,'mode'			=> 'create'
			,'data'			=> $this->_data
		);
		
		$this->loadPageFrontend($send);
	}


}
