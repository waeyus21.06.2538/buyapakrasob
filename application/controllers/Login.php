<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends LI_Controller {
	
	function __construct() {
        parent::__construct();
		
		$this->is_active		= 0; 
		$this->is_delete		= 0; 
		
		$this->load->model(
				array(
                'm_user'
				));
	}
	
	
	public function index(){
		
		$this->load->view('page/security/login');
	}
	
	public function authen(){
		$_message_title 	= 'ข้อความแจ้งเตือน !';
		
		$_user_name = $this->input->post('user_name');
		$_user_pass = $this->input->post('user_pass');
		
		$_obj_user = $this->m_user->viewByUserAuthen($_user_name, $_user_pass, $this->is_active, $this->is_delete);
		if($_obj_user['status'] == true && $_obj_user['results'] != null){
			  
			$_user_arr = array(
				 'user_code' 		=> $_obj_user['results']->user_code
				,'user_type_code' 	=> $_obj_user['results']->user_type_code
				,'user_name' 		=> $_obj_user['results']->user_name
				,'user_img' 		=> $_obj_user['results']->user_img
			);
			  
			$this->session->set_userdata('logged_in', $_user_arr);
			switch($_obj_user['results']->user_type_code){
				case '5A7F141E7E2A1':
					redirect(base_url().'admin/dasboard');
				break;
				case '5A7F14B70BB44':
					redirect(base_url().'admin/research');
				break;

			}
			
	 
		}else{
			$_message = 'ชื่อผู้ใช้งาน และรหัสผ่านไม่ถูกต้อง กรุณาลองใหม่';
			$_warning = 'warning';
			
			$this->session->set_flashdata('message', $_message);
			$this->session->set_flashdata('message_title', $_message_title);
			$this->session->set_flashdata('message_check', $_warning);
			
			redirect(base_url().'login');
		}
	}
	
	public function logout(){
		$this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();
		redirect(base_url().'login', 'refresh');
	}
}
