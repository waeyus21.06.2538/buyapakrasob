<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends LI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model(
			array(
				'admin/m_banner',
			));
	}

	public function index(){
		// $_start 			= 0;
        // $_length			= 6;	
		// $this->_data['generalproducts'] = $this->m_generalproducts->view_generalproductsLimit($_start, $_length)->result();

		$send = array(
			'header' 		=> 'header'
			,'content' 		=> 'generalproducts/index'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'ติดต่อเรา'
			,'data'			=> $this->_data
		);

		$this->loadPageFrontend($send);
	}

	public function detail($id=null){
		$id 	 					= $this->setBase64_decode($id);

		// นับการดูสินค้า
		$this->m_banner->plus_view($id);
		
		$this->_data['banner'] 		= $this->m_banner->get_bannerById($id)->row();

		$this->veiw_contact();

		$send = array(
			'header' 		=> 'header'
			,'content' 		=> 'banner/banner_detail'
			,'footer' 		=> 'footer'
			,'pagetitle' 	=> 'รายละเอียด'
			,'data'			=> $this->_data
		);

		$this->loadPageFrontend($send);
	}




}
